package alien.server.game;

import java.util.List;

import alien.server.sector.Position;
import alien.server.sector.Sector;
/**
 * Represent a pawn of a player. It can move on a MapClass and attack other pawns.

 */
public abstract class Pawn {
         protected int numberOfSteps; 
         private Position position, startingPosition;
         private String name;
         protected Player owner;
         private boolean silent;
         private boolean alive;
         /**
          * Returns true if this pawn can use items
          * @return true if this pawn can use items
          */
         abstract public boolean canUseItem();
         /**
          * Returns true if this pawn can attack
          * @return true if this pawn can attack
          */
         abstract public boolean canAttack();
         /**
          * Return true if this pawn can enter in the specified sector
          * @param sector Sector to be tested
          * @return true if this pawn can enter in the specified sector
          */
         abstract public boolean canEnter(Sector sector);
         /**
          * This pawn add itself on gameBoard pawns lists depending on its type
          * and sets the startingPosition and position depending on the mapClass used in the specified gameBoard
          * @param gameBoard The gameBoard in witch is added
          */
         abstract public void register(GameBoard gameBoard);
         abstract public void unregister(GameBoard gameBoard);
         protected Pawn(String name,Player owner, int numberOfSteps)
         {
        	 this.name=name;
        	 this.numberOfSteps=numberOfSteps;
        	 this.owner=owner;
        	 silent=false;
        	 alive=true;
         }
         /**
          * Change the position of this pawn to the specified position
          * @param to Destination
          */
         public void move(Position to)
         {
        	 position=to;
         }
         /**
          * This pawn is killed and his owner is notified
          * 
          */
         public void die()
         {
        	 alive=false;
           	if(owner!=null) owner.pawnKilled();
        	
         }
         /**
          * This pawn attack a set of Pawns.
          * The attacked pawns are killed if they can't defend.
          * @param attackedPawns Pawns to be attacked
          */
         public void attack(List<Pawn> attackedPawns)
         {
     		for(Pawn pawn : attackedPawns)
     		{
     			if(pawn.canDefend())
     			{
     				pawn.defend();
     			}
     			else
     			{
     				pawn.die();
     			}
     		}
         }
         private void defend() {
        	 if(owner!=null)
        	 owner.defend();
			
		}
		private boolean canDefend() {
			if(owner==null) return false;
			return owner.canDefend();
		}

		public int getNumberOfSteps() {
			return numberOfSteps;
		}
		public void setNumberOfSteps(int numberOfSteps) {
			this.numberOfSteps = numberOfSteps;
		}
		public Position getStartingPosition() {
			return startingPosition;
		}
		public Position getPosition()
		{
			return position;
		}
		public void setSilent(boolean silent)
		{
			this.silent=silent;
		}
		public boolean isSilent()
		{
			return silent;
		}

		/**
		 * Set the starting position and the current position to the spiecified position
		 * @param startingPosition Position where this pawn start the game
		 */
		public void setStartingPosition(Position startingPosition)
		{
			this.startingPosition=startingPosition;
			this.position=startingPosition;
		}
		/**
		 * Returns true if this pawn is alive
		 * @return true if this pawn is alive
		 */
		public boolean isAlive()
		{
			return alive;
		}
		/**
		 * Notify the owner of this pawn that this pawn has been spotted.
		 */
		public void spotted()
		{
			owner.declarePosition(getPosition());
		}
		public String getName() {
			return name;
		}
		public abstract String toString();
}
