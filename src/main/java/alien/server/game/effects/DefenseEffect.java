package alien.server.game.effects;

import alien.server.card.ItemCard;
import alien.server.game.Player;

public class DefenseEffect extends InstantEffect{

	public DefenseEffect(ItemCard item) {
		super(item);
	}

	@Override
	public void affect(EffectAffectable affectable) {
		Player player =affectable.getPlayer();
		player.addDefenseItem(getItemCard());
	}

}
