package alien.server.game.effects;

import alien.server.card.ItemCard;

public abstract class InstantEffect extends Effect{
	private ItemCard item;
    public InstantEffect(ItemCard item) {
    	this.item=item;
	}
    protected ItemCard getItemCard()
    {
    	return item;
    }
	@Override
	public final void remove(EffectAffectable affectable) {
		
	}

}
