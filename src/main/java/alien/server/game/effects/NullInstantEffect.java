package alien.server.game.effects;

import alien.server.card.ItemCard;

public class NullInstantEffect extends InstantEffect{

	public NullInstantEffect(ItemCard item) {
		super(item);
	}

	@Override
	public void affect(EffectAffectable affectable) {
		
	}

}
