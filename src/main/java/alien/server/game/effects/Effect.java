package alien.server.game.effects;

public abstract class Effect {

public abstract void affect(EffectAffectable affectable);
public abstract void remove(EffectAffectable affectable);
}
