package alien.server.game.effects;

import alien.server.game.Pawn;
import alien.server.game.Player;
import alien.server.util.StringUpdate;

public class AdrenalineEffect extends Effect{

	@Override
	public void affect(EffectAffectable affectable) {
		Pawn pawn = affectable.getPawn();
		pawn.setNumberOfSteps(pawn.getNumberOfSteps()+1);//aumento il numero di passi che può effettuare la pedina
		Player player = affectable.getPlayer();
		player.notifyObservers(StringUpdate.privateMessage("Now you can move to greater distance", player.getName()));
	}
	
	@Override
	public void remove(EffectAffectable affectable) {
		Pawn pawn = affectable.getPawn();
		pawn.setNumberOfSteps(pawn.getNumberOfSteps()-1);
	}

}
