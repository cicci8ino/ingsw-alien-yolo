package alien.server.game.effects;

import alien.server.game.Pawn;
import alien.server.game.Player;
import alien.server.sector.MapClass;

public interface EffectAffectable {
public Pawn getPawn();
public MapClass getMap();
public Player getPlayer();
}
