package alien.server.game.effects;

import alien.server.game.Pawn;

public class SedativesEffect extends Effect{

	@Override
	public void affect(EffectAffectable affectable) {

		Pawn pawn = affectable.getPawn();
		pawn.setSilent(true);
	}

	@Override
	public void remove(EffectAffectable affectable) {
		Pawn pawn = affectable.getPawn();
		pawn.setSilent(false);
	}

}
