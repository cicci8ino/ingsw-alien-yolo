package alien.server.game.effects;

import alien.server.game.Pawn;
import alien.server.game.Player;
import alien.server.util.StringUpdate;

public class TeleportEffect extends Effect{

	@Override
	public void affect(EffectAffectable affectable) {
		Pawn pawn = affectable.getPawn();
		pawn.move(pawn.getStartingPosition());
		Player player = affectable.getPlayer();
		player.notifyObservers(StringUpdate.privateMessage("You teleported to the starting position",player.getName()));
		
	}

	@Override
	public void remove(EffectAffectable affectable) {
		
		
	}

}
