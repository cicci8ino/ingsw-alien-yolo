package alien.server.game.effects;

import alien.server.game.Player;

public class AttackEffect extends Effect {

	@Override
	public void affect(EffectAffectable affectable) {
		Player player=affectable.getPlayer();
		player.attack();
	}

	@Override
	public void remove(EffectAffectable affectable) {
		
	}

}
