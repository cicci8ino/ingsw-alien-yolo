package alien.server.game.effects;

import alien.server.game.Player;
import alien.server.sector.MapClass;
import alien.server.status.SpotlightEvent;
import alien.server.status.WaitingPosition;

public class SpotlightEffect extends Effect{

	@Override
	public void affect(EffectAffectable affectable) {
		MapClass map = affectable.getMap();
		Player player = affectable.getPlayer();
		SpotlightEvent event= new SpotlightEvent(player,  map);
		player.pushIncompleteStatus(new WaitingPosition(event));
		player.notifyStatus();
	}

	@Override
	public void remove(EffectAffectable affectable) {
		
	}

}
