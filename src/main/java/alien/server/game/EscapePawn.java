package alien.server.game;
/**Tutti i pawn che possono usare un escape Hatch devono implementare questa interfaccia
 * 
 *
 */
public interface EscapePawn {
	public void escape();

	public boolean isEscaped();

}
