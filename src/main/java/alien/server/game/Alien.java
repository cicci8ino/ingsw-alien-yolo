package alien.server.game;

import java.util.*;

import alien.server.sector.Sector;

public class Alien extends Pawn 
{
	static private final int STEPS_PER_TURN=2;
	/**
	 * An alien pawn.
	 * @param owner The player who owns this pawn
	 * @param name The name of this pawn
	 */
    public Alien(Player owner, String name)
    {
    	super(name,owner,STEPS_PER_TURN);
    }
	@Override
	public boolean canUseItem() {
		return false;
	}
	/**
      * This pawn attack a set of Pawns.
      * The attacked pawns are killed if they can't defend.
      * If one or more humanPawns are killed, then numberOfSteps of this pawn is incremented by one.
      * @param attackedPawns Pawns to be attacked
	 */
    @Override 
    public void attack(List<Pawn> attackedPawns)
    {
    	super.attack(attackedPawns);
    	List<Pawn> killedPawns=new ArrayList<Pawn>();
    	for(Pawn attacked : attackedPawns)
    	{
    		if(!attacked.isAlive())killedPawns.add(attacked);
    	}
    	if(numberOfSteps==STEPS_PER_TURN && containsHuman(killedPawns))
    	{
        	numberOfSteps=STEPS_PER_TURN+1;
    	}
    }

	private boolean containsHuman(List<Pawn> killed)
	{
		for(Pawn pawn : killed) if(pawn instanceof Human) return true;
		return false;
	}

	@Override
	public boolean canAttack() {
		return true;
	}
	@Override
	public boolean canEnter(Sector sector) {
		return sector.canEnter(this);
	}

	@Override
	public void register(GameBoard gameBoard) {
		gameBoard.registerAlien(this);
		
	}

	@Override
	public String toString() {
		return "Alien";
	}
	@Override
	public void unregister(GameBoard gameBoard) {
		gameBoard.unregister(this);
	}
	
}
