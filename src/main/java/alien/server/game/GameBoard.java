package alien.server.game;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import alien.server.card.*;
import alien.server.exception.PlayerNameNotRegistredException;
import alien.server.sector.MapClass;
import alien.server.status.DisconnectedStatus;
import alien.server.status.TurnEndedStatus;
import alien.server.status.TurnStartedStatus;
import alien.server.status.WonStatus;
import alien.server.util.StringUpdateObservable;
import alien.server.util.StringUpdateObserver;
import alien.server.util.StringUpdate;

public class GameBoard implements StringUpdateObservable {

  private static final int MAX_NUMBER_OF_TURNS =39;
private List <Player> playersList;
  private final List<Human> humans=new ArrayList<Human>();
  private final List<Alien> aliens=new ArrayList<Alien>();
  private GameDecks decks;
  private List<StringUpdateObserver> observers=new ArrayList<StringUpdateObserver>();
  private final MapClass map;
  Integer playerOnTurnIndex;
  public GameBoard(MapClass map)
  {
	  this.map=map;
  }
  /**
   * Creates all the players object and puts them in playersList.
   *  The number of the players created is equal to the number of names specified.
   * @param playersNames The names of the players
   */
  public void createPlayers(List<String> playersNames)
  {
	  playersList= new ArrayList<Player>();
	  decks=new GameDecks(playersNames.size());
		for(String playerName: playersNames)
		{
			Player player = new Player(map,decks,playerName);
			for(StringUpdateObserver observer: observers) player.register(observer);
			playersList.add(player);
		}
  }

public boolean noHumansLeft() {
	
	for(Human human :humans) if(human.isAlive()&&(!human.isEscaped())) return false;
	return true;
}
/**
 * Sets all alien players status to turnEndedStatus and notify the aliens defeat via broadcast message 
 */
public void humansVictory() {
	for(Player player: playersList) if(aliens.contains(player.getPawn())) player.setStatus(new TurnEndedStatus());
	notifyObservers(StringUpdate.victoryMessage("Last human escaped: Aliens defeat"));
}
/**
 * Sets all alien players status to wonStatus and notify the aliens victory via broadcast message
*/
public void aliensVictory() {

	for(Player player: playersList) {
		if(aliens.contains(player.getPawn()))
				{
				player.setStatus(new WonStatus());
				player.notifyStatus();
				}
		
	}		
	notifyObservers(StringUpdate.victoryMessage("Last human killed: Aliens victory"));
}
/**
 * The player having the specified name is set to DisconnectedStatus, his pawn is unregistred from the pawn lists
 * If the player was the last human then is called aliensVictory
 * If the player was the player on turn then is called nextTurn
 * A broadcast message is notified declaring the player disconnection
 * 
 * @param playerName The name of the disconnected player
 */
public void disconnectPlayer(String playerName)
{
	Player player = getPlayerByName(playerName);
	player.setStatus(new DisconnectedStatus());
	player.getPawn().unregister(this);
	if(noHumansLeft()) aliensVictory();
	if(player.equals(getPlayerOnTurn()))
	{
		nextTurn();
	}
	notifyObservers(StringUpdate.broadcastMessage(player.getName()+" disconnected"));
	
}
/**
 * Gets the player on turn
 * @return the player on turn
 */
public Player getPlayerOnTurn()
{
	return playersList.get(playerOnTurnIndex);
}
/**Ends this turn
 * Changes the player on turn and sets the player status to TurnStartedStatus
 */
public void nextTurn()
{
	Player playerOnTurn=getPlayerOnTurn();
	playerOnTurn.removeEffects();
	playerOnTurnIndex++;
	if(playerOnTurnIndex==playersList.size())
		playerOnTurnIndex=0;
	playerOnTurn=getPlayerOnTurn();
	playerOnTurn.isYourTurn();
	if(playerOnTurn.getTurn()>=MAX_NUMBER_OF_TURNS) aliensVictory();
	else{
		if(playerOnTurn.canPlay())
		{
			playerOnTurn.setStatus(new TurnStartedStatus());
			playerOnTurn.notifyStatus();
		}
	else nextTurn();
	}
}
public List<String> getPlayersNames(){
	
	List<String> names= new ArrayList<String>();
	for(Player player: playersList) names.add(player.getName());
	return names;
}
/**
 * Registers the human pawn in the humans list and sets its starting position
 * @param human Human to be registred
 */
public void registerHuman(Human human)
{
	humans.add(human);
	human.setStartingPosition(map.getHumanPosition());
}
/**
 * Adds the specified alien to the aliens list and sets its starting position
 * @param alien Alien to be registred
 */
public void registerAlien(Alien alien)
{
	aliens.add(alien);
	alien.setStartingPosition(map.getAlienPosition());
}

public Player getPlayerByName(String name) {
	
	for(Player player: playersList) if(player.getName().equals(name)) return player;
	throw new PlayerNameNotRegistredException();
}
public MapClass getMap()
{
	return map;
}
/**
 * Gets a list of strings representing the names of the dead players 
 * Player.isAlive is called to know the state of the player
 * @return list of strings representing the names of the dead players 
 */
public List<String> getDeadPlayersNames() {
	List<String> deadPlayers= new ArrayList<String>();
	for(Player player: playersList) if(!player.isAlive()) deadPlayers.add(player.getName());
	   return deadPlayers;
}

@Override
public void register(StringUpdateObserver observer) {
	 if(observer == null) throw new NullPointerException("Null Observer");
     if(!observers.contains(observer)) observers.add(observer);
}
@Override
public void unregister(StringUpdateObserver observer) {
    observers.remove(observer);
}
public void unregister(Human human) {
	humans.remove(human);
}
public void unregister(Alien alien) {
	aliens.remove(alien);
}
@Override
public void notifyObservers(StringUpdate stringUpdate) {
	for (StringUpdateObserver obs : observers) {
        obs.update(stringUpdate);
    }
}
/**
 *  Assigns the roles to the players calling player.drawCharacterCard for each player
 *  Registers the new players pawns in the pawn list calling pawn.register(this)
 */
public void assignRoles() {
	for(Player player: playersList) 
	{
		player.drawCharacterCard();
		player.getPawn().register(this);
	}
	
}
public List<Player> getPlayers() {
	return playersList;
}
/**
 * Chooses randomly the first player among the players in the playerslist
 * sets the playerOnTurnIndex
 * sets the status of the choosen player to TurnStartedStatus
 */
public void chooseRandomlyFirstPlayer()
{
	Random r = new Random();
	playerOnTurnIndex=r.nextInt(playersList.size());
	Player first = getPlayerOnTurn();
	
	StringUpdate update = StringUpdate.broadcastMessage(first.getName()+" has been choosen as the first player");
	for(Player player: playersList) update.addPrivateMessage("Hello "+player.getName(), player.getName());
	notifyObservers(update);
	first.isYourTurn();
	first.setStatus(new TurnStartedStatus());
	first.notifyStatus();
}
}
