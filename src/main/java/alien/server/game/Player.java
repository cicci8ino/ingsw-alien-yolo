package alien.server.game;

import java.util.*;

import alien.server.card.*;
import alien.server.exception.CannotDefendException;
import alien.server.exception.EmptyDeckException;
import alien.server.exception.NotIncompleteStatusException;
import alien.server.game.effects.Effect;
import alien.server.game.effects.EffectAffectable;
import alien.server.game.effects.InstantEffect;
import alien.server.sector.MapClass;
import alien.server.sector.Position;
import alien.server.sector.Sector;
import alien.server.status.DeadStatus;
import alien.server.status.HasAttackedStatus;
import alien.server.status.HasMovedStatus;
import alien.server.status.IncompleteStatus;
import alien.server.status.Status;
import alien.server.status.TurnEndedStatus;
import alien.server.status.WaitingItemCard;
import alien.server.status.WonStatus;
import alien.server.util.StringUpdateObserver;
import alien.server.util.StringUpdate;
import alien.server.util.StringUpdateObservable;;
public class Player implements EffectAffectable,StringUpdateObservable{
	static public final int MAX_NUMBER_OF_ITEMS=3;

	private String name;
	private int turn;
	final private GameDecks decks;
	final private MapClass map;
	private List<ItemCard> itemCards= new ArrayList<ItemCard>();
	private List<Effect> effects= new ArrayList<Effect>();
	private List<ItemCard> defenseCards = new ArrayList<ItemCard>();
	private List<StringUpdateObserver> observers=new ArrayList<StringUpdateObserver>();
    private Pawn pawn;
    private Status status;
    /**
     * A player in the game. It can handle basic game actions like drawing cards and attacking sectors.
     * @param map Map where the player is playing
     * @param decks Decks of the match in witch the player is playing
     * @param name Name of this player
     */
	public Player(MapClass map, GameDecks decks, String name)
	{
		this.map=map;
		this.decks=decks;
		this.name=name;
		this.status=new TurnEndedStatus();
		turn=0;
	}
    /**
     * The player uses one of the ItemCards in his possession and puts it back in the itemCardsDeck
     * A boradcast message and a private message are notified
     * If the item cannot be used or the player's pawn cannot use items,a private message is notified
     * @param cardIndex Index of the item that has to be used
     */
	public void useItem(Integer cardIndex)
	{
		ItemCard item = itemCards.get(cardIndex.intValue());
		if(item.isUsable(getStatus())&&pawn.canUseItem())
		{
			notifyObservers(StringUpdate.broadcastMessage(getName()+" uses "+item.toString()));
			Effect effect = item.getEffect();
			effect.affect(this);
			effects.add(effect);
			itemCards.remove(item);
			decks.put(item);
			notifyObservers(StringUpdate.privateMessage("You used the item", getName()));
		}
		else
		{
			notifyObservers(StringUpdate.privateMessage("You can't use this item", getName()));
		}
	}
	/**
	 * Discards the choosen item card and puts it back in the itemCardDeck
	 * @param cardIndex
	 */
	public void discardItem(Integer cardIndex)
	{
		ItemCard item = itemCards.remove(cardIndex.intValue());
		defenseCards.remove(item);
		decks.put(item);
		notifyStatus();
		notifyObservers(StringUpdate.broadcastMessage(getName()+" discarded "+item.toString()));
	}

	/**
	 * Draws a sector card and resolve the effects. The drawn card is finally put back in the deck
	 */
	public void drawSectorCard()
	{
		SectorCard card;
		card= decks.drawSectorCard();
		notifyObservers(StringUpdate.privateMessage("You draw a "+card.toString(), name));
		card.activeEffect(this);
		decks.put(card);
		
	}
	/**
	 * Draws an escape card and resolve the effects. The card isn't put back in the deck
	 */
	public void drawEscapeCard()
	{
		EscapeCard card;
		card= decks.drawEscapeCard();
		if(card.isGreen())
		{
			escape();
		}
		else notifyObservers(StringUpdate.broadcastMessage("The escape hatch is broken! Shame on "+getName()));
	}
	/**
	 * Draw a character card and sets the player pawn
	 */
	public void drawCharacterCard()
	{
		CharacterCard card;
		card = decks.drawCharacterCard();
		pawn=card.getPawn(this);
	}
	/**
	 * Draws a itemCard. The associated instantEffect affects this player. 
	 * If the itemCards list size exceeds MAX_NUMBER_OF_ITEMS, 
	 * the current status is pushed and is set a new WaitingForCard status 
	 */
	public void drawItemCard() 
	{
		ItemCard card=null;
		try{
		card = decks.drawItemCard();
		itemCards.add(card);
		if(pawn.canUseItem())
		{
			InstantEffect effect = card.getInstantEffect();
			effect.affect(this);
		}
		notifyObservers(StringUpdate.privateMessage("You draw a "+card.toString(), name));
		if(itemCards.size()>MAX_NUMBER_OF_ITEMS)
		{
			IncompleteStatus nextStatus = new WaitingItemCard(getStatus());
			pushIncompleteStatus(nextStatus);
		}
		}
		catch(EmptyDeckException ex){notifyObservers(StringUpdate.privateMessage("There are no Item cards left", getName()));}

	}


	private void escape()
	{
		notifyObservers(StringUpdate.broadcastMessage(getName()+" succesfully escaped"));
		try{
		EscapePawn pawn = (EscapePawn) this.pawn;
		pawn.escape();}
		catch(ClassCastException ex){
			System.err.println("Error: this pawn cannot escape.");
		}
		setStatus(new WonStatus());
		notifyStatus();
	}
	/**
	 * Notifies a broadcast message declaring the death of this player's pawn.
	 * The pawn is removed from the sector it was in
	 * The current status is set to a new DeadStatus
	 */
	public void pawnKilled()
	{
		Pawn pawn = getPawn();
		Sector sector = map.getSector(pawn.getPosition());
		sector.pawnExit(pawn);
		setStatus(new DeadStatus());
		notifyStatus();
		notifyObservers(StringUpdate.playerKilled(name));
		
	}
	/**
	 * The turn number counter is incremented
	 */
	public void isYourTurn()
	{
		turn++;
	}
	/**
	 *Returns true if this player can play a turn
	 * @return true if this player can play a turn
	 */
	public boolean canPlay()
	{
		return status.canPlay();
	}
	/**
	 * Remove all effects 
	 */
	public void removeEffects()
	{
	    for(Effect effect:effects)	{
	    	effect.remove(this);
	    }
	    effects.clear();
	}
	/**Moves the pawn in the specified position
	 * Removes the pawn from the current sector it is in
	 * Adds the pawn in the sector realted to the specified position
	 * Changes the status of the player in HasMovedStatus
	 * If the destination is not reachable a private message is notified
	 * @param position Position where the pawn has to be moved
	 */
	public void move(Position destination)
	{
		Pawn pawn = getPawn();
		if(map.isReachable(destination, pawn)) 
		{
			map.getSector(pawn.getPosition()).pawnExit(pawn);
			Sector sectorDestination=map.getSector(destination);
			sectorDestination.pawnIsArrived(pawn);
			pawn.move(destination);
			setStatus(new HasMovedStatus());
			notifyStatus();
		}
		else
		{
			notifyObservers(StringUpdate.privateMessage("This position is not reachable", getName()));
		}
	}
	
	/**
	 * Returns true if the player can defend
	 * @return true if the player can defend
	 */
	public boolean canDefend()
	{
		return (!defenseCards.isEmpty());
	}
	/**
	 * Removes one of the defense cards
	 * The removed card is put back in the item deck
	 * @throws CannotDefendException if the player cannot defend a {@link CannotDefendException} is thrown
	 */
	public void defend() {
		if(!canDefend()) throw new CannotDefendException();
		ItemCard defenseCard=defenseCards.remove(0);
		itemCards.remove(defenseCard);
		decks.put(defenseCard);
		notifyObservers(StringUpdate.broadcastMessage(getName()+" uses a "+defenseCard.toString()+" to avoid the attack"));	
	}
	/**
	 * Sets the current status
	 * @param status The new status
	 */
	public void setStatus(Status status)
	{
		this.status=status;
	}
	public String getName() {
		return name;
	}
	/**
	 * Makes the pawn attack all the pawns present in its sector
	 * Sets a new HasAttackedStatus and notifies the attack
	 */
	public void attack() {
			Pawn pawn = getPawn();
			Sector attackedSector = map.getSector(pawn.getPosition());
			List<Pawn> attackedPawns= attackedSector.getPawns();
			attackedPawns.remove(pawn);
			pawn.attack(attackedPawns);
			setStatus(new HasAttackedStatus());
			notifyStatus();
			notifyObservers(StringUpdate.attackUpdate(name, pawn.getPosition()));
	}

	/**
	 * Returns true if the player's pawn is still alive
	 * @return true if the player's pawn is still alive
	 */
	public boolean isAlive()
	{
		return pawn.isAlive();
	}
	/**
	 * Notifies the current position of the pawn to all players
	 */
	public void declarePosition() {
		declarePosition(getPawn().getPosition());
	}
	/**
	 * Notifies the specified position as if it was the position of the pawn to all players
	 * @param position Position to declare
	 */
	public void declarePosition(Position position) {
		
		notifyObservers(StringUpdate.broadcastMessage(getName()+" announce to be in "+position.toString()));
	}
/**
 * Removes the current IncompleteStatus and sets the current status using the previous status
 * @throws NotIncompleteStatusException If the current status isn't a IncompleteStatus a {@link NotIncompleteStatusException} is thrown
 * @return Returns the popped status
 */
	public IncompleteStatus popIncompleteStatus() {
		try
		{
			IncompleteStatus status = (IncompleteStatus) getStatus();
			Status previousStatus = status.getPrevious();
			setStatus(previousStatus);
			notifyStatus();
			return status;
		}
		catch(ClassCastException ex)
		{
			throw new NotIncompleteStatusException();
		}
	}
	/**
	 * Sets the specified status as the current status and the current status becomes the previous status
	 * @param status IncompleteStatus to push
	 */
	public void pushIncompleteStatus(IncompleteStatus status)
	{
		status.setPrevious(this.status);
		this.status=status;
	}
	public void notifyStatus() {
		notifyObservers(StringUpdate.privateMessage(status.getMessage(), name));
	}

	public MapClass getMap() {
		return map;
	}
	@Override
	public Player getPlayer() {
		return this;
	}
	public void addDefenseItem(ItemCard defenseCard) {
		defenseCards.add(defenseCard);
		
	}
	public Status getStatus()
	{
		return status;
	}

	public Pawn getPawn()
	{
		return pawn;
	}
	public List<ItemCard> getCards() {
		
		return itemCards;
	}
	@Override
	public void register(StringUpdateObserver observer) {
		 if(observer == null) throw new NullPointerException("Null Observer");
	     if(!observers.contains(observer)) observers.add(observer);
	}
	@Override
	public void unregister(StringUpdateObserver observer) {
	    observers.remove(observer);
	}

	@Override
	public void notifyObservers(StringUpdate stringUpdate) {
		for (StringUpdateObserver obs : observers) {
	        obs.update(stringUpdate);
	    }
	}
	public int getTurn() {
			return new Integer(turn);
	}
}
