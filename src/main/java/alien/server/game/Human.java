package alien.server.game;

import alien.server.sector.Sector;

public class Human extends Pawn implements EscapePawn {
	private boolean escaped;
	/**
	 * A human pawn
	 * @param owner The player who owns this pawn
	 * @param name The name of this pawn
	 */
    public Human(Player owner, String name)
    {
    	super(name,owner, 1);
    	escaped=false;
    }
	@Override
	public boolean canUseItem() {
		return true;
	}
	
	
	@Override
	public boolean canAttack() {
		return false;
	}
	@Override
	public boolean canEnter(Sector sector) {
		return sector.canEnter(this);
	}

	@Override
	public void register(GameBoard gameBoard) {
	     gameBoard.registerHuman(this);
	}

	@Override
	public String toString() {
return "Human";
	}
	/**
	 * 	Set the escaped flag as true
	 */
	public void escape()
	{
		escaped=true;
	}
/**
 * Returns true if has been called the escaped method previously
 */
	public boolean isEscaped() {
		return escaped;
	}
@Override
public void unregister(GameBoard gameBoard) {
	gameBoard.unregister(this);
}
}
