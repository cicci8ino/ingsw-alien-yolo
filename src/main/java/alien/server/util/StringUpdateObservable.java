package alien.server.util;


public interface StringUpdateObservable {
	/**
	 * 	Registers a stringUpdateObserver in the observers list
	 * @param observer The observer to add
	 * @throws NullPointerException throws a {@link NullPointerException} when the specified observer is null 
	 */
    public void register(StringUpdateObserver obj);
    
    public void unregister(StringUpdateObserver obj);
    public void notifyObservers(StringUpdate stringUpdate);

}
