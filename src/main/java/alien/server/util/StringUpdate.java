package alien.server.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import alien.server.game.Player;
import alien.server.sector.Position;

public class StringUpdate {
	
	List<Position> eventPositions=new ArrayList<Position>();
	Boolean matchOver=false;
	String disconnectedPlayer=null;
	Map<String,List<String>> privateMessages= new HashMap<String,List<String>>();
	List<String> broadcastMessages= new ArrayList<String>();
	public StringUpdate()
	{
		
	}
	public StringUpdate(String message)
	{
		addBroadcastMessage(message);
	}
	public StringUpdate(String message, String playerName)
	{
		addPrivateMessage(message, playerName);
	}
	public void addBroadcastMessage(String message)
	{
		broadcastMessages.add(message);
	}
	public void addPrivateMessage(String message, String playerName)
	{
		List<String>messages;
		if(privateMessages.containsKey(playerName))
		{
			messages=privateMessages.get(playerName);
		}
		else {
			messages= new ArrayList<String>();
			privateMessages.put(playerName, messages);
		}
		messages.add(message);
	}
	public void addDisconnectedPlayer(Player player)
	{
		this.disconnectedPlayer=player.getName();
	}
	public void addEventPositions(List<Position> eventPositions)
	{
		this.eventPositions.addAll(eventPositions);
	}
	/**
	 * Genera un StringUpdate specifico per l'evento di attacco
	 * @param name Nome giocatore attaccante
	 * @param position Posizione in cui avviene l'attacco
	 * @return
	 */
	public static StringUpdate attackUpdate(String name, Position position) {
		 StringUpdate update= new StringUpdate();
		 update.addBroadcastMessage(name+" ha attaccato in "+position.toString());
		return update;
	}
	public static StringUpdate playerKilled(String name) {
		 StringUpdate update= new StringUpdate();
		 update.addBroadcastMessage(name+" è morto!");
		return update;
	}
	public static StringUpdate broadcastMessage(String broadcastMessage)
	{
		StringUpdate update = new StringUpdate();
		update.addBroadcastMessage(broadcastMessage);
	    return update;
	}
	public static StringUpdate privateMessage(String privateMessage, String playerName)
	{
		StringUpdate update = new StringUpdate();
		update.addPrivateMessage(privateMessage,playerName);
	    return update;
	}
	public static StringUpdate eventPositionMessage(List<Position>eventPositions, String broadCastMessage)
	{
		StringUpdate update = new StringUpdate();
		update.addBroadcastMessage(broadCastMessage);
		update.addEventPositions(eventPositions);
	    return update;
	}
	public static StringUpdate invalidAction(String playerName) {
		StringUpdate update = new StringUpdate();
		update.addPrivateMessage("Azione non valida!", playerName);
		return update;
	}
	public Map<String, List<String>> getPrivateMessages()
	{
		return privateMessages;
	}
	public List<Position> getEventPositions()
	{
		return eventPositions;
	}
	public List<String> getBroadcastMessages()
	{
		return broadcastMessages;
	}
	public Boolean getMatchOver()
	{
		return matchOver;
	}
  public String getDisconnectedPlayer()
  {
	  return disconnectedPlayer;
  }
public static StringUpdate victoryMessage(String message) {
	StringUpdate update = broadcastMessage(message);
	update.matchOver=true;
	return update;
}
}
