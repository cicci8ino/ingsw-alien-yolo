package alien.server.util;


public interface StringUpdateObserver {
	public void update(StringUpdate update);
}
