package alien.server.status;

import java.util.List;

import alien.common.RemoteStatus;
import alien.server.game.Pawn;
import alien.server.sector.MapClass;
import alien.server.sector.Position;

public class WaitingPosition extends IncompleteStatus {
	private EventPosition event;
    public WaitingPosition( EventPosition event) {
   		this.event=event;
	}
    public WaitingPosition(Status previousStatus, EventPosition event) {
    	super(previousStatus);
		this.event=event;
	}
	@Override
	public void accept(StatusVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public RemoteStatus getRemoteStatus() {
	     RemoteStatus remoteStatus = new RemoteStatus();
		   remoteStatus.setCanSelectPosition(true);
		   remoteStatus.setOnTurn(true);
	     return remoteStatus;
	}

	@Override
	public String getMessage() {
		return "Seleziona una posizione per fare rumore";
	}


	public void applyEvent(Position position)
	{
		event.apply(position);
	}
	
    @Override
    public List<Position> getUsablePositions(MapClass map, Pawn pawn)
    {
    	return event.getValidPositions(map, pawn);
    }
	@Override
	public boolean canPlay() {
		return true;
	}
}
