package alien.server.status;

import java.util.ArrayList;
import java.util.List;

import alien.server.game.Pawn;
import alien.server.game.Player;
import alien.server.sector.MapClass;
import alien.server.sector.Position;
import alien.server.util.StringUpdate;

public class NoiseEvent extends EventPosition {
	private Player player;
	public NoiseEvent(Player player)
	{
		this.player=player;
	}
	@Override
	public void apply(Position position) {
		player.declarePosition(position);
		List<Position> positions = new ArrayList<Position>();
		positions.add(position);
		player.notifyObservers(StringUpdate.eventPositionMessage(positions, "Noise in "+position.toString()));
	}
	@Override
	public List<Position> getValidPositions(MapClass map, Pawn pawn) {
		List<Position> validPositions= new ArrayList<Position>();
		for(Position position: map.getMappedPositions()) 
		  if(map.isValidPosition(position, pawn))  validPositions.add(position);
		return validPositions;
	}

}
