package alien.server.status;

import java.util.List;

import alien.server.game.Pawn;
import alien.server.sector.MapClass;
import alien.server.sector.Position;

public abstract class EventPosition {

	public abstract void apply(Position position);
	/**
	 * Trova tutte le posizioni in cui può essere applicato questo evento.
	 * @param map Mappa dove deve essere applicato
	 * @param pawn Pawn che applica l'effetto
	 * @return Lista posizioni dove può essere applicato questo evento.
	 */
	public abstract List<Position> getValidPositions(MapClass map, Pawn pawn);
}
