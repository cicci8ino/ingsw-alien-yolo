package alien.server.status;

import java.util.ArrayList;
import java.util.List;

import alien.common.RemoteStatus;
import alien.server.game.Pawn;
import alien.server.sector.MapClass;
import alien.server.sector.Position;

public class TurnEndedStatus extends Status{

	@Override
	public void accept(StatusVisitor visitor) {
		visitor.visit(this);
		
	}

	@Override
	public RemoteStatus getRemoteStatus() {
	     return new RemoteStatus();
	
	}

	@Override
	public String getMessage() {
		return "Turno terminato";
	}

	@Override
	public List<Position> getUsablePositions(MapClass map, Pawn pawn) {
		return new ArrayList<Position>();
	}

	@Override
	public boolean canPlay() {
		return true;
	}


	
	

}
