package alien.server.status;

import java.util.List;

import alien.server.game.Pawn;
import alien.server.game.Player;
import alien.server.sector.MapClass;
import alien.server.sector.Position;
import alien.server.sector.Sector;
import alien.server.util.StringUpdate;

public class SpotlightEvent extends EventPosition{
	private Player player;
	private MapClass map;
	public SpotlightEvent(Player player, MapClass map) {
		this.player=player;
		this.map=map;
	}
	@Override
	public void apply(Position position) {
		List<Position> illuminatedPositions = position.getAdjacentPositions();
		illuminatedPositions.add(position);
		for(Position illumPos: illuminatedPositions) 
		{
			Sector illuminatedSector = map.getSector(illumPos);
			if(illuminatedSector!=null)
			{
				for(Pawn pawn :illuminatedSector.getPawns())
				{
					pawn.spotted();
				}
			}
		}
		player.notifyObservers(StringUpdate.eventPositionMessage(illuminatedPositions, "Spotilight activated"));
	}
	@Override
	public List<Position> getValidPositions(MapClass map, Pawn pawn) {
		
		return map.getMappedPositions();
	}
	

}
