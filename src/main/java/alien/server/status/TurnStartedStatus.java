package alien.server.status;
import java.util.List;

import alien.common.RemoteStatus;
import alien.server.game.Pawn;
import alien.server.sector.MapClass;
import alien.server.sector.Position;

public class TurnStartedStatus extends Status {

	@Override
	public void accept(StatusVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public RemoteStatus getRemoteStatus() {
	     RemoteStatus remoteStatus = new RemoteStatus();
		   remoteStatus.setCanSelectPosition(true);
		   remoteStatus.setCanSelectItemCard(true);
		   remoteStatus.setOnTurn(true);
	     return remoteStatus;
	}

	@Override
	public String getMessage() {
		return "è il tuo turno";
	}

	@Override
	public List<Position> getUsablePositions(MapClass map, Pawn pawn) {
		return map.reachablePositions(pawn);
	}

	@Override
	public boolean canPlay() {
		return true;
	}


}
