package alien.server.status;

import java.util.ArrayList;
import java.util.List;

import alien.common.RemoteStatus;
import alien.server.game.Pawn;
import alien.server.sector.MapClass;
import alien.server.sector.Position;

public class DisconnectedStatus extends Status {

	@Override
	public void accept(StatusVisitor visitor) {
		
	}

	@Override
	public RemoteStatus getRemoteStatus() {
		return null;
	}

	@Override
	public String getMessage() {
		return null;
	}

	@Override
	public boolean canPlay() {
		return false;
	}

	@Override
	public List<Position> getUsablePositions(MapClass map, Pawn pawn) {
		return new ArrayList<Position>();
	}

}
