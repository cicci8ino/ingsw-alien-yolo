package alien.server.status;

public abstract class IncompleteStatus extends Status {
	private Status previous;
	public IncompleteStatus()
	{
	}
	public IncompleteStatus(Status previous)
	{
		this.previous=previous;
	}
	public void setPrevious(Status status)
	{
		this.previous=status;
	}
	public Status getPrevious()
	{
		return previous;
	}
}