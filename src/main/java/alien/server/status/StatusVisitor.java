package alien.server.status;

public interface StatusVisitor {
 public void visit(DeadStatus status);
 public void visit(HasAttackedStatus status);
 public void visit(HasMovedStatus status);
 public void visit(TurnEndedStatus status);
 public void visit(TurnStartedStatus status);
 public void visit(WaitingItemCard status);
 public void visit(WaitingPosition status);
 public void visit(WonStatus status);
}