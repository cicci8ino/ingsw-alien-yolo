package alien.server.status;
import java.util.List;

import alien.common.RemoteStatus;
import alien.server.game.Pawn;
import alien.server.sector.MapClass;
import alien.server.sector.Position;

public abstract class Status {

	public abstract void accept(StatusVisitor visitor);
	public abstract RemoteStatus getRemoteStatus();
	public abstract String getMessage();
	public abstract boolean canPlay();
	public abstract List<Position> getUsablePositions(MapClass map, Pawn pawn);

}
