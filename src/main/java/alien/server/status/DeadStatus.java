package alien.server.status;
import java.util.ArrayList;
import java.util.List;

import alien.common.RemoteStatus;
import alien.server.game.Pawn;
import alien.server.sector.MapClass;
import alien.server.sector.Position;

public class DeadStatus extends Status{

	@Override
	public void accept(StatusVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public RemoteStatus getRemoteStatus() {
	     RemoteStatus remoteStatus = new RemoteStatus();
	   	 remoteStatus.setDead(true);
	     return remoteStatus;
	}

	@Override
	public String getMessage() {
		return "You died!";
	}

	@Override
	public List<Position> getUsablePositions(MapClass map, Pawn pawn) {
		return new ArrayList<Position>();
	}


	@Override
	public boolean canPlay() {
		return false;
	}

}
