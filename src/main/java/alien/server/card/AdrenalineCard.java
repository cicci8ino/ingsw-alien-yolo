package alien.server.card;
import alien.server.game.effects.AdrenalineEffect;
import alien.server.game.effects.Effect;
import alien.server.game.effects.InstantEffect;
import alien.server.game.effects.NullInstantEffect;
import alien.server.status.TurnStartedStatus;
public class AdrenalineCard extends ItemCard{

	@Override
	public Effect getEffect() {
		return new AdrenalineEffect();
	}


	@Override
	public InstantEffect getInstantEffect() {
		return new NullInstantEffect(this);
	}
	@Override
	public void visit(TurnStartedStatus status) {
		setUsable(true);
	}
	@Override
	public String toString() {
		return "Adrenaline";
	}

}
