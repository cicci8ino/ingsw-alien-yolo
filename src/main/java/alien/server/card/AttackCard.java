package alien.server.card;
import alien.server.game.effects.AttackEffect;
import alien.server.game.effects.Effect;
import alien.server.game.effects.InstantEffect;
import alien.server.game.effects.NullInstantEffect;
import alien.server.status.HasMovedStatus;
import alien.server.status.TurnStartedStatus;
import alien.server.status.WaitingItemCard;
public class AttackCard extends ItemCard{

	@Override
	public Effect getEffect() 
	{
		return new AttackEffect();
	}

	@Override
	public InstantEffect getInstantEffect() {
		return new NullInstantEffect(this);
	}
	@Override
	public void visit(TurnStartedStatus status) {
		setUsable(true);
	}
	@Override
	public void visit(HasMovedStatus status) {
		setUsable(true);
	}
	@Override
	public void visit(WaitingItemCard status) {
		setUsable(true);
	}
	@Override
	public String toString() {
		return "Attack";
	}



}
