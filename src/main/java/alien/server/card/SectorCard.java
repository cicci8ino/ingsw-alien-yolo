package alien.server.card;


import alien.server.game.Player;


public abstract class SectorCard extends Card{
	protected final boolean drawItemCard;
	public abstract void activeEffect(Player player);
	public SectorCard(boolean drawItemCard)
	{
		this.drawItemCard=drawItemCard;
	}
}
