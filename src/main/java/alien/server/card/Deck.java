package alien.server.card;
import java.util.*;

import alien.server.exception.EmptyDeckException;
public class Deck <T extends Card>{
private final List <T> inUse = new ArrayList<T>();//lista delle carte da cui si pesca
private final List <T> used= new ArrayList<T>();//lista delle carte usate;
 public Deck()
 {
	 
	 
 }
 public void addCard(T card)
 {//aggiungi carta a inUse (da usare in creazione del mazzo)
	 inUse.add(card);
 }
 public void addDeck(Deck <T> deck)
 {//aggiunge tutte le carte di un deck a questo
	 inUse.addAll(deck.inUse);
	 used.addAll(deck.used);
 }
 public T drawCard()
 {//pesca una carta
	 if(inUse.isEmpty()) reshuffle();//se non ci sono carte da usare rifaccio il mazzo
	 T card = inUse.remove(0);
	 return card;
 }
 public void putCard(T card)
 {//quando il giocatore ripone la carta usata, o guardata
	 used.add(card);
 }
 /**
  * Mischia le carte ancora da usare
  */
 public void shuffle()
 {
	 Random r = new Random();
	 for(int i=0;i<inUse.size();i++)
		 inUse.add(inUse.remove(r.nextInt(inUse.size()-i)));
 }
 /**
  * Rimette tutte le carte usate in quelle da usare e le mischia
  */
 public void reshuffle()
 {//rifaccio il mazzo, svuoto la lista usate in ordine casuale e le aggiungo a inUse
	 if(used.isEmpty()&&inUse.isEmpty()) throw new EmptyDeckException();
	 while(!used.isEmpty())
		 inUse.add(used.remove(used.size()-1));
	 shuffle();
 }
 /**
  * Returns the total number of cards in this deck, used or inUse
  * @return total number of cards in this deck, used or inUse
  */
public int size() {
	return inUse.size()+used.size();
}
 
}
