package alien.server.card;

import alien.server.game.Pawn;
import alien.server.game.Player;


public abstract class CharacterCard extends Card
{
	private final String name;
	public CharacterCard(String name)
	{
		this.name=name;
	}
   public abstract Pawn getPawn(Player player);
   public String getName()
   {
	   return name;
   }
}
