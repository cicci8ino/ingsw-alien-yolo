package alien.server.card;

import alien.server.game.effects.Effect;
import alien.server.game.effects.InstantEffect;
import alien.server.status.DeadStatus;
import alien.server.status.HasAttackedStatus;
import alien.server.status.HasMovedStatus;
import alien.server.status.Status;
import alien.server.status.StatusVisitor;
import alien.server.status.TurnEndedStatus;
import alien.server.status.TurnStartedStatus;
import alien.server.status.WaitingItemCard;
import alien.server.status.WaitingPosition;
import alien.server.status.WonStatus;

public abstract class ItemCard extends Card implements StatusVisitor
{
	private Boolean usable;
	public abstract Effect getEffect();
	public abstract InstantEffect getInstantEffect();

    protected void setUsable(Boolean value)
    {
    	usable=value;
    }
	@Override
	public void visit(DeadStatus status) {
		setUsable(false);
	}

	@Override
	public void visit(HasAttackedStatus status) {
		setUsable(false);
	}

	@Override
	public void visit(HasMovedStatus status) {
		setUsable(false);
	}

	@Override
	public void visit(TurnEndedStatus status) {
		setUsable(false);
	}

	@Override
	public void visit(TurnStartedStatus status) {
		setUsable(false);
	}

	@Override
	public void visit(WaitingItemCard status) {
		setUsable(false);
	}

	@Override
	public void visit(WaitingPosition status) {
		setUsable(false);
	}

	@Override
	public void visit(WonStatus status) {
		setUsable(false);
	}
	public Boolean isUsable(Status status)
	{
		status.accept(this);
		return usable;
	}
}
