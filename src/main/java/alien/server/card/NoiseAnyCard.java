package alien.server.card;


import alien.server.game.Player;
import alien.server.status.EventPosition;
import alien.server.status.NoiseEvent;
import alien.server.status.WaitingPosition;

public class NoiseAnyCard extends SectorCard{

	public NoiseAnyCard(boolean drawItemCard) {
		super(drawItemCard);
	}
	
	@Override
	public void activeEffect(Player player) {
		
		if(drawItemCard) player.drawItemCard();
		EventPosition noiseEvent = new NoiseEvent(player);
		player.pushIncompleteStatus(new WaitingPosition(noiseEvent));
	}

	@Override
	public String toString() {
		return "Noise In Any Sector";
	}
}
