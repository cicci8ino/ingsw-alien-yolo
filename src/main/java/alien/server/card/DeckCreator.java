package alien.server.card;

import alien.server.exception.NegativeNumberOfPlayersException;
import alien.server.exception.TooManyPlayersException;

public final class DeckCreator
{
private DeckCreator(){};
private static final int NUMBEROF_NOISECARDS=10;
private static final int NUMBEROF_NOISEANYCARDS=10;
private static final int NUMBEROF_SILENCECARDS=5;
private static final int NUMBEROF_DRAWITEMCARDS=5;
public static final int NUMBEROF_SECTORCARDS = NUMBEROF_NOISEANYCARDS+NUMBEROF_NOISECARDS+NUMBEROF_SILENCECARDS;

private static final int NUMBEROF_ESCAPEGREENCARDS=3;
private static final int NUMBEROF_ESCAPEREDCARDS=3;
public static final int NUMBEROF_ESCAPECARDS=NUMBEROF_ESCAPEGREENCARDS+NUMBEROF_ESCAPEREDCARDS;


private static final int NUMBEROF_ADRENALINECARDS=2;
private static final int NUMBEROF_TELEPORTCARDS=2;
private static final int NUMBEROF_SPOTLIGHTCARDS=2;
private static final int NUMBEROF_SEDATIVECARDS=3;
private static final int NUMBEROF_ATTACKCARDS=2;
private static final int NUMBEROF_DEFENSECARDS=1;
public static final int NUMBEROF_ITEMS_CARDS=NUMBEROF_ADRENALINECARDS+
												NUMBEROF_ATTACKCARDS+
												NUMBEROF_DEFENSECARDS+
												NUMBEROF_TELEPORTCARDS+
												NUMBEROF_SPOTLIGHTCARDS+
												NUMBEROF_SEDATIVECARDS;
private static final int MAX_NUMBEROF_HUMANS=4;
private static final int MAX_NUMBEROF_ALIENS=4;
private static final int MAX_NUMBEROF_PLAYERS=MAX_NUMBEROF_ALIENS+MAX_NUMBEROF_HUMANS;
private static final String[] humansNames = {"Captain","Pilot","Soldier","Psychologist"};
private static final String[] aliensNames = {"First Alien","Second Alien","Third Alien","Fourth Alien"};


public static final Deck<SectorCard> generateSectorDeck()
{
	int i;
	Deck<SectorCard> deck=new Deck<SectorCard>();
    for(i=0;i<NUMBEROF_SILENCECARDS;i++) 
    	deck.addCard(new SilenceCard(false));
    for(i=0;i<NUMBEROF_NOISEANYCARDS;i++) 
    	deck.addCard(new NoiseAnyCard(i<NUMBEROF_DRAWITEMCARDS));
    for(i=0;i<NUMBEROF_NOISECARDS;i++) 
    	deck.addCard(new NoiseCard(i<NUMBEROF_DRAWITEMCARDS));
    deck.shuffle();
	return deck;
}
public static final Deck<ItemCard> generateItemDeck()
{
		
		int i;
		Deck<ItemCard> deck=new Deck<ItemCard>();
	    for(i=0;i<NUMBEROF_ADRENALINECARDS;i++) 
	    	deck.addCard(new AdrenalineCard());
	    for(i=0;i<NUMBEROF_TELEPORTCARDS;i++) 
	    	deck.addCard(new TeleportCard());
	    for(i=0;i<NUMBEROF_SPOTLIGHTCARDS;i++) 
	    	deck.addCard(new SpotlightCard());
	    for(i=0;i<NUMBEROF_SEDATIVECARDS;i++) 
	    	deck.addCard(new SedativesCard());
	    for(i=0;i<NUMBEROF_ATTACKCARDS;i++) 
	    	deck.addCard(new AttackCard());
	    for(i=0;i<NUMBEROF_DEFENSECARDS;i++) 
	    	deck.addCard(new DefenseCard());
	    deck.shuffle();
		return deck;
	}
public static final Deck<EscapeCard> generateEscapeDeck()
{
		
		int i;
		Deck<EscapeCard> deck=new Deck<EscapeCard>();
	    for(i=0;i<NUMBEROF_ESCAPEGREENCARDS;i++) 
	    	deck.addCard(new EscapeCard(EscapeCard.Color.GREEN));
	    for(i=0;i<NUMBEROF_ESCAPEREDCARDS;i++) 
	    	deck.addCard(new EscapeCard(EscapeCard.Color.RED));
	    deck.shuffle();
		return deck;
	}
public static final Deck<CharacterCard> generateCharacterDeck(Integer numberOfPlayers)
{
		if(numberOfPlayers<0) throw new NegativeNumberOfPlayersException("Non è ammesso un valore negativo di giocatori");
		if(numberOfPlayers>MAX_NUMBEROF_PLAYERS) throw new TooManyPlayersException("Il massimo numero di giocatori consentito è "+MAX_NUMBEROF_PLAYERS);
		int i;
		int numberOfHumans, numberOfAliens;
		numberOfHumans=numberOfPlayers/2;
		numberOfAliens=numberOfPlayers-numberOfHumans;
		Deck<CharacterCard> deck=new Deck<CharacterCard>();
	    for(i=0;i<numberOfHumans;i++) 
	    	deck.addCard(new HumanCard(humansNames[i]));
	    for(i=0;i<numberOfAliens;i++) 
	    	deck.addCard(new AlienCard(aliensNames[i]));
	    deck.shuffle();
		return deck;
		
	}

}
