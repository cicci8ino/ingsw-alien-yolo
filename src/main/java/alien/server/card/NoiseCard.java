package alien.server.card;


import java.util.ArrayList;
import java.util.List;

import alien.server.game.Player;
import alien.server.sector.Position;
import alien.server.util.StringUpdate;


public class NoiseCard extends SectorCard {
   
    
	public NoiseCard(boolean drawItemCard) {
		super(drawItemCard);
	}

	@Override
	public void activeEffect(Player player) {
		if(drawItemCard) player.drawItemCard();
		player.declarePosition();
		List<Position> positions=new ArrayList<Position>();
		positions.add(player.getPawn().getPosition());
		player.notifyObservers(StringUpdate.eventPositionMessage(positions, "Noise in "+player.getPawn().getPosition()));
	}

	@Override
	public String toString() {
		return "Noise In Your Sector";
	}

}
