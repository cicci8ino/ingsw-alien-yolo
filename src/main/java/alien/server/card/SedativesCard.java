package alien.server.card;
import alien.server.game.effects.Effect;
import alien.server.game.effects.InstantEffect;
import alien.server.game.effects.NullInstantEffect;
import alien.server.game.effects.SedativesEffect;
import alien.server.status.TurnStartedStatus;
public class SedativesCard extends ItemCard{

	@Override
	public Effect getEffect() {
		return new SedativesEffect();
		
	}
	@Override
	public void visit(TurnStartedStatus status) {
		setUsable(true);
	}

	@Override
	public InstantEffect getInstantEffect() {
		
		return new NullInstantEffect(this);
	}
	@Override
	public String toString() {
		return "Sedatives";
	}



}
