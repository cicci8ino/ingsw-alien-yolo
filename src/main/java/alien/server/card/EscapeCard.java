package alien.server.card;

public class EscapeCard extends Card
{
	private Color color;
        enum Color
        {
        	GREEN,RED;
        }
        public EscapeCard(Color color)
        {
        	this.color=color;
        }
        public boolean isGreen()
        {
        	if(color==Color.GREEN) return true;
        	return false;
        }
		@Override
		public String toString() {
			return color.toString()+" ESCAPE CARD";
		}
}