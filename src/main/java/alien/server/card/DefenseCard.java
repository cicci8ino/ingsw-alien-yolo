package alien.server.card;
import alien.server.game.effects.DefenseEffect;
import alien.server.game.effects.Effect;
import alien.server.game.effects.InstantEffect;
import alien.server.game.effects.NullEffect;
public class DefenseCard extends ItemCard{

	@Override
	public Effect getEffect() {
		return new NullEffect();
	}

@Override
public InstantEffect getInstantEffect() {
	return new DefenseEffect(this);
}
@Override
public String toString() {
	return "Defense";
}


}
