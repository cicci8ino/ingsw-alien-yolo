package alien.server.card;

public class GameDecks {
	  final Deck<SectorCard> sectorDeck=new Deck<SectorCard>();
	  final Deck<EscapeCard> escapeDeck=new Deck<EscapeCard>();
	  final Deck<ItemCard> itemDeck=new Deck<ItemCard>();
	  final Deck<CharacterCard> characterDeck=new Deck<CharacterCard>();
	  public GameDecks(int playerNumber)
	  {
		  generateDecks(playerNumber);
	  }
	  private void generateDecks(int playerNumber)
	  {//costruisce i 4 mazzi
		 
		   sectorDeck.addDeck(DeckCreator.generateSectorDeck());
		   //costruzione ItemDeck
		   itemDeck.addDeck(DeckCreator.generateItemDeck());
		   //costruzione EscapeDeck
		   escapeDeck.addDeck(DeckCreator.generateEscapeDeck());
		   //costruzione CharacterDeck
		   characterDeck.addDeck(DeckCreator.generateCharacterDeck(playerNumber));
	  }
	public void put(ItemCard item) {
		itemDeck.putCard(item);
	}

	public void put(SectorCard card) {
		sectorDeck.putCard(card);
	}
	public SectorCard drawSectorCard() {
		return sectorDeck.drawCard();
	}
	public EscapeCard drawEscapeCard() {
		return escapeDeck.drawCard();
	}
	public CharacterCard drawCharacterCard() {
		return characterDeck.drawCard();
	}
	public ItemCard drawItemCard() {
		return itemDeck.drawCard();
	}
	public int getItemCardDeckSize()
	{
		return itemDeck.size();
	}
}
