package alien.server.card;

import alien.server.game.Player;
public class SilenceCard extends SectorCard {

	public SilenceCard(boolean drawItemCard) {
		super(drawItemCard);
	}

	@Override
	public void activeEffect(Player player) {
		
	}

	@Override
	public String toString() {
		return "Silence";
	}

}
