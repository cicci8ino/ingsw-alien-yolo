package alien.server.card;
import alien.server.game.effects.Effect;
import alien.server.game.effects.InstantEffect;
import alien.server.game.effects.NullInstantEffect;
import alien.server.game.effects.TeleportEffect;
import alien.server.status.HasMovedStatus;
import alien.server.status.TurnStartedStatus;
import alien.server.status.WaitingItemCard;
public class TeleportCard extends ItemCard{
	@Override
	public Effect getEffect() 
	{
		return new TeleportEffect();
	}
	@Override
	public void visit(TurnStartedStatus status) {
		setUsable(true);
	}
	@Override
	public void visit(HasMovedStatus status) {
		setUsable(true);
	}
	@Override
	public void visit(WaitingItemCard status) {
		setUsable(true);
	}
	@Override
	public InstantEffect getInstantEffect() {
		return new NullInstantEffect(this);
	}

	@Override
	public String toString() {
		return "Teleport";
	}
}