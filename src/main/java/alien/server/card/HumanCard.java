package alien.server.card;
import alien.server.game.Human;
import alien.server.game.Pawn;
import alien.server.game.Player;

public class HumanCard extends CharacterCard{
	public HumanCard(String name)
	{
		super(name);
	}
	@Override
	public Pawn getPawn(Player player) {//retituisce una pedina di tipo umano
		
		return new Human(player,getName());
	}

	@Override
	public String toString() {
		return "Human Character Card";
	}


}
