package alien.server.card;

import alien.server.game.Pawn;
import alien.server.game.Alien;
import alien.server.game.Player;
public class AlienCard extends CharacterCard
{
	public AlienCard(String name)
	{
		super(name);
	}
	@Override
	public Pawn getPawn(Player player) {//retituisce una pedina di tipo alieno
		
		return new Alien(player,getName());
	}

	@Override
	public String toString() {
		return "Alien Card";
	}

}
