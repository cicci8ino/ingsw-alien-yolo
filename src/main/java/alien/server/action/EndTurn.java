package alien.server.action;

import alien.server.status.HasAttackedStatus;
import alien.server.status.HasMovedStatus;
import alien.server.status.TurnEndedStatus;

public class EndTurn extends BaseAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -225620773896250784L;
	public EndTurn()
	{
		
	}
	@Override
	public void visit(HasMovedStatus status) {
		endTurn();
	}
	@Override
	public void visit(HasAttackedStatus status) {
		endTurn();
	}
	private void endTurn()
	{
		getPlayerSubject().setStatus(new TurnEndedStatus());
		getPlayerSubject().notifyStatus();
		gameBoard.nextTurn();
	}
}
