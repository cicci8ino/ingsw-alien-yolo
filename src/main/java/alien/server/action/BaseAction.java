package alien.server.action;

import java.io.Serializable;
import alien.server.game.GameBoard;
import alien.server.game.Player;
import alien.server.status.*;
import alien.server.util.StringUpdate;

public abstract class BaseAction implements StatusVisitor, Serializable{

	private static final long serialVersionUID = 1L;
	protected GameBoard gameBoard;
	private String playerName;
	public void setPlayerName(String name)
	{
		this.playerName=name;
	}
	public String getPlayerName()
	{
		return playerName;
	}
	private void setGameBoard(GameBoard gameBoard)
	{
		this.gameBoard=gameBoard;
	}
	protected Player getPlayerSubject()
	{
		return gameBoard.getPlayerByName(getPlayerName());
	}
	public void applyTo(GameBoard gameBoard)
	{
		setGameBoard(gameBoard);
		getPlayerSubject().getStatus().accept(this);
	}
	@Override
	public void visit(DeadStatus status) {
		getPlayerSubject().notifyObservers(StringUpdate.invalidAction(getPlayerSubject().getName()));
	}
	@Override
	public void visit(HasAttackedStatus status) {
		getPlayerSubject().notifyObservers(StringUpdate.invalidAction(getPlayerSubject().getName()));
	}
	@Override
	public void visit(HasMovedStatus status) {
		getPlayerSubject().notifyObservers(StringUpdate.invalidAction(getPlayerSubject().getName()));
	}
	@Override
	public void visit(TurnEndedStatus status) {
		getPlayerSubject().notifyObservers(StringUpdate.invalidAction(getPlayerSubject().getName()));
	}
	@Override
	public void visit(TurnStartedStatus status) {
		getPlayerSubject().notifyObservers(StringUpdate.invalidAction(getPlayerSubject().getName()));
	}
	@Override
	public void visit(WaitingItemCard status) {
		getPlayerSubject().notifyObservers(StringUpdate.invalidAction(getPlayerSubject().getName()));
	}
	@Override
	public void visit(WaitingPosition status) {
		getPlayerSubject().notifyObservers(StringUpdate.invalidAction(getPlayerSubject().getName()));
	}
	@Override
	public void visit(WonStatus status) {
		getPlayerSubject().notifyObservers(StringUpdate.invalidAction(getPlayerSubject().getName()));
	}
	

	
	
}
