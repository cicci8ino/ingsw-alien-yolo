package alien.server.action;

import alien.server.game.Player;
import alien.server.status.HasMovedStatus;

public class Attack extends BaseAction{
    /**
	 * 
	 */
	private static final long serialVersionUID = 9072325782842435994L;
	public Attack()
    {
    	
    }
    @Override
    public void visit(HasMovedStatus status) {
    	Player playerSubject= getPlayerSubject();
    	if(playerSubject.getPawn().canAttack())
    	{
    		playerSubject.attack();
    		if(gameBoard.noHumansLeft()) gameBoard.aliensVictory();
    	}
    }


}
