package alien.server.action;

import alien.server.game.Player;
import alien.server.sector.Position;
import alien.server.status.TurnStartedStatus;
import alien.server.status.WaitingPosition;

public class PositionSelected extends BaseAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7222877846317368735L;
	private Position positionSelected;
	public PositionSelected(Position position) {
		this.positionSelected=position;
	}
   public Position getSelectedPosition()
   {
	   return positionSelected;
   }
	@Override
	public void visit(TurnStartedStatus status) {
		Player playerSubject=getPlayerSubject();
		playerSubject.move(positionSelected);
		playerSubject.getMap().getSector(positionSelected).playerStopsHere(playerSubject);
		if(gameBoard.noHumansLeft()) gameBoard.humansVictory();
	}
@Override
public void visit(WaitingPosition status) {
	Player playerSubject=getPlayerSubject();
	status.applyEvent(positionSelected);
	playerSubject.popIncompleteStatus();
	}
}
