package alien.server.action;

import alien.server.game.GameBoard;

public class Disconnected extends BaseAction {

	private static final long serialVersionUID = -4536277460025207384L;

@Override
public void applyTo(GameBoard gameBoard) {

	gameBoard.disconnectPlayer(getPlayerName());
}
}
