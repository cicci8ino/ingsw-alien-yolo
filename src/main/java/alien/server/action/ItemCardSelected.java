package alien.server.action;

import alien.server.game.Player;
import alien.server.status.HasMovedStatus;
import alien.server.status.TurnStartedStatus;
import alien.server.status.WaitingItemCard;

public class ItemCardSelected extends BaseAction{

/**
	 * 
	 */
	private static final long serialVersionUID = 3615919479235464915L;
	protected Integer cardIndex;
	public ItemCardSelected(Integer cardIndex) {
		this.cardIndex=cardIndex;
	}
	@Override
	public void visit(TurnStartedStatus status) {
		useItem();
	}
	@Override
	public void visit(HasMovedStatus status) {
		useItem();
	}
	@Override
	public void visit(WaitingItemCard status) {
		getPlayerSubject().popIncompleteStatus();
		useItem();
	}
	private void useItem()
	{
		Player playerSubject=getPlayerSubject();
		playerSubject.useItem(cardIndex);
	}
	public Integer getCardIndex() {
		return cardIndex;
	}

}
