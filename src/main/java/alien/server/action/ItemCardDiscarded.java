package alien.server.action;

import alien.server.status.HasMovedStatus;
import alien.server.status.TurnStartedStatus;
import alien.server.status.WaitingItemCard;
import alien.server.util.StringUpdate;

public class ItemCardDiscarded extends ItemCardSelected{
		/**
	 * 
	 */
	private static final long serialVersionUID = 5629700087599294640L;
		public ItemCardDiscarded(Integer cardIndex) {
			super(cardIndex);
		}
		public void visit(TurnStartedStatus status) {

			getPlayerSubject().notifyObservers(StringUpdate.invalidAction(getPlayerSubject().getName()));
		}
		@Override
		public void visit(HasMovedStatus status) {

			getPlayerSubject().notifyObservers(StringUpdate.invalidAction(getPlayerSubject().getName()));
		}
		@Override
		public void visit(WaitingItemCard status) {
			getPlayerSubject().discardItem(cardIndex);
			getPlayerSubject().popIncompleteStatus();
		}
		
	}