package alien.server.controller;
import java.util.Observable;
import java.util.Observer;

import alien.server.action.BaseAction;
import alien.server.game.GameBoard;
public class Controller implements Observer
{
GameBoard gameBoard;

public Controller(GameBoard gameBoard) {
this.gameBoard=gameBoard;
	}


@Override
public void update(Observable o, Object arg) {
	BaseAction action = (BaseAction)arg;
	action.applyTo(gameBoard); 
}


}
