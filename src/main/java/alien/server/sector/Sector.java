package alien.server.sector;
import java.util.ArrayList;
import java.util.List;

import alien.server.game.Alien;
import alien.server.game.Human;
import alien.server.game.Pawn;
import alien.server.game.Player;


public abstract class Sector{
	
	protected final List<Pawn> pawns=new ArrayList<Pawn>();
	private final Position position;
	public Sector(Position position) {

		this.position=position;
	}
	public abstract String toString();
	public abstract boolean canEnter(Human human);
	public abstract boolean canEnter(Alien alien);
	/**
	 * Cosa succede quando il player si ferma in questo settore
	 * @param player
	 */
	public void playerStopsHere(Player player)
	{
	}
	public boolean canEnter(Pawn pawn)
	{
		return pawn.canEnter(this);
	}
	public void pawnIsArrived(Pawn pawn)
	{//quando arriva aggiungo il pawn alla lista 
		pawns.add(pawn);
	}
	public void pawnExit(Pawn pawn)
	{
		pawns.remove(pawn);
	}
		
	
	public Position getPosition() {
		return position;
	}


	public List<Pawn> getPawns() {
		return new ArrayList<Pawn>(pawns);
		
	}
}
