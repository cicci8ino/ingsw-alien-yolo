package alien.server.sector;

import alien.server.game.Alien;
import alien.server.game.Human;
import alien.server.game.Player;

public class DangerousSector extends Sector{
		
	
	public DangerousSector(Position position) {
		super(position);
	}



	@Override
	public void playerStopsHere(Player player) {
	   if(!player.getPawn().isSilent())
	   {
		   player.drawSectorCard();
		   player.notifyStatus();
	   }
	}



	@Override
	public boolean canEnter(Human human) {
		return true;
	}



	@Override
	public boolean canEnter(Alien alien) {
		return true;
	}



	@Override
	public String toString() {
		return "D";
	}

}
