package alien.server.sector;

import alien.server.game.Alien;
import alien.server.game.Human;

public class AlienSector extends Sector{
	public AlienSector(Position position) {
		super(position);
	}

	@Override
	public boolean canEnter(Human human) {
		return false;
	}

	@Override
	public boolean canEnter(Alien alien) {
		return false;
	}

	@Override
	public String toString() {
		return "A";
	}






}
