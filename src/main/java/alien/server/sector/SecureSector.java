package alien.server.sector;

import alien.server.game.Alien;
import alien.server.game.Human;

public class SecureSector extends Sector {
	public SecureSector(Position position) {
		super(position);
	}

	@Override
	public boolean canEnter(Human human) {
		return true;
	}

	@Override
	public boolean canEnter(Alien alien) {
		return true;
	}

	@Override
	public String toString() {
		return "S";
	}
}
