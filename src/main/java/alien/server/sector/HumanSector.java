package alien.server.sector;

import alien.server.game.Alien;
import alien.server.game.Human;

public class HumanSector extends Sector {
	 
public HumanSector(Position position)
{
	super(position);
}


	@Override
	public boolean canEnter(Human human) {
		return false;
	}

	@Override
	public boolean canEnter(Alien alien) {
		return false;
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "H";
	}

}
