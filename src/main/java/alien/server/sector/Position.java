package alien.server.sector;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import alien.server.sector.Position;

 final public class Position implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9163231221859153188L;
	private final int row,column;
	
	public Position(int row, int column) {

		this.row = row;	
		this.column = column;
	}

	public int getColumn() {
		return column;
	}
	public int getRow() {
		return row;
	}
	public String getColumnString() {
		return Character.toString((char) (column+64));
	}
	public String getRowString() {
		if (row<10) return "0"+row;
		return String.valueOf(row);
	}
	/**
	 * Trova le posizioni adiacenti a questa. 
	 * @return Lista posizioni adiacenti. (ArrayList)
	 */
	public List<Position> getAdjacentPositions()
	{
		List <Position>positions=new ArrayList<Position>();
		positions.add(new Position(row, column-1));
		positions.add(new Position(row, column+1));
		positions.add(new Position(row-1, column));
		positions.add(new Position(row+1, column));
		if(column%2==0)
		{
			positions.add(new Position(row+1, column-1));
			positions.add(new Position(row+1, column+1));
		}
		else
		{
			positions.add(new Position(row-1, column-1));
			positions.add(new Position(row-1, column+1));
		}
		return positions;
	} 
	
	@Override
	public Position clone() {
		return new Position(this.row, this.column);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}
	
	@Override
	public String toString()
	{
		return getColumnString()+getRowString();

	}
	
	//TODO Pulire codice

}
