package alien.server.sector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import alien.server.game.Alien;
import alien.server.game.Human;
import alien.server.game.Pawn;

public class MapGenerator {
	private final static int ESCAPE_SECTORS=4;
	private double probabilityOfVoidSector=0.2;
	private double safeSectorRatio=0.3;
	private double minDistanceBetweenEscapeSectors=6;
	private Random r;
	private int escapeSectorsCreated;
	private int numberOfIterations;
	List<Position> notVisitedPositions;
	List<Position> unaccessiblePositions;
	List<Position> visitedPositions;
	public MapGenerator()
	{
		r=new Random();
	}
	public MapClass getRandomMap()
	{
		MapClass map=new MapClass();
		notVisitedPositions=new ArrayList<Position>();
		unaccessiblePositions= new ArrayList<Position>();;
		visitedPositions= new ArrayList<Position>();
		
        setStartingPositions(map);
		
		visitedPositions.add(map.getAlienPosition());
		visitedPositions.add(map.getHumanPosition());

		escapeSectorsCreated=0;
		numberOfIterations=0;
		
		do
		{
			numberOfIterations++;
			notVisitedPositions= findAdjacentNotVisitedPosition();
			map.putAllSectors(createRandomSectors());
			notVisitedPositions.removeAll(unaccessiblePositions);
			visitedPositions.addAll(notVisitedPositions);
		}while(!notVisitedPositions.isEmpty());
		
		applyAdditionalRules(map);
		createEscapeSectorsNumber(map);
		//System.out.println(escapeSectorsCreated);
		return map;
	}
	/**
	 * Aggiunge regole addizionali di buon formazione della mappa
	 * @param map mappa da modificare
	 */
	private void applyAdditionalRules(MapClass map) {
		outdistanceAliensFromHumans(map,3);
		
	}
private void createEscapeSectorsNumber(MapClass map) {
	//cerco di creare il numero giusto di escape sector nelle posizioni più distanti
	int depth=mapDepth(map);
	int i=depth;
	Pawn human = new Human(null,"");
	human.setStartingPosition(map.getHumanPosition());
	List<Position> usedPositions= new ArrayList<Position>();
	while(i>2&&escapeSectorsCreated<ESCAPE_SECTORS)
	{
		human.setNumberOfSteps(i);
		List<Position> candidatePositions= map.reachablePositions(human);
		human.setNumberOfSteps(i-1);
		candidatePositions.removeAll(map.reachablePositions(human));
		while(escapeSectorsCreated<ESCAPE_SECTORS&&(!candidatePositions.isEmpty()))
		{
			
			Position choosen =mostDistantFromOthers(candidatePositions,usedPositions);

			candidatePositions.remove(choosen);
			if(choosen!=null)
			{
				escapeSectorsCreated++;
			map.putSector(new EscapeSector(choosen, escapeSectorsCreated));
			usedPositions.add(choosen);
			}
			else candidatePositions.clear();
		}
		i--;
	}
	
	//se non sono riuscito a crearne abbastanza provo a crearli dove ci sono le posizioni vuote
	   while(escapeSectorsCreated<ESCAPE_SECTORS&&(!unaccessiblePositions.isEmpty()))
	   {
		   Position escapePosition = unaccessiblePositions.remove(r.nextInt(unaccessiblePositions.size()));
		   escapeSectorsCreated++;
		   map.putSector(new EscapeSector(escapePosition, escapeSectorsCreated));
		   
	   }
	
	}
private Position mostDistantFromOthers(List<Position> candidatePositions,
		List<Position> usedPositions) {
	//trova la posizione con distanza massima rispetto alla posizione di usedPositions più vicina
	
	if(usedPositions.isEmpty()) return candidatePositions.get(r.nextInt(candidatePositions.size()));
int maxDistance=0;
	Position mostDistant=candidatePositions.get(0);
	for(Position choosen: candidatePositions)
    {
	//trovo distanza minima rispetto a posizioni in usedPositions
		
	int minDistance=Integer.MAX_VALUE;
	for(Position used:usedPositions)
	{
		int deltaRow=used.getRow()-choosen.getRow();
		int deltaColumn=used.getColumn()-choosen.getColumn();
		int absoluteDistance;
		absoluteDistance=deltaRow*deltaRow+deltaColumn*deltaColumn;
		if(minDistance>absoluteDistance)
		{
			minDistance=absoluteDistance;
		}
	}
	if(maxDistance<minDistance)
	{
		maxDistance=minDistance;
		mostDistant=choosen;
	}
}
	if(maxDistance>minDistanceBetweenEscapeSectors)
	return mostDistant;
	return null;
}
private int mapDepth(MapClass map)
{
	int depth=MapClass.ROWS+MapClass.COLUMNS;
	Pawn human = new Human(null,"");
	human.setStartingPosition(map.getHumanPosition());
	human.setNumberOfSteps(depth);
	List<Position> allPositions= map.reachablePositions(human);
	List<Position> nearest;
	
	do{
		depth--;
		human.setNumberOfSteps(depth);
		nearest=map.reachablePositions(human);
	}while(nearest.containsAll(allPositions)&&depth>0);
		return depth+1;
}
/**
 * Crea una zona di settori vuoti tra la posizione di partenza degli umani e quella degli alieni
 * @param map mappa da modificare
 * @param distance estensione della zona vuota
 */
	private void outdistanceAliensFromHumans(MapClass map,int distance) {
		int i;
		List<Position> reachableFromHumansPositions,reachableFromAliensPositions;
		Pawn human,alien;
		human=new Human(null,"");
		human.setStartingPosition(map.getHumanPosition());
		alien=new Alien(null,"");
		alien.setStartingPosition(map.getAlienPosition());
		for(i=1;i<distance;i++)
		{
			human.setNumberOfSteps(i);
			alien.setNumberOfSteps(i);
			reachableFromHumansPositions=map.reachablePositions(human);
			reachableFromAliensPositions=map.reachablePositions(alien);
			for(Position position: reachableFromHumansPositions)
			{
				if(reachableFromAliensPositions.contains(position)) map.removeSector(position);
			}
		}
		
	}

	private boolean randomlyChooseIfVoid() {
		Double dinamicVoidProbability;//probabilità di vuoto incrementa con le iterazioni
		dinamicVoidProbability=probabilityOfVoidSector-0.4+0.08*((double)numberOfIterations);
		if(r.nextDouble()<dinamicVoidProbability) return true;
		return false;
	}

	/**
	 * Data una lista di posizioni, crea una lista di settori in quelle posizioni con tipo dinamico random, oppure settori vuoti. Non tutte le posizioni diventano settori data la possibilità di generare settori vuoti
	 * @param positions posizioni da trasformare in settori
	 * @param voidPositions posizioni vuote da aggiornare
	 * @return Lista di settori di tipo random
	 */
	private List<Sector> createRandomSectors()
	{
		List<Sector>sectors=new ArrayList<Sector>();
		for(Position position: notVisitedPositions)
		{
		    if(randomlyChooseIfVoid()) unaccessiblePositions.add(position);
		    else sectors.add(createRandomSector(position));
		}
		return sectors;
	}
	/**
	 * 
	 * Trova le posizioni adiacenti a visitedPositions; non include le posizioni già presenti in visitedPostions; non include più volte la stessa posizione; esclude le posizioni non valide; esclude posizioni di settori vuoti
	 * @param visitedPositions Posizioni visitate da cui calcolare le adiacenze
	 * @param voidPositions Posizioni di settori vuoti inaccessibili che sono da escludere
	 * @return lista delle posizioni adiacenti non visitate
	 */
	private List<Position> findAdjacentNotVisitedPosition()
	{
		List<Position> notVisitedPositions= new ArrayList<Position>();
		for(Position position: visitedPositions)
		{//per ogni posizione visitata calcolo gli adiacenti e li metto in not visited position;
			List<Position> adjacentPositions=position.getAdjacentPositions();
			for(Position adjacent: adjacentPositions) 
			 if(!notVisitedPositions.contains(adjacent)&&!visitedPositions.contains(adjacent)&&!unaccessiblePositions.contains(adjacent)&&MapClass.isValidPosition(adjacent))
				 notVisitedPositions.add(adjacent);
		}
		return notVisitedPositions;
	}
	/**
	 * Imposta le posizioni di partenza della mappa
	 * @param map mappa da modificare
	 */
	private void setStartingPositions(MapClass map)
	{
		Position center=getRandomCenter();
		
		map.setAlienPosition(getRandomAlienStartingPosition(center));
		map.setHumanPosition(getRandomHumanStartingPosition(center));
	}
	private Sector createRandomSector(Position sectorPosition)
	{
		if(r.nextDouble()<safeSectorRatio) return new SecureSector(sectorPosition);
		return new DangerousSector(sectorPosition);
	}
	/**
	 * Restituisce una posizione centrale alla mappa, ma con coordinata row casualmente scelta nel secondo terzo della mappa
	 * @return Posizione centrale alla mappa con riga pseudocasuale
	 */
	private Position getRandomCenter()
	{
		int centerRow,centerCol;
		centerRow=r.nextInt(MapClass.ROWS/3)+MapClass.ROWS/3+1;
		centerCol=MapClass.COLUMNS/2+1;
		Position center=new Position(centerRow,centerCol);
		return center;
	}
	private Position getRandomHumanStartingPosition(Position center)
	{
		int row,col;

		row= center.getRow()+1+r.nextInt(2);
		col=center.getColumn();
		return new Position(row,col);
	}
	private Position getRandomAlienStartingPosition(Position center)
	{
		int row,col;

		row= center.getRow()-r.nextInt(2);
		col=center.getColumn();
		return new Position(row,col);
	}
	
}
