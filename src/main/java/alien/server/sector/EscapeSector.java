package alien.server.sector;

import alien.server.game.Alien;
import alien.server.game.Human;
import alien.server.game.Player;

public class EscapeSector extends Sector {
    private boolean usable;//se è ancora aperta = true, se usata o danneggiata diventa false
    private final int number;
    public EscapeSector(Position position, int number)
    {
    	super(position);
    	this.number=number;
    	usable=true;
    }
	@Override
	public void playerStopsHere(Player player) {
       //notifica tutti dell'arrivo
		if(usable)
		{
			usable=false;
			player.drawEscapeCard();//fa pescare una carta scialuppa

		}
	}

	public boolean isUsable() {
		return usable;
	}
	public int getNumber() {
		return number;
	}
	@Override
	public boolean canEnter(Human human) {
		
		return isUsable();
	}
	@Override
	public boolean canEnter(Alien alien) {
		return false;
	}
	@Override
	public String toString() {
		return String.valueOf(number);
	}
	
	

}
