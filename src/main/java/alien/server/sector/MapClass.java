package alien.server.sector;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alien.server.exception.SectorOutOfBoundException;
import alien.server.game.Pawn;
import alien.server.sector.Position;

public class MapClass{
	
	final private Map<Position, Sector> mapStructure = new HashMap<Position, Sector>();
	static final public Integer COLUMNS=23,ROWS=14;
    private Position humanPosition=null;
	private Position alienPosition=null;
	public MapClass() 
	{
		
		
	} 

	public List<Position> reachablePositions(Pawn pawn) {

		Map<Position, Integer> distancesFromStartingPosition = new HashMap<Position, Integer>();//mappa delle distanze rispetto alla posizione iniziale
		List<Position> notVisitedPositions = new ArrayList<Position>(); //lista nodi ancora da raggiungere
		List<Position> visitedPositions = new ArrayList<Position>(); //lista nodi già analizzati
		Position start = pawn.getPosition(); //posizione iniziale giocatore
		Position nearestNode = null;
		int steps = pawn.getNumberOfSteps();	
		int minimalDistance=0;
		
		distancesFromStartingPosition.put(start, 0); // imposta a zero la distanza da start
		notVisitedPositions.add(start);
		
		while (!notVisitedPositions.isEmpty())//se non ci sono più posizioni da visitare la destinazione non è raggiungibile
		{
			nearestNode = findNearestNode(notVisitedPositions, distancesFromStartingPosition);
			minimalDistance=distancesFromStartingPosition.get(nearestNode); 
			notVisitedPositions.remove(nearestNode); //rimuovo nearest node dalla lista
			visitedPositions.add(nearestNode);//lo aggiungo alla lista dei visitati
			
			if(minimalDistance<steps){
				List<Position> adjacentsToNearest =getAdjacentValidPositions(nearestNode, pawn);//lista con posizioni adiacenti a nearest node
				adjacentsToNearest.removeAll(visitedPositions);//non analizzo quelle già visitate
			
			for (Position adjacent:adjacentsToNearest) 
			{
					if(!distancesFromStartingPosition.containsKey(adjacent)||distancesFromStartingPosition.get(adjacent)>minimalDistance + 1)
					{//se ho scoperto un modo migliore per raggiungerlo aggiorno la distanza e lo metto tra quelli da visitare
						distancesFromStartingPosition.put(adjacent, minimalDistance+1);
						if(!notVisitedPositions.contains(adjacent)) notVisitedPositions.add(adjacent);
					}
				}
			}
		}
		visitedPositions.remove(start);
		return visitedPositions;
	}
/**
 * Controlla se la destinazione è raggiungibile dal pawn
 * @param destination
 * @param pawn
 * @return
 */
	public boolean isReachable(Position destination, Pawn pawn) {
		if (!isValidPositionToMove(destination, pawn)) 
			return false; 
		List<Position> reachablePositions = reachablePositions(pawn);
		for(Position position: reachablePositions) if(position.equals(destination)) return true;
		return false;
	}
	public Position getHumanPosition()
	{
		return humanPosition;
	}
	public Position getAlienPosition()
	{
		return alienPosition;
	}

	public void setHumanPosition(Position humanPosition) {
		if(this.humanPosition!=null)removeSector(this.humanPosition);
		HumanSector humanSector = new HumanSector(humanPosition);
		putSector(humanSector);
		this.humanPosition = humanPosition;
	}

	public void setAlienPosition(Position alienPosition) {
		if(this.alienPosition!=null)removeSector(this.alienPosition);
		AlienSector alienSector = new AlienSector(alienPosition);
		putSector(alienSector);
		this.alienPosition = alienPosition;
	}
/**
 * trova la posizione con distanza minima tra le posizioni in positions
 * @param positions Posizioni da analizzare
 * @param distances mappa delle distanze
 * @return
 */
	private Position findNearestNode(List<Position> positions, Map <Position, Integer> distances)
	{
		
		if(positions.isEmpty()) return null;
		
		Position nearestNode=positions.get(0);
		int min=distances.get(nearestNode);
		for (Position position:positions) {
			if (distances.get(position)<min) { 
				min=distances.get(position); 
				nearestNode=position;
			}
		}
		return nearestNode;
	}
	/**Controllo di validità della posizione in riferimento al pedone. 
	 * 
	 * @param position Posizione da controllare
	 * @param pawn Pedone da controllare
	 * @return True se la posizione è valida per il pedone
	 */
    public boolean isValidPosition(Position position, Pawn pawn)
	{
		if(!isValidPosition(position))
			return false;
		if(!mapStructure.containsKey(position))
			return false;
		if(!mapStructure.get(position).canEnter(pawn))
			return false;
		return true;
	}
    public boolean isValidPositionToMove(Position position, Pawn pawn)
	{
		if(!isValidPosition(position,pawn))
			return false;
		if(position.equals(pawn.getPosition()))
			return false;
		return true;
	}
	/**
	 * Controlla che la posizione non esca dai bordi della mappa
	 * @param position Posizione da validare
	 * @return True se la posizione è ammissibile
	 */
	public static boolean isValidPosition(Position position)
	{
		if(position.getColumn()>COLUMNS)
			return false;
		if(position.getColumn()<1) 
			return false;
		if(position.getRow()>ROWS)
			return false;
		if(position.getRow()<1) 
			return false;
		return true;
	}
	/**
	 * Put the specified sector in the map structure. 
	 *  If the map previously contained a sector in the position of the specified sector, 
	 *  the old sector is replaced by the specified sector
	 *  @throws SectorOutOfBoundException If the specified sector is null or its position is out of map bounds
	 * @param sector
	 */
	public void putSector(Sector sector)
	{
		if(sector==null) throw new SectorOutOfBoundException("Sector cannot be null");
		if(!isValidPosition(sector.getPosition())) 
			throw new SectorOutOfBoundException("Sector position"+sector.getPosition().toString()+" is not valid");
		
		mapStructure.put(sector.getPosition(), sector);
	}
	public void removeSector(Position position)
	{
			mapStructure.remove(position);
	}


	public List<Position> getAdjacentValidPositions(Position position, Pawn pawn)
	{//ritorna posizioni adiacenti a position valide per pawn
		List<Position> adjacentValidPositions = new ArrayList<Position>();
		for(Position pos:position.getAdjacentPositions())
		{
			if(isValidPositionToMove(pos, pawn)) adjacentValidPositions.add(pos);
		}
		return adjacentValidPositions;
	}
	public void putAllSectors(List<Sector>sectors) {
	    for(Sector sector: sectors)
	    {
	    	putSector(sector);
	    }
	}
	@Override
	public String toString()
	{
		String s="";
		int r,c;
		
		for(r=1;r<=ROWS;r++)
		{
			for(c=1;c<=COLUMNS;c++)
			{
				Position tmp = new Position(r, c);
				if(!mapStructure.containsKey(tmp)) s=s+"N";
				else s=s+mapStructure.get(tmp).toString();
			}
			s=s+"\r\n";
		}
		return s;
	}

	public Sector getSector(Position position) {
		return mapStructure.get(position);
		
	}

	public List<Position> getMappedPositions() {
		List<Position> mappedPositions = new ArrayList<Position>(mapStructure.keySet());
		return mappedPositions;
	}
}
	
	
