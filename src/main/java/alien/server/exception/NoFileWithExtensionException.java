package alien.server.exception;

public class NoFileWithExtensionException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3691084254258596466L;

	public NoFileWithExtensionException(String message) {
		super(message);
	}

}
