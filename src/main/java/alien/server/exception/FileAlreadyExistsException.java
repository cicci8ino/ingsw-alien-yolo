package alien.server.exception;

public class FileAlreadyExistsException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6431565691255682441L;

	public FileAlreadyExistsException(String message) {
		super(message);
	}

}
