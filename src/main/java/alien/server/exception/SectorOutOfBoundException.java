package alien.server.exception;

public class SectorOutOfBoundException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4267909509827464553L;

	public SectorOutOfBoundException(String message) {
		super(message);
	}
}
