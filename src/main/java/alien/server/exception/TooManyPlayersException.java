package alien.server.exception;

public class TooManyPlayersException extends RuntimeException {
/**
	 * 
	 */
	private static final long serialVersionUID = -4983504796472436056L;

public TooManyPlayersException(String stringMessage) {
	super(stringMessage);
	
}

}
