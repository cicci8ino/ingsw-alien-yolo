package alien.server.exception;

public class MalformedMapException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 385755508940704208L;

	public MalformedMapException(String message) {
		super(message);
	}
	
}
