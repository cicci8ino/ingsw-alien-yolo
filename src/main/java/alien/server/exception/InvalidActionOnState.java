package alien.server.exception;

public class InvalidActionOnState extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 635334102050920977L;

	public InvalidActionOnState(String message) {
		super(message);
	}
	
}
