package alien.server.exception;

public class FolderNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 47968998679205608L;

	public FolderNotFoundException(String message) {
		super(message);
	}
	
}
