package alien.server.exception;

public class NegativeNumberOfPlayersException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4938808288179127750L;

	public NegativeNumberOfPlayersException(String s)
	{
		super(s);
	}

}
