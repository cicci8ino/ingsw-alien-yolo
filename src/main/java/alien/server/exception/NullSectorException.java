package alien.server.exception;

public class NullSectorException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2640954245082421397L;

	public NullSectorException(String message) {
		super(message);
	}
}