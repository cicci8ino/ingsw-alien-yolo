package alien.server.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Observable;
import alien.common.Name;

public class ClientRegistration extends Observable implements Runnable{
    private String playerName;
    private Socket socket;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private List<String> namesTaken;
    
	public ClientRegistration(Socket socket, List<String> namesTaken) {
		this.socket=socket;
		this.namesTaken=namesTaken;
	}
/**
 * Richiede connessione a ServerSocket della chat poi
 * Continua a chiedere il nome fino a quando ne ottiene uno non ancora presente in namesTaken
 * poi avverte networkManager tramite notifica del nuovo client pronto a giocare;
 */
	@Override
	public void run() {
		try{
			  in = new ObjectInputStream(socket.getInputStream());
	          out = new ObjectOutputStream(socket.getOutputStream());
	          System.out.println("REGISTRATION:Registrazione in corso: richiedo nome");
	          requestName();
	          acceptName();

		}
		catch(IOException ioex) {
		}
		catch (ClassNotFoundException ex){
		}
	}
	
	private void acceptName()
	{
		 try{
			 sendNameAccepted();
			 addClient();
    	 }
    	 catch(IOException ex){
    		 synchronized(namesTaken)
    		 {
    			 namesTaken.remove(playerName);
    		 }
    	 }
	}
	private void requestName() throws IOException,ClassNotFoundException
	{
			boolean nameChoosen=false;
              while (!nameChoosen) {
            	 
            		  System.out.println("REGISTRATION:Richidendo nome");
            	      playerName=  readName();
            	      System.out.println("REGISTRATION:Richiesto nome:"+playerName);
            	      synchronized (namesTaken) 
            	      {
            	    	  if(namesTaken.contains(playerName)) nameChoosen=false;
            	    	  else
            	    	  {
            	    		  nameChoosen=true;
            	    		  namesTaken.add(playerName);
            	    	  }
            	      }
            	      if(!nameChoosen)  sendNameNotAccepted();
            	  }
            	  
              
	}
	private void addClient() throws IOException
	{
		SocketClientHandler client = new SocketClientHandler(socket, in, out, playerName);
		setChanged();
		notifyObservers(client);
	}
	private String readName() throws ClassNotFoundException, IOException {
		return ((Name)in.readObject()).getName();
	}
	
	private void sendNameAccepted() throws IOException {

		  System.out.println("REGISTRATION:nome accettato");
		out.writeObject(new Boolean(true));
		out.flush();
	}
	private void sendNameNotAccepted() throws IOException {

		  System.out.println("REGISTRATION:nome rifiutato");
		out.writeObject(new Boolean(false));
		out.flush();
	}

}
