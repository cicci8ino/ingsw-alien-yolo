package alien.server.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import alien.common.ChatMessage;

public class ChatHandler implements Runnable {
	 Socket socket;
	 List<ChatHandler> chatHandlers;
	 ObjectInputStream in;
	 ObjectOutputStream out;
	public ChatHandler(Socket socket,List<ChatHandler> chatHandlers) throws IOException {
		this.socket=socket;
		this.chatHandlers=chatHandlers;
		in = new ObjectInputStream(socket.getInputStream());
		out = new ObjectOutputStream(socket.getOutputStream());
	}

	@Override
	public void run() {
		try
		{
			while(true)
			{
				
				ChatMessage chatMessage = (ChatMessage) in.readObject();
				broadcastMessage(chatMessage);
			}
		}
		catch(Exception ex){
			System.out.println("Errore su chat:disconnessione" );
			try
			{
				in.close();
				out.close();
				socket.close();
			}
			catch(IOException ioE)
			{
				
			}
		}
	}
	private void broadcastMessage(ChatMessage message)
	{
		synchronized(chatHandlers){
		for(ChatHandler chatHandler : chatHandlers) chatHandler.send(message);
		}
	}
	private void send(ChatMessage message) {
		try{
			synchronized (out) {
				out.writeObject(message);
				out.flush();
			}}
		catch(IOException ex){}
	}

}
