package alien.server.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RegistrationServer implements Runnable{
	private final int PORT;
	private List<String> namesTaken = new ArrayList<String>();
	private MatchManager networkManager;
	private boolean open;
	public RegistrationServer(MatchManager networkManager, int port)
	{
		this.PORT=port;
		this.networkManager=networkManager;
		
	}
	public void start() {
		print("Avvio...");
		open=true;
		new Thread(this).start();
	}
	private void playersRegistration() {
		ExecutorService executor = Executors.newCachedThreadPool();
		ServerSocket serverSocket=null;
		try{
		 serverSocket = new ServerSocket(PORT);
		while (open) {
			try {
			Socket socket = serverSocket.accept();
			print("Ricevuta connessione");
			ClientRegistration client = new ClientRegistration(socket,namesTaken);
			client.addObserver(networkManager);
			executor.submit(client);
			} catch (IOException e) {
			}
			print("Registrazione richiesta");
		}
		}
		catch(IOException ex){}
		finally
		{
			try{serverSocket.close();}catch(Exception ex){}
		}
	}
	public void closeRegistrations()
	{
		open=false;
	}
	
	private void print(String message)
	{
		System.out.println("REGISTRATION SERVER:"+message);
	}
	@Override
	public void run() {
		playersRegistration();
		
	}
}
