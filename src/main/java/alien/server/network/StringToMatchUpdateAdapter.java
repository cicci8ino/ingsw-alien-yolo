package alien.server.network;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import alien.common.BroadcastUpdate;
import alien.common.MatchUpdate;
import alien.common.PlayerLists;
import alien.common.RemoteItemCard;
import alien.common.RemoteStatus;
import alien.common.UnicastUpdate;
import alien.server.card.ItemCard;
import alien.server.game.GameBoard;
import alien.server.game.Player;
import alien.server.sector.Position;
import alien.server.util.StringUpdate;

public class StringToMatchUpdateAdapter {
	
	
	private final GameBoard gameBoard;
	public StringToMatchUpdateAdapter(GameBoard gameBoard)
	{
		this.gameBoard=gameBoard;
	}
	private BroadcastUpdate generateBroadcastUpdate(StringUpdate stringUpdate)
	{
		BroadcastUpdate broadcastUpdate;
		
		PlayerLists playersLists = new PlayerLists(new ArrayList<String>(gameBoard.getPlayersNames()));
		
		List<String> deadPlayers = gameBoard.getDeadPlayersNames();
		for(String name: deadPlayers) playersLists.addToDeadPlayers(name);
		
		 
		
		broadcastUpdate= new BroadcastUpdate(playersLists, new ArrayList<Position>(stringUpdate.getEventPositions()), gameBoard.getPlayerOnTurn().getName());
		
		List<String> messages=stringUpdate.getBroadcastMessages();
		for(String message:messages) broadcastUpdate.addBroadcastMessage(message);
		broadcastUpdate.setMatchOver(stringUpdate.getMatchOver());
		return broadcastUpdate;
	}
	private UnicastUpdate generateUnicastUpdate(String playerName)
	{
		UnicastUpdate unicastUpdate;
		Player player = gameBoard.getPlayerByName(playerName);
		RemoteStatus remoteStatus= player.getStatus().getRemoteStatus();
		
		Position playerPosition = player.getPawn().getPosition();
		unicastUpdate=new UnicastUpdate(remoteStatus, playerPosition);
		
		List<ItemCard> cards= player.getCards();
		int i=0;
		for(ItemCard card: cards)
		{
			RemoteItemCard remoteItemCard = new RemoteItemCard(card.toString(), i);
			boolean usable=false;
			if(player.getPawn().canUseItem())
			{
				usable=card.isUsable(player.getStatus());
			}
			unicastUpdate.addCard(remoteItemCard,usable);
			i++;
		}
		
		List<Position> usablePositions = player.getStatus().getUsablePositions(gameBoard.getMap(),player.getPawn());
	    unicastUpdate.addUsablePositions(usablePositions);
		
	    unicastUpdate.setTurn(player.getTurn());
		return unicastUpdate;
	}
	private UnicastUpdate generateUnicastUpdate(String playerName, List<String> messages)
	{
	    	UnicastUpdate unicastUpdate = generateUnicastUpdate(playerName);
	    	for(String message: messages) unicastUpdate.addUnicastMessage(message);
	    	return unicastUpdate;
	}
	public MatchUpdate generateMatchUpdate(StringUpdate stringUpdate)
	{
		MatchUpdate matchUpdate;
		BroadcastUpdate broadcastUpdate = generateBroadcastUpdate(stringUpdate);
		
		matchUpdate=new MatchUpdate(broadcastUpdate);
		
		Map<String,List<String>> privateMessages= stringUpdate.getPrivateMessages();
		for(String playerName: privateMessages.keySet()) 
		{
			UnicastUpdate unicastUpdate = generateUnicastUpdate(playerName,privateMessages.get(playerName));
			matchUpdate.addUnicastUpdateForPlayer(playerName, unicastUpdate);
		}
		return matchUpdate;
	}
	

}
