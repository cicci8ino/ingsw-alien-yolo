package alien.server.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatServer implements Runnable{
	ServerSocket serverSocket;
	List<ChatHandler> chatHandlers = new ArrayList<ChatHandler>();
	private final int PORT;
	public ChatServer(int port) throws IOException
	{
		this.PORT=port;
		serverSocket = new ServerSocket(PORT);
		
	}
	public void start() {
		new Thread(this).start();
	}
	@Override
	public void run() {
		ExecutorService executor = Executors.newCachedThreadPool();
		while(true)
		{ try {
			Socket socket=	serverSocket.accept();

			ChatHandler chatHandler = new ChatHandler(socket,chatHandlers);
			synchronized (chatHandlers) {
				chatHandlers.add(chatHandler);
			}
			executor.submit(chatHandler);
			
		} catch (IOException e) {
		}
		
		
		
		}
	}

	
	
}
