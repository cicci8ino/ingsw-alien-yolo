package alien.server.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Observable;
import alien.common.InitialInfo;
import alien.common.MatchUpdate;
import alien.server.action.BaseAction;
import alien.server.action.Disconnected;

public class SocketClientHandler extends Observable implements Runnable {
	private ObjectInputStream in;
	private ObjectOutputStream out;
	private Socket socket;
	private String playerName;
	private boolean gameStarted;
	private boolean isConneted;
	private InitialInfo initialInfo;
	
	public SocketClientHandler(Socket socket, ObjectInputStream input, ObjectOutputStream output, String playerName)
	{
		this.playerName=playerName;
this.socket=socket;
		this.in=input;
		this.out=output;
		isConneted = true;
		gameStarted=false;
	}
	/**
	 * Continua a leggere azioni dalla rete e le notifica al controller
	 */
	public void run() {
	try
	{
		System.out.println("ClientHandler "+ playerName +" started");
		while(!gameStarted&&isConneted)
		{
			Thread.sleep(100);
		}
		while (isConneted)
		{
			System.out.println("ClientHandler "+playerName+" in attesa di azioni");
			BaseAction action = (BaseAction)in.readObject();
			System.out.println("ClientHandler "+playerName+" ricevuta azione "+action.toString());
			action.setPlayerName(playerName);
			
			this.setChanged();
			this.notifyObservers(action);
		}
	}
	catch (Exception e) 
	{
	}
		finally
		{
			try
			{
				in.close();
				out.close();
				socket.close();
			}
			catch(IOException ex)
			{
				System.err.println(ex.getMessage());
			}
		}
	}

	public String getPlayerName() {
		return playerName;
	}
	public synchronized void sendMatchUpdate(MatchUpdate matchUpdate) throws IOException {
		out.writeObject(matchUpdate);
		out.flush();
	}
	
	public synchronized void gameIsStarted(InitialInfo initialInfo)
	{

		this.initialInfo=initialInfo;

		try {
			sendInitialInfo();
		} catch (IOException e) {
		}
		gameStarted=true;
	}
	public void disconnect()
	{
		isConneted=false;
		BaseAction disconnectAction = new Disconnected();
		disconnectAction.setPlayerName(playerName);
		this.setChanged();
		this.notifyObservers(disconnectAction);
		try{
			in.close();
			out.close();
			socket.close();
		}
		catch(IOException ex){}
	}
	public boolean isConnected()
	{
		return isConneted;
	}
	private void sendInitialInfo() throws IOException {
		
		out.writeObject(initialInfo);
		out.flush();
		System.out.println("ClientHandler: Initial infos inviate a "+playerName);
	}
}
