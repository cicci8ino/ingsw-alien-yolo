package alien.server.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import alien.common.InitialInfo;
import alien.common.MatchUpdate;
import alien.server.controller.Controller;
import alien.server.file.MapHandler;
import alien.server.game.GameBoard;
import alien.server.game.Player;
import alien.server.sector.MapClass;
import alien.server.util.StringUpdate;
import alien.server.util.StringUpdateObserver;

public class MatchManager implements Observer,Runnable,StringUpdateObserver{
List<SocketClientHandler> clients = new ArrayList<SocketClientHandler>();
private final static int MAX_NUMBEROF_PLAYERS=8;
private final static int MIN_NUMBEROF_PLAYERS=2;
private final static long SECONDS_BEFORE_GAME_START=10;
private Controller controller;
private Boolean open;
private ExecutorService executor;
private GameBoard gameBoard;
private Object lock=new Object();
volatile long timeLastRegistration = System.currentTimeMillis();
private final StringToMatchUpdateAdapter stringUpdateAdapter;
/**
 * Aggiunge ad una lista tutti i client registrati correttamente e poi fa partire il game
 */
public MatchManager() {
	MapHandler mapHandler = new MapHandler();
	print("Inserisci il nome della mappa");
	MapClass map=mapHandler.getMap(requestMapName());
	gameBoard= new GameBoard(map);
	gameBoard.register(this);
	controller= new Controller(gameBoard);
	stringUpdateAdapter=new StringToMatchUpdateAdapter(gameBoard);
	executor = Executors.newCachedThreadPool();
	open=true;

}
private String requestMapName()
{
	InputStreamReader reader = new InputStreamReader (System.in);
	BufferedReader myInput = new BufferedReader (reader);
	try {
		
		return new String (myInput.readLine());
	} catch (IOException e) {
	}
	return "";
}
/**
 * Chiude le registrazioni. inizia il gioco; passa le initial infos a tutti i giocatori
 */
public void startGame()
{
	gameBoard.createPlayers(getPlayersNames());
	gameBoard.assignRoles();
	Map<String, InitialInfo> initialInfos = getInitialInfo();
	for(SocketClientHandler client:clients)
	{
		String playerName = client.getPlayerName();
		print("invio initial info a "+playerName);
		InitialInfo initialInfo = initialInfos.get(playerName);
		client.gameIsStarted(initialInfo);
	}
	gameBoard.chooseRandomlyFirstPlayer();
}
private Map<String,InitialInfo> getInitialInfo()
{
	Map<String,InitialInfo> initialInfos = new HashMap<String, InitialInfo>();
	String stringMap = gameBoard.getMap().toString();
	
	for(Player player : gameBoard.getPlayers())
	{
		String pawnType = player.getPawn().toString();
		String pawnName = player.getPawn().getName();
		InitialInfo initialInfo = new InitialInfo(stringMap, pawnType,pawnName, "Welcome "+player.getName()+"\nYou are "+pawnType,player.getStatus().getRemoteStatus());
		initialInfos.put(player.getName(), initialInfo);
	}
	return initialInfos;
}


	/**
	 * riceve client che hanno registrato il nome
	 */
	@Override
	public void update(Observable o, Object arg) {
		
	   	print("ricevuto update");
		SocketClientHandler client = (SocketClientHandler)arg;
		synchronized (lock) 
		{
			if(clients.size()<MAX_NUMBEROF_PLAYERS&&open)
			{
				client.addObserver(controller);
				clients.add(client);
				System.out.println("NETWORnK MANAGER: Client pronto ad esecuzione; nome="+client.getPlayerName());
				executor.submit(client);
				timeLastRegistration=System.currentTimeMillis();
			}
		}
	}
	public void sendMatchUpdate(MatchUpdate matchUpdate)
	{
		SocketClientHandler brokenClient=null;
		print("sending MatchUpdate");
		for(SocketClientHandler client: clients)
		{
			try{
				client.sendMatchUpdate(matchUpdate.prepareUpdateForPlayer(client.getPlayerName()));
			}
			catch(IOException ex) {
				System.out.println("Connessione persa con "+client.getPlayerName());
				brokenClient=client;
				break;
			}
		}
		if(brokenClient!=null){
			clients.remove(brokenClient);
			brokenClient.disconnect();
		}
		
	}
	public List<String> getPlayersNames() {
		List<String> names = new ArrayList<String>();
		for(SocketClientHandler client: clients) 
			names.add(client.getPlayerName());
		return names;
	}

	@Override
	public void run() {
		print("Running...");
		long millisBeforeStart=SECONDS_BEFORE_GAME_START*1000;
		while(open)
		{
			synchronized (lock) {
			if(clients.size()>=MIN_NUMBEROF_PLAYERS)
			{
				if(System.currentTimeMillis()-timeLastRegistration>millisBeforeStart)
				{
					
						open=false;
				}
			}
			else timeLastRegistration = System.currentTimeMillis();
			}
			if((System.currentTimeMillis()/100)%100==0) print("Rimangono "+(millisBeforeStart-(System.currentTimeMillis()-timeLastRegistration))/1000+" secondi prima dell'inizio della partita");
			else
			if((System.currentTimeMillis()/100)%10==0&&(millisBeforeStart-(System.currentTimeMillis()-timeLastRegistration))/1000<6) print("Rimangono "+(millisBeforeStart-(System.currentTimeMillis()-timeLastRegistration))/1000+" secondi prima dell'inizio della partita");

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
			
		}
		startGame();
		
		while(true)
		{
			
			MatchUpdate matchUpdate = stringUpdateAdapter.generateMatchUpdate(StringUpdate.broadcastMessage(""));
			sendMatchUpdate(matchUpdate);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
			}
		}
	}
	private void print(String message)
	{
		System.out.println("MatchManager:"+message);
	}
	@Override
	public void update(StringUpdate update) {
		if(update.getDisconnectedPlayer()!=null) disconnectClient(update.getDisconnectedPlayer());
		
		MatchUpdate matchUpdate = stringUpdateAdapter.generateMatchUpdate(update);
		sendMatchUpdate(matchUpdate);
	}

	private void disconnectClient(String disconnectedPlayer) {
		SocketClientHandler client=null;
		for(SocketClientHandler c: clients){if(c.getPlayerName().equals(disconnectedPlayer)) client=c;
		if(client!=null)clients.remove(client);
		}
	}
	
}
