package alien.server.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server  {
	
	MatchManager matchManager;
	ChatServer chatServer;
	RegistrationServer registrationServer;

	public Server()
	{
		
	}
	
	private void start() throws IOException
	{
		int chatPort, gamePort;
		matchManager = new MatchManager();
		print("Inserire numero porta game server:");
		gamePort=readPort();
		
		System.out.println("Inserire numero porta chat server:");
		chatPort=readPort();
		
		

		registrationServer= new RegistrationServer(matchManager,gamePort);
		chatServer = new ChatServer(chatPort);
		
		
		
		ExecutorService executor = Executors.newSingleThreadExecutor();
		executor.submit(matchManager);
		
		print("Avvio registration server on port 2005");
		registrationServer.start();


		print("Avvio chat server on port 2006");
		chatServer.start();
		
		
	}

	private int readPort()
	{
		InputStreamReader reader = new InputStreamReader (System.in);
		BufferedReader myInput = new BufferedReader (reader);
		boolean accepted=false;
		int portRead=-1;
		while(!accepted)
		{
			accepted=true;
			try {
				String read = new String (myInput.readLine());

				portRead=Integer.parseInt(read);
				if(portRead<=0||portRead>=65536)
				{
					print("Numero di port non valido: inserisci un numero tra 0 e 65536 esclusi");
					accepted=false;
				}
			} catch (IOException e) {
				print("Errore di lettura: inserire nuovamente");
				accepted=false;
			}
			catch(NumberFormatException ne)
			{
				print("Inserisci un numero");
				accepted=false;
			}
		}
		return portRead;
	}
	
	public void sendNameAccepted(OutputStream outputStream) throws IOException {
		new ObjectOutputStream(outputStream).writeObject(new Boolean(true));
	}
	public void sendNameNotAccepted(OutputStream outputStream) throws IOException {

		new ObjectOutputStream(outputStream).writeObject(new Boolean(false));
	}


	
	private void print(String message)
	{
		System.out.println("SERVER:"+message);
	}
	public static void main(String[] args)
	{
		Server server=new Server();
		try {
			System.out.println("Server started");
			server.start();
		} catch (IOException e) {
		}
		
	}

}
