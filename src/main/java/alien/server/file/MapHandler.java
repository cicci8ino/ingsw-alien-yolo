package alien.server.file;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import alien.server.exception.MalformedMapException;
import alien.server.sector.DangerousSector;
import alien.server.sector.EscapeSector;
import alien.server.sector.MapClass;
import alien.server.sector.MapGenerator;
import alien.server.sector.Position;
import alien.server.sector.Sector;
import alien.server.sector.SecureSector;

public class MapHandler {
	
	private FileHandler folderToHandle;
	
	public static final String TEXT_EXTENSION = ".tmap";
	public static final String DEFAULT_FOLDER_NAME = "maps";
	public final String chosenFolder;
	
	public static final int NUMBEROF_ESCAPE=4, NUMBEROF_ALIEN=1, NUMBEROF_HUMAN=1;
	
	/**
	 * Costruisce il maphandler. Utilizza la cartella di default
	 */
	
	public MapHandler() { 
		folderToHandle = FileHandler.handleFolder(DEFAULT_FOLDER_NAME);
		this.chosenFolder=DEFAULT_FOLDER_NAME;
	}
	
	/**
	 * Costruisce il MapHandler
	 * @param mapFolder Nome cartella in cui sono salvate le mappe
	 */
	
	public MapHandler(String mapFolder) { 
		folderToHandle = FileHandler.handleFolder(mapFolder);
		this.chosenFolder=mapFolder;
	}
	
	/**
	 * Ritorna la mappa con un certo nome. Se non è disponibile ne genera una.
	 * @param fileName Nome mappa
	 * @return 
	 */
	
	public MapClass getMap(String fileName) {
		File mapFile = new File(DEFAULT_FOLDER_NAME+"/"+fileName.toLowerCase()+TEXT_EXTENSION);
		if (mapFile.exists()) {
			System.out.println("Map "+fileName+" found.");
			try {
				return getTextMap(mapFile);
			} catch (IOException e) {
				System.out.println("Something wrong loading map. Generating a new one");
				MapGenerator mg = new MapGenerator();
				return mg.getRandomMap();
			} catch (MalformedMapException e) {
				System.out.println("Map is malformed. Generating a new one");
				MapGenerator mg = new MapGenerator();
				return mg.getRandomMap();
			}
		}
		else {
			MapGenerator mg = new MapGenerator();
			System.out.println("Map "+fileName+" not found. Generating new map");
			return mg.getRandomMap();
		}
	}

	
	private MapClass getTextMap(File file) throws IOException {
		return convertFromTextToMapObj(folderToHandle.getFileAsString(file));
	}
	
	public static MapClass convertFromTextToMapObj(String textMap) {
		int row=1, col=1;
		final MapClass map = new MapClass();
		isTextMapLegit(textMap);
		for (Character c:textMap.toCharArray()) {
			Sector sectorToSave;
			Position positionToSave = new Position(row, col);
			switch (c) {
			case 'D':
				sectorToSave=new DangerousSector(positionToSave);
				map.putSector(sectorToSave);
				break;
			case 'S':
				sectorToSave=new SecureSector(positionToSave);
				map.putSector(sectorToSave);
				break;
			case '\n':
				row++;
				col=0;
				break;
			case 'A':
				map.setAlienPosition(positionToSave);
				break;
			case 'H':
				map.setHumanPosition(positionToSave);
				break;
			default:
				break;
			}
			col++;
			if (Character.isDigit(c)) {
				sectorToSave=new EscapeSector(positionToSave, Character.getNumericValue(c));
				map.putSector(sectorToSave);
			}
				
		}
		return map;
		
	}
	
	public static void isTextMapLegit(String textMap) throws MalformedMapException {
		int numberOfAlien=0, numberOfHuman=0, numberOfEscape=0;
		List<Character> escapeNumber = new ArrayList<Character>(4);
		escapeNumber.add('1');
		escapeNumber.add('2');
		escapeNumber.add('3');
		escapeNumber.add('4');
		for (Character c:textMap.toCharArray()) {
			if (!(isCharacterEndLine(c)||c.equals('\r')||isCharacterAlien(c)||isCharacterHuman(c)||isCharacterSecure(c)||isCharacterDangerous(c)||isCharacterNull(c)||isCharacterEscape(c)))
				throw new MalformedMapException(c+" is not a valid character in map save");
			if (isCharacterAlien(c))
				numberOfAlien++;
			if (isCharacterHuman(c))
				numberOfHuman++;
			if (isCharacterEscape(c)) {
				numberOfEscape++;
				escapeNumber.remove(c);
			}
		}
		if (numberOfAlien!=NUMBEROF_ALIEN || numberOfHuman!=NUMBEROF_HUMAN || numberOfEscape!=NUMBEROF_ESCAPE || !escapeNumber.isEmpty())
			throw new MalformedMapException("Wrong number of alien/human/escape sector");
	}
	
	private static boolean isCharacterEndLine(Character c){ 
		return c.equals('\n');
	}
	
	private static boolean isCharacterAlien(Character c){ 
		return c.equals('A');
	}
	
	private static boolean isCharacterHuman(Character c){ 
		return c.equals('H');
	}
	
	private static boolean isCharacterSecure(Character c){ 
		return c.equals('S');
	}
	
	private static boolean isCharacterDangerous(Character c){ 
		return c.equals('D');
	}
	
	private static boolean isCharacterNull(Character c){ 
		return c.equals('N');
	}
	
	private static boolean isCharacterEscape(Character c) {
		return Character.isDigit(c);
	}
	
}
