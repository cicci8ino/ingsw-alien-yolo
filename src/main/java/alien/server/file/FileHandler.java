package alien.server.file;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileHandler { 
	/**
	 * File della cartella da gestire
	 */
	private final File folderToHandle; 
	
	private FileHandler(String folderName) { 
		this.folderToHandle=new File(folderName);
	}
	
	/**
	 * Inizializza un File Handler su una cartella. Unico costruttore pubblico.
	 * @param folderName Cartella da "gestire"
	 * @return File Handler
	 */
	
	public static FileHandler handleFolder(String folderName) { 
		FileHandler fileHandler = new FileHandler(folderName);
		return fileHandler;
	}
	
	/**
	 * "Apri" un file trattandolo come una stringa
	 * @param path File da aprire
	 * @return Stringa associata al file
	 * @throws IOException
	 */
	
	public String getFileAsString(File path) throws IOException  { 
		if (!path.exists())
			throw new FileNotFoundException(path.getAbsolutePath()+" not found.");
		FileReader fileReader = new FileReader(path); 
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		StringBuilder stringBuilder = new StringBuilder();
		String line;
		while((line=bufferedReader.readLine())!=null)
			stringBuilder.append(line).append("\n");
		bufferedReader.close();
		return stringBuilder.toString();
	}
	

	public String getFolderPath() {
		return folderToHandle.getAbsolutePath();
	}
	
	

}
