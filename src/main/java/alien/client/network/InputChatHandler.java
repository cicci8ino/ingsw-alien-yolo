package alien.client.network;

import alien.client.controller.ClientController;
import alien.client.view.ErrorUI;
import alien.common.ChatMessage;

public class InputChatHandler implements Runnable{

	private final ClientController client;
	private final Connection connection;
	private final ErrorUI errorUI;
	
	public InputChatHandler(ClientController client, Connection connection, ErrorUI errorUI) {
		this.client=client;
		this.connection=connection;
		this.errorUI=errorUI;
	}
	
	public ChatMessage getChatMessage() throws Exception {
		return connection.receiveChat();
	}
	
	@Override
	public void run() {
			try {
				while(true)
					client.getUI().showChatMessage(getChatMessage().toString());
			} catch (Exception e) {
				errorUI.showFatalErrorMessage("CHAT: "+e.getMessage());
			}	
	}	
}
