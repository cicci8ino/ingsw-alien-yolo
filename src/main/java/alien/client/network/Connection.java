package alien.client.network;

import alien.common.ChatMessage;

public abstract class Connection {

	public abstract Object receiveObject() throws Exception;
	public abstract void sendObject(Object object) throws Exception;
	public abstract void connect() throws Exception;
	public abstract ChatMessage receiveChat() throws Exception;
	public abstract void sendChat(ChatMessage chatMessage) throws Exception;
}
