package alien.client.network;

import alien.client.view.ErrorUI;
import alien.common.ChatMessage;
import alien.common.Name;
import alien.server.action.BaseAction;

public class OutputHandler {
	
	final private Connection connection;
	final private ErrorUI errorUI;
	private Object sendLock = new Object();
	private Object sendChatLock = new Object();
	
	/**
	 * Gestisce dati in uscita
	 * Chat, invio nome e invio azioni
	 * @param connection Protocollo di connessione adottato
	 * @param errorUI UI di errore
	 */
	
	public OutputHandler(Connection connection, ErrorUI errorUI) {
		this.connection = connection;
		this.errorUI = errorUI;
	}
	
	public void sendAction(BaseAction action) {
		try {
			connection.sendObject(action);
		} catch (Exception e) {
			errorUI.showErrorMessage("Error sending action: "+e.getMessage());
		}
	}
	
	public void sendUsername(String name) {
		try {
			synchronized(sendLock) {
				connection.sendObject(new Name(name));
			}
		} catch (Exception e) {
			errorUI.showErrorMessage("Error sending username: "+e.getMessage());
		}
	}
	public void sendChatMessage(String message, String sender)  {
		try {
			if (!message.trim().isEmpty()) {
				synchronized(sendChatLock) {
					connection.sendChat(new ChatMessage(message, sender));
				}
			}
		} catch (Exception e) {
			errorUI.showErrorMessage("Error sending chat message: "+e.getMessage());
		}
	}


}
