package alien.client.network;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import alien.common.ChatMessage;

public class SocketServerConnection extends Connection {

	//cose che servono al client. leggere, scrivere, connettersi
	private Socket server;
	private Socket chatServer;
	private ObjectOutputStream serverOutputOBJ;
	private ObjectInputStream serverInputOBJ;
	private ObjectOutputStream chatOutputOBJ;
	private ObjectInputStream chatInputOBJ;
	private final String address;
	private final int port;
	private final int chatPort;
	
	/**
	 * Costruisce una connessione con protocollo socket
	 * @param serverParameters Parametri da utilizzare
	 */
	
	public SocketServerConnection(ServerParameters serverParameters) {
		this.address=serverParameters.getServerAddress();
		this.port=Integer.parseInt(serverParameters.getServerPort());
		this.chatPort=Integer.parseInt(serverParameters.getChatPort());
	
	}
	
	@Override
	public synchronized Object receiveObject() throws Exception{
		return serverInputOBJ.readObject();
	}

	@Override
	public void connect() throws Exception {
		server = new Socket(address, port);
		chatServer = new Socket(address, chatPort);
		serverOutputOBJ = new ObjectOutputStream(server.getOutputStream());
		serverOutputOBJ.flush();
		chatOutputOBJ = new ObjectOutputStream(chatServer.getOutputStream());
		chatOutputOBJ.flush();
		serverInputOBJ = new ObjectInputStream(server.getInputStream());	
		chatInputOBJ = new ObjectInputStream(chatServer.getInputStream());
	}

	@Override
	public void sendObject(Object object) throws Exception {
		serverOutputOBJ.flush();
		serverOutputOBJ.writeObject(object);
	}

	@Override
	public ChatMessage receiveChat() throws Exception {
		return (ChatMessage) chatInputOBJ.readObject();
	}

	@Override
	public void sendChat(ChatMessage chatMessage) throws Exception {
		chatOutputOBJ.writeObject(chatMessage);
	}
	

}
