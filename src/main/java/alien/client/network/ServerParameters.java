package alien.client.network;

public class ServerParameters {

	private final String serverAddress;
	private String serverPort;
	private String chatPort;
	private final boolean rmiIsChosen; //rmi false di default
	
	/**
	 * Oggetto che contiene i parametri del server
	 * @param serverAddress Indirizzo ip del server
	 * @param serverPort Porta del gioco
	 * @param chatPort Porta della chat
	 */
	
	public ServerParameters(String serverAddress, String serverPort, String chatPort) {
		this.serverAddress=serverAddress;
		this.serverPort=serverPort;
		this.chatPort=chatPort;
		this.rmiIsChosen=false;
	}
	public String getServerAddress() {
		return serverAddress;
	}
	public String getServerPort() {
		return serverPort;
	}
	public String getChatPort() {
		return chatPort;
	}
	public boolean isRmiChosen() {
		return rmiIsChosen;
	}
	
}
