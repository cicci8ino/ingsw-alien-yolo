package alien.client.network;

import alien.client.controller.ClientController;
import alien.client.view.ErrorUI;
import alien.common.InitialInfo;
import alien.common.MatchUpdate;


public class InputHandler implements Runnable {

	private final ClientController client;
	private final Connection connection;
	private final ErrorUI errorUI;
	
	/**
	 * Gestisce i dati in ingresso
	 * MatchUpdate, InitialInfo ed esito accettazione nome
	 * @param client Controller
	 * @param connection Protocollo di connessione adottato
	 * @param errorUI UI di errore su cui stampare errori
	 */
	
	public InputHandler(ClientController client, Connection connection, ErrorUI errorUI) {
		this.client=client;
		this.connection=connection;
		this.errorUI = errorUI;
	}
	
	/**
	 * Aspetta esito dal server riguardo al nome scelto
	 * @return Esito
	 * @throws Exception
	 */
	
	public Boolean isNameAccepted() throws Exception {
		client.getUI().showLogMessage("Waiting for name to be accepted");
		Boolean isNameAccepted = (Boolean) connection.receiveObject();
		if (isNameAccepted==null)
			throw new Exception("Name accepted boolean is null");
		if (isNameAccepted) {
			client.getUI().showLoading(client.getName());
			return true;
		}
		errorUI.showErrorMessage("Name is already chosen");
		return false;
	}
	
	/**
	 * Riceve InitialInfo
	 * @return InitiaInfo ricevute
	 * @throws Exception
	 */
	
	public InitialInfo getInitialInfo() throws Exception {
		return (InitialInfo) connection.receiveObject();
	}
	
	/**
	 * Riceve MatchUpdate
	 * @return MatchUpdate ricevuto
	 * @throws Exception
	 */
	
	public MatchUpdate getMatchUpdate() throws Exception {
		client.getUI().showLogMessage("Receiving matchupdate");
		MatchUpdate receivedMatchUpdate = (MatchUpdate) connection.receiveObject();
		client.getUI().showLogMessage("Matchupdate received");
		return receivedMatchUpdate;
	}
	/**
	 * Notifica le info iniziali alla ui (che provvederà ad applicarle)
	 * @param matchUpdateToApply
	 */
	public void notifyInitialInfoToUI(InitialInfo initialInfoToApply) {
		client.getUI().showLogMessage("Applying initialinfo");
		client.updateUI(initialInfoToApply);
	}
	/**
	 * Notifica l'update alla ui (che provvederà ad applicarlo)
	 * @param matchUpdateToApply
	 */
	public void notifyMatchUpdateToUI(MatchUpdate matchUpdateToApply) {
		client.getUI().showLogMessage("Applying matchupdate");
		client.updateUI(matchUpdateToApply);
	}

	@Override
	public void run() {
		try {
			client.getUI().showLogMessage("Waiting for Map and Character");
			InitialInfo initialInfo = getInitialInfo();
			if (initialInfo==null)
				throw new Exception("InitialInfo is null");
			client.getUI().showLogMessage("Initial info received");
			notifyInitialInfoToUI(initialInfo); //mettiti in attesa di info e applicale
			new Thread(client).start();
			while (true) {
				MatchUpdate matchUpdate = getMatchUpdate();
				if (matchUpdate==null) 
					throw new Exception("MatchUpdate is null");
				notifyMatchUpdateToUI(matchUpdate); //mettiti in attesa di matchupdate e applical
			}
		} catch (Exception e) {
			errorUI.showFatalErrorMessage("GAME: "+e.getMessage());
		}
	}
	/**
	 * Richiedi il nome, fin quando questo non è accettato dal server
	 */
	public void askForName() {
		try {
			do {
				client.getUI().askForName();
				
			} while (!isNameAccepted());
			new Thread(this).start();
		} catch (Exception e) {
			errorUI.showErrorMessage("Something wrong receiving name confirm: "+e.getMessage());
		}
	}
}
