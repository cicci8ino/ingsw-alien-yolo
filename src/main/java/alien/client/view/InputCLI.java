package alien.client.view;

import alien.client.model.ClientModel;
import alien.server.action.Attack;
import alien.server.action.EndTurn;
import alien.server.action.ItemCardDiscarded;
import alien.server.action.ItemCardSelected;
import alien.server.action.PositionSelected;
import alien.server.sector.Position;

public class InputCLI {
	
	private final ClientModel clientModel;
	
	private final CLI cli;
	
	/**
	 * Gestisce input da linea di comando
	 * @param actionPerformer Model
	 * @param cli UI CLI
	 */
	
	public InputCLI(ClientModel actionPerformer, CLI cli) {
		this.clientModel = actionPerformer;
		this.cli=cli;
	}

	/**
	 * Analizza input ed esegue azione associata
	 * @param input Input
	 * @throws Exception
	 */
	
	public void execute(String input) throws Exception {
		
		ErrorUI errorUI = clientModel.getErrorUI();
		
		if (CLI.HELP_PREFIX.equals(input)) {
			cli.showHelp();
			return;
		}
		if (CLI.MAP_PREFIX.equals(input)) {
			cli.showMap(clientModel.getMap());
			return;
		}
		if (CLI.PLAYERS_PREFIX.equals(input)) {
			cli.showPlayers(clientModel.getPlayerLists());
			return;
		}
		
		
		//ACTION
		
		if (CLI.CARD_PREFIX.equals(input)) {
			cli.showCardsInHand(clientModel.getCardsInHand());
			cli.showInfoMessage("Usage: '/card 1 d' to discard card number 1, '/card 2 u' to use card number 2");
			return;
		}
		if (input.matches("/card\\s[0-3]\\s[ud]")) {
			ItemCardSelected actionCard = getCardActionFromInput(input);
			clientModel.sendCommand(actionCard);
			return;
		}
		if (CLI.POS_PREFIX.equals(input)) {
			cli.showUsablePositions(clientModel.getUsablePositions());
			cli.showInfoMessage("Usage: '/pos A01' to select position A01");
			return;
		}
		if (input.matches("/pos\\s[A-W][0][1-9]") || (input.matches("/pos\\s[A-W][1][0-4]"))) {
			Position selectedPosition = getPositionFromInput(input);
			System.out.println(selectedPosition.toString());
			PositionSelected positionAction = new PositionSelected(selectedPosition);
			clientModel.sendCommand(positionAction);
			return;
		}
		if (CLI.ATTACK_PREFIX.equals(input)) {
			clientModel.sendCommand(new Attack());
			return;
		}
		if (CLI.ENDTURN_PREFIX.equals(input)) {
			clientModel.sendCommand(new EndTurn());
			return;
		}
		if (input.startsWith("/")) {
			errorUI.showErrorMessage("Command not recognized");
		}
	}
	
	private static Position getPositionFromInput(String input) {
		String[] inputArray = input.split("\\s");
		String withoutPos = inputArray[1];
		String[] command = withoutPos.split("[A-W]");
		String number = command[1];
		String[] command2 = withoutPos.split("[0-1]");
		String letter = command2[0];
		int numberInt = Integer.parseInt(number);
		int letterInt = letter.charAt(0)-64;
		return new Position(numberInt, letterInt);
	}
	
	private static ItemCardSelected getCardActionFromInput(String input) {
		String[] inputArray = input.split("\\s");
		String index = inputArray[1];
		String command = inputArray[2];
		int intIndex = Integer.parseInt(index);
		if (command.charAt(0)=='d')
			return new ItemCardDiscarded(intIndex);
		return new ItemCardSelected(intIndex);
	}
	
}
