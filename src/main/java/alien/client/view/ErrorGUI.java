package alien.client.view;

import javax.swing.JOptionPane;

public class ErrorGUI extends ErrorUI{

	@Override
	public void showErrorMessage(String message) {
		JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.PLAIN_MESSAGE, null);
	}
	
	@Override
	public void showFatalErrorMessage(String message) {
		JOptionPane.showMessageDialog(null, message+"\nPlease restart the game", "Fatal Error", JOptionPane.PLAIN_MESSAGE, null);
	}

}
