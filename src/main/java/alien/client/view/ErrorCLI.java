package alien.client.view;

public class ErrorCLI extends ErrorUI{

	public final static String ERROR_MESSAGE_HEADER ="[ERROR] ";
	
	@Override
	public void showErrorMessage(String message) {
		System.err.println(ERROR_MESSAGE_HEADER+message);
	}

	@Override
	public void showFatalErrorMessage(String message) {
		System.err.println(CLI.FATAL_ERROR_MESSAGE_HEADER+message+"\nPlease restart the game");
	}
	
	

}
