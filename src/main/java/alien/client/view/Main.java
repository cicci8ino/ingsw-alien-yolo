package alien.client.view;

import alien.client.controller.ClientController;
import alien.client.model.ClientModel;

public class Main {

	public static final boolean DEBUG = false;
	
	public static void main(String[] args) {
		//
		ClientView myUI = new GUI();
		ErrorUI errorUI = new ErrorGUI();
		ClientModel actionPerformer = new ClientModel(errorUI);
		ClientController myClient = new ClientController(myUI, actionPerformer);
		myUI.setActionPerformer(actionPerformer);
		myClient.askForConnection();
	}
	
}
