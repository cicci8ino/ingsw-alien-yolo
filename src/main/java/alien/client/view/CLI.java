package alien.client.view;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import alien.client.network.ServerParameters;
import alien.common.PlayerLists;
import alien.common.RemoteItemCard;
import alien.server.sector.Position;

public class CLI extends ClientView{

	private Scanner inputReader;
	
	//MESSAGES HEADER
	
	public static final String NAME_MESSAGE_HEADER = "[NAME] ";
	public static final String CONFIG_MESSAGE_HEADER ="[CONFIG] ";
	public static final String UNICAST_MESSAGE_HEADER ="[UNICAST] ";
	public static final String BROADCAST_MESSAGE_HEADER ="[BROADCAST] ";
	public static final String FATAL_ERROR_MESSAGE_HEADER ="[FATAL ERROR] ";
	public static final String INFO_MESSAGE_HEADER ="[INFO] ";
	public static final String LOG_MESSAGE_HEADER = "[LOG] ";
	public static final String MAP_MESSAGE_HEADER = "[MAP] ";
	public static final String CHARACTER_MESSAGE_HEADER = "[CHARACTER] ";
	public static final String HELP_MESSAGE_HEADER = "[HELP] ";
	public static final String ALIVE_PLAYERS_HEADER = "[ALIVE PLAYERS] ";
	public static final String DEAD_PLAYERS_HEADER = "[DEAD PLAYERS] ";
	public static final String PLAYER_IN_TURN_HEADER = "[PLAYER IN TURN] ";
	public static final String CURRENT_POSITION_HEADER = "[CURRENT POSITION] ";
	public static final String EVENT_POSITIONS_HEADER = "[EVENT POSITION] ";
	public static final String USABLE_POSITIONS_HEADER = "[USABLE POSITIONS] ";
	public static final String CARDS_HEADER = "[CARDS IN HAND] ";
	public static final String TURN_NUMBER_HEADER = "[TURN NUMBER] ";
	
	//COMMANDS
	
	public static final String POS_PREFIX = "/pos";
	public static final String ATTACK_PREFIX = "/attack";
	public static final String ENDTURN_PREFIX = "/end";
	public static final String CARD_PREFIX = "/card";
	public static final String PLAYERS_PREFIX = "/players";
	public static final String HELP_PREFIX = "/help";
	public static final String MAP_PREFIX = "/map";
	public static final String POSITIONS_PREFIX = "/positions";
	
	public CLI() {
		inputReader = new Scanner(System.in);
	}


	@Override
	public void askForName() {
		writeToUI(NAME_MESSAGE_HEADER+"Insert name: ");
		getClientModel().setAndSendName(inputReader.nextLine());		
	}

	@Override
	public void askForServerParameters() {
		requestConfig("Server address?");
		String serverAddress=inputReader.nextLine();
		requestConfig("Game port?");
		String serverPort=inputReader.nextLine();
		requestConfig("Chat port?");
		String chatPort=inputReader.nextLine();
		getClientModel().requestToConnect(new ServerParameters(serverAddress, serverPort, chatPort));
	}
	

	@Override
	public void showInfoMessage(String message) {
		writeToUI(INFO_MESSAGE_HEADER+message);
	}

	@Override
	public void showBroadcast(String message) {
		writeToUI(BROADCAST_MESSAGE_HEADER+message);
	}

	@Override
	public void showUnicast(String message) {
		writeToUI(UNICAST_MESSAGE_HEADER+message);
	}
	
	private void writeToUI(String message) {
		System.out.println(message);
	}

	public void showFatalErrorMessage(String message) {
		System.err.println(FATAL_ERROR_MESSAGE_HEADER+message+"\nPlease restart the game");	
	}
	
	@Override
	public void showChatMessage(String message) {
		writeToUI(message);
	}
	public void requestConfig(String message) {
		writeToUI(CONFIG_MESSAGE_HEADER+message);
	}

	@Override
	public void showMap(String map) {
		writeToUI(MAP_MESSAGE_HEADER+"\n\n"+getPrintableMap(map)+"\n");
	}
	public String getPrintableMap(String map)
	{
		map=map.replaceAll("N", " ");
		String printableMap="  ";
		String lines[] = map.split("\\r?\\n");
		
		if(lines.length==0) 
			throw new InvalidParameterException("This map is not well formed and printable");

		int R=lines.length;
		int C=lines[0].length();
		int r,c;
		for(c=0;c<C;c++)
		{
			printableMap=printableMap+(" "+((char)(65+c)));
		}

		printableMap=printableMap+("\n  ");
		for(c=0;c<C/2+1;c++)
		{
			printableMap=printableMap+(" _  ");
		}
		printableMap=printableMap+("\n");
		for(r=0;r<R;r++)
		{
			String line[]= lines[r].split("");
			
			printableMap=printableMap+"  ";
			for(c=0;c*2<C-1;c++)
			{
				printableMap=printableMap+("/"+line[c*2]+"\\_");
			}
			printableMap=printableMap+("/"+line[c*2]+"\\");

			printableMap=printableMap+("\n"+(r+1));
			if(r<9)printableMap=printableMap+" ";
			for(c=0;c*2+1<C;c++)
			{
				printableMap=printableMap+("\\_/"+line[c*2+1]);
			}
			printableMap=printableMap+("\\_/\n");
		}
		printableMap=printableMap+("   ");
		for(c=0;c<C/2;c++)
		{
			printableMap=printableMap+(" \\_/");
		}
	
		return printableMap;
	}
	@Override
	public void showCharacter(String characterName) {
		writeToUI(CHARACTER_MESSAGE_HEADER+characterName);	
	}

	@Override
	public void activateInput() {
		InputCLI inputCLI = new InputCLI(getClientModel(), this);
		try {
			while(true) {
				String input = inputReader.nextLine();
				if (!isInputCommand(input))
					getClientModel().sendChat(input);
				else {
					inputCLI.execute(input);
				}
			}
		}
		catch (Exception e) {
			this.showFatalErrorMessage("INPUT HANDLING "+e.getMessage());
		}
	}
	
	public boolean isInputCommand(String input) throws Exception {
		if (input.startsWith("/"))
			return true;
		return false;
	}
	
	public void showHelp() {
		writeToUI(HELP_MESSAGE_HEADER);
		writeToUI(POS_PREFIX+":		Select position (moving or apply card/effect)");
		writeToUI(ATTACK_PREFIX+":	Attack");
		writeToUI(ENDTURN_PREFIX+":		End turn");
		writeToUI(CARD_PREFIX+":		Select card");
		writeToUI(PLAYERS_PREFIX+":	Show players list");
		writeToUI(MAP_PREFIX+":		Show map");

	}

	@Override
	public void showEventPositions(List<Position> list) {
		showListOfPositions(list, EVENT_POSITIONS_HEADER);
	}

	@Override
	public void showAlivePlayers(List<String> alivePlayers) {
		showListOfThings(ALIVE_PLAYERS_HEADER, alivePlayers);
	}

	@Override
	public void showPlayerInTurn(String playerInTurn) {
		writeToUI(PLAYER_IN_TURN_HEADER+ playerInTurn);
	}
	
	public void showListOfPositions(List<Position> positions, String positionHeader) {
		if (positions.isEmpty()) {
			writeToUI("No positions selectable");
			return;
		}
		List<String> stringList = new ArrayList<String>();
		for (Position aPosition:positions) {
			stringList.add(aPosition.toString());
		}
		showListOfThings(positionHeader, stringList);
	}
	
	public void showListOfThings(String header, List<String> things) {
		String stringToDisplay=header;
		if (!things.isEmpty()) {
			for (String aThing:things)
				stringToDisplay=stringToDisplay+"\n"+aThing;
			writeToUI(stringToDisplay);
		}
	}

	@Override
	public void showDeadPlayers(List<String> deadPlayers) {
		showListOfThings(DEAD_PLAYERS_HEADER, deadPlayers);
		
	}


	@Override
	public void showCurrentPosition(Position currentPosition) {
		writeToUI(CURRENT_POSITION_HEADER+currentPosition.toString());
		
	}

	@Override
	public void showUsablePositions(List<Position> usablePositions) {
		showListOfPositions(usablePositions, USABLE_POSITIONS_HEADER);
	}
	
	@Override
	public void showCardsInHand(Map<RemoteItemCard, Boolean> cardsInHand) {
		writeToUI(CARDS_HEADER);
		if (cardsInHand.isEmpty())
			showInfoMessage("No cards in hand");
		for (RemoteItemCard aCard:cardsInHand.keySet())
			writeToUI(aCard.getIndexNumber()+"	"+aCard.getName());
	}

	@Override
	public void initUI() {
		//NOT USED ON CLI. USED TO START DIALOG ON GUI
	}

	@Override
	public void showInitialInfo(String map, String characterName) {
		showMap(map);
		showCharacter(characterName);
		
	}

	@Override
	public void showPlayers(PlayerLists playerLists) {
		showAlivePlayers(playerLists.getUnknownTypePlayers());
		showDeadPlayers(playerLists.getDeadPlayers());
	}

	@Override
	public void showYouAreDead(String message) {
		showInfoMessage(message);
		
	}

	@Override
	public void showYouWon(String message) {
		showInfoMessage(message);
		
	}

	@Override
	public void setOnTurn() {
		//NOT USED ON CLI. USED TO MODIFY GUI PARAMETERS
	}

	@Override
	public void setTurnOver() {
		//NOT USED ON CLI. USED TO MODIFY GUI PARAMETERS
	}

	@Override
	public void showLoading(String playerName) {
		showInfoMessage("Welcome " +playerName);
		showInfoMessage("Waiting for match to start");
	}

	@Override
	public void showTurnNumber(int number) {
		writeToUI(TURN_NUMBER_HEADER+number+"/"+"39");
	}

	@Override
	public void showMatchOver(String message) {
		showInfoMessage(message);
	}


	@Override
	public void setCanAttack(boolean value) {
		if (value)
			showInfoMessage("You can attack");
	}


	@Override
	public void setCanEndTurn(boolean value) {
		if (value)
			showInfoMessage("You can end turn");	
	}

}
