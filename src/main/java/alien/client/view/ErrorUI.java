package alien.client.view;

public abstract class ErrorUI {
	
	public abstract void showErrorMessage(String message);
	public abstract void showFatalErrorMessage(String message);
}
