package alien.client.view;

import java.util.List;
import java.util.Map;
import alien.client.model.ClientModel;
import alien.common.PlayerLists;
import alien.common.RemoteItemCard;
import alien.server.sector.Position;

public abstract class ClientView {
	
	private ClientModel clientModel;
	
	/**
	 * Setta il model
	 * @param actionPerformer Model
	 */
	
	public void setActionPerformer(ClientModel actionPerformer) {
		this.clientModel=actionPerformer;
		initUI();
	}
	
	public ClientModel getClientModel() {
		return clientModel;
		
	}
	
	public abstract void activateInput(); //SOLO PER CLI
	
	//SHOWER
	
	public abstract void askForName();
	public abstract void askForServerParameters();
	public abstract void initUI();
	
	/*
	 * SHOW MESSAGES
	 */
	
	public abstract void showBroadcast(String message);
	public abstract void showUnicast(String message);
	public abstract void showInfoMessage(String message);
	public abstract void showChatMessage(String message);
	public void showLogMessage(String message) {
		if (Main.DEBUG)
			System.out.println("[LOG] "+message);
	}
	
	public abstract void showYouAreDead(String message);
	public abstract void showYouWon(String message);
	public abstract void showMatchOver(String message);
	
	public abstract void setOnTurn();
	public abstract void setTurnOver();
	
	/*
	 * UPDATE UI
	 */
	
	public abstract void showInitialInfo(String map, String characterName);
	public abstract void showMap(String map);
	public abstract void showCharacter(String characterName);
	public abstract void showCardsInHand(Map<RemoteItemCard, Boolean> cardsInHand);
	public abstract void showUsablePositions(List<Position> usablePositions);
	public abstract void showEventPositions(List<Position> eventPositions); //NO IN CLI
	public abstract void showAlivePlayers(List<String> alivePlayers);
	public abstract void showDeadPlayers(List<String> deadPlayers);
	public abstract void showPlayerInTurn(String playerInTurn);
	public abstract void showCurrentPosition(Position currentPosition);
	public abstract void showLoading(String playerName);
	public abstract void showTurnNumber(int number);
	
	//MatchUpdate SHOWER
	
	public abstract void showPlayers(PlayerLists playerLists);
	
	public void showUnicastMessages(List<String> unicastMessages) {
		if (!unicastMessages.isEmpty())
			for (String aMessage:unicastMessages) {
				if (!"".equals(aMessage))
					showUnicast(aMessage);
			}
	}
	public void showBroadcastMessages(List<String> broadcastMessages) {
		if (!broadcastMessages.isEmpty())
			for (String aMessage:broadcastMessages) {
				if (!"".equals(aMessage))
					showBroadcast(aMessage);
			}
	}

	public abstract void setCanAttack(boolean value);
	public abstract void setCanEndTurn(boolean value);
}
