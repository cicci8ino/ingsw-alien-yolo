package alien.client.view;

import java.awt.Color;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import alien.client.gui.dialog.LoadingDialog;
import alien.client.gui.dialog.ConnectionDialog;
import alien.client.gui.dialog.MatchIsOverDialog;
import alien.client.gui.dialog.NameDialog;
import alien.client.gui.panels.CompleteGUI;
import alien.client.gui.panels.Configuration;
import alien.common.PlayerLists;
import alien.common.RemoteItemCard;
import alien.server.sector.Position;

public class GUI extends ClientView {

	private ConnectionDialog parametersDialog;
	private NameDialog nameDialog;
	private CompleteGUI completeGUIPanel;
	private LoadingDialog loadingDialog;
	private JFrame gameWindow;
	JComponent focusComponent;

	@Override
	public void askForName() {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	nameDialog.setVisible(true);
		    }
		  });
	}

	@Override
	public void askForServerParameters() {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	parametersDialog.setVisible(true);
		    }
		  });
	}

	@Override
	public void showBroadcast(final String message) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.showBroadcastMessage(message);
		    }
		  });
		
		
	}

	@Override
	public void showUnicast(final String message) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.showUnicastMessage(message);
		    }
		  });
		
	}

	@Override
	public void showInfoMessage(String message) {
		System.out.println("[INFO]: "+message);
	}

	@Override
	public void showChatMessage(final String message) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.showChatMessage(message);
		    }
		  });
		
		
		
	}

	@Override
	public void showInitialInfo(final String map, final String characterName) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	loadingDialog.setVisible(false);
				gameWindow = new JFrame();
				completeGUIPanel = new CompleteGUI(map.toString(), characterName, getClientModel());
				gameWindow.add(completeGUIPanel);
				gameWindow.setBackground(Color.WHITE);
				gameWindow.setSize(Configuration.RESOLUTION_X, Configuration.RESOLUTION_Y);
				gameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				gameWindow.setTitle("Escape From The Aliens In The Outer Space");
				gameWindow.setVisible(true);
		    }
		  });
		
		
	}

	@Override
	public void showMap(String map) {
	}

	@Override
	public void showCharacter(String characterName) {	
	}

	@Override
	public void showCardsInHand(final Map<RemoteItemCard, Boolean> cardsInHand) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.updateCardsInHand(cardsInHand);
		    }
		  });
		
	}

	@Override
	public synchronized void showUsablePositions(final List<Position> usablePositions) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.updateUsablePositions(usablePositions);
		    }
		  });
	}

	@Override
	public void showEventPositions(final List<Position> eventPositions) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.updateEventPositions(eventPositions);
		    }
		  });
	}

	@Override
	public void showAlivePlayers(List<String> alivePlayers) {
		//NOT USED IN GUI
	}

	@Override
	public void showDeadPlayers(List<String> deadPlayers) {
		//NOT USED IN GUI
	}

	@Override
	public void showPlayerInTurn(final String playerInTurn) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.updatePlayerOnTurn(playerInTurn);
		    }
		  });
	}

	@Override
	public void showCurrentPosition(final Position currentPosition) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.updatePlayerPosition(currentPosition);
		    }
		  });
		
	}


	@Override
	public void initUI() { //inizializzata dopo che è arrivato l'actionperformer
		nameDialog = new NameDialog(getClientModel());
		parametersDialog = new ConnectionDialog(getClientModel());
		
	}


	@Override
	public void showPlayers(final PlayerLists playerLists) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.updatePlayerLists(playerLists);
		    }
		  });
		
	}

	@Override
	public void activateInput() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showYouAreDead(String message) {
		
		showPopup(message);
		
	}

	@Override
	public void showYouWon(String message) {
		showPopup(message);
	}

	@Override
	public void setOnTurn() {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.setOnTurn();
		    }
		  });
		
	}

	@Override
	public void setTurnOver() {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.setTurnOver();
		    }
		  });
	}	

	@Override
	public void showLoading(final String playerName) {
		loadingDialog=new LoadingDialog(playerName);
		loadingDialog.showDialog();
		
	}

	@Override
	public void showTurnNumber(final int number) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.setTurn(number);
		    }
		  });
	}

	@Override
	public void showMatchOver(String message) {
		showPopup(message);
		
	}

	public void showPopup(final String message) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	MatchIsOverDialog dialog = new MatchIsOverDialog(message, gameWindow);
				dialog.showPopup();
		    }
		  });
	}

	@Override
	public void setCanAttack(final boolean value) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.setCanAttack(value);
		    }
		  });
	}

	@Override
	public void setCanEndTurn(final boolean value) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	completeGUIPanel.setCanEndTurn(value);
		    }
		  });
	}
	

}
