package alien.client.controller;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import alien.client.model.ClientModel;
import alien.client.model.ParametersForConnection;
import alien.client.network.Connection;
import alien.client.network.InputChatHandler;
import alien.client.network.InputHandler;
import alien.client.network.OutputHandler;
import alien.client.network.ServerParameters;
import alien.client.network.SocketServerConnection;
import alien.client.view.ClientView;
import alien.client.view.ErrorUI;
import alien.common.BroadcastUpdate;
import alien.common.InitialInfo;
import alien.common.MatchUpdate;
import alien.common.RemoteStatus;
import alien.common.UnicastUpdate;
import alien.server.sector.Position;

public class ClientController implements Runnable, Observer {

	private final ClientView ui;
	
	private String playerName;
	
	private ClientModel clientModel;
	
	/**
	 * Costruisce il controller lato client
	 * @param ui View
	 * @param actionPerformer Model
	 */
		
	public ClientController(ClientView ui, ClientModel actionPerformer) {
		this.ui = ui;
		this.clientModel=actionPerformer;
		actionPerformer.addObserver(this);
	}
	
	/**
	 * Avvia connessione
	 * @param serverParameters Parametri
	 * @param errorUI UI per stampare errori
	 */
	
	public void connect(ServerParameters serverParameters, ErrorUI errorUI) {
		try {
			Connection connection = new SocketServerConnection(serverParameters);
			connection.connect();
			ui.showLogMessage("Connected");
			OutputHandler outputHandler = new OutputHandler(connection, errorUI);
			this.setOutputHandler(outputHandler);
			InputHandler inputHandler = new InputHandler(this, connection, errorUI);
			inputHandler.askForName();
			new Thread(new InputChatHandler(this, connection, errorUI)).start();
		} catch (Exception e) {
			errorUI.showErrorMessage(e.getMessage());
			synchronized(this) {
				notify();
			}
		}		
	}
	
	/**
	 * Avvia la richiesta di connessione
	 */
	
	public void askForConnection() {
		
		while (true) {
			ui.askForServerParameters();
			synchronized (this) {
				try {
					wait();
				} catch (InterruptedException e) {	
				}
			}
		}		
	}
	
	/**
	 * Aggiorna la ui in seguito all'arrivo di un MatchUpdate
	 * @param matchUpdate MatchUpdate da applicare
	 */

	public void updateUI(MatchUpdate matchUpdate) {
		ui.showLogMessage("Starting applying matchupdate");
		handleBroadcastUpdate(matchUpdate.getBroadcastUpdate());
		if (matchUpdate.getBroadcastUpdate().getMatchOver())
			ui.showMatchOver("Match is over. Nothing to do here.");
		if (matchUpdate.containsUnicastUpdates()) {
			ui.showLogMessage("Unicast update received");
			handleUnicastUpdate(matchUpdate.getUnicastUpdate());
			if (matchUpdate.getUnicastUpdate().getPlayerStatus().isDead() && !clientModel.isMatchOver()) {
				ui.showYouAreDead("You are dead!\nYou can continue to watch the game");
			}
			if (matchUpdate.getUnicastUpdate().getPlayerStatus().hasWon() && !clientModel.isMatchOver()) {
				ui.showYouWon("You won!\nYou can continue to watch the game");
			}
		}
		
	}
	
	/**
	 * Aggiorna la ui in seguito all'arrivo di un InitialInfo
	 * @param initialInfo InitialInfo da applicare
	 */
	
	public void updateUI(InitialInfo initialInfo) {
		ui.showLogMessage("Starting applying initialinfo");
		clientModel.renewValidator(new UnicastUpdate(initialInfo.getInitialStatus(), null));
		String map = initialInfo.getTextMap();
		String characterName = initialInfo.getCharacterName();
		ui.showInitialInfo(map, characterName); //MOSTRA MAPPA
		clientModel.setMap(map);
		clientModel.setPawnType(initialInfo.getPawnType());
		ui.showLogMessage("Initial info applied");
	}
	
	public ClientView getUI() {
		return ui;
	}
	
	public String getName() {
		return playerName;
	}
	
	/**
	 * Setta il gestore di output
	 * @param outputHandler
	 */
	
	private void setOutputHandler(OutputHandler outputHandler) {
		clientModel.setOutputHandler(outputHandler);
	}
	
	/**
	 * Avvia su thread la lettura di input
	 * Usato solo in cli
	 */

	@Override
	public void run() {
		ui.activateInput();
	}
	
	public void setPlayerName(String name) {
		playerName=name;
	}

	
	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof String)
			playerName=(String) arg;
		if (arg instanceof ParametersForConnection) {
			ParametersForConnection pars = (ParametersForConnection) arg;
			connect(pars.getServerParameters(), pars.getErrorUI());
		}		
	}
	
	/**
	 * Gestisce la stampa della parte unicast del MatchUpdate
	 * @param unicast
	 */
	
	public void handleUnicastUpdate(UnicastUpdate unicast) {
		ui.showUnicastMessages(unicast.getUnicastMessages()); //stampa unicast
		if (!clientModel.isTurnNumberUpdatedRelativeTo(unicast.getTurn())) {
			ui.showTurnNumber(unicast.getTurn());
		}
		if (!clientModel.isPositionUpdatedRelativeTo(unicast.getActualPosition())) {
			ui.showCurrentPosition(unicast.getActualPosition()); //e aggiorna la ui
		}
		if (!clientModel.areCardsUpdatedRelativeTo(unicast.getCardsInHand())){
			ui.showCardsInHand(unicast.getCardsInHand());
		}
		RemoteStatus lastKnownStatus = unicast.getPlayerStatus();
		if (lastKnownStatus.isOnTurn())
			ui.setOnTurn();
		else
			ui.setTurnOver();
		ui.setCanAttack(lastKnownStatus.canAttack() && clientModel.getPawnType().equals("Alien"));
		ui.setCanEndTurn(lastKnownStatus.canEndTurn());
		List<Position> usablePositions = unicast.getUsablePositions();//salva posizioni cliccabili
		ui.showUsablePositions(usablePositions);
		clientModel.setUnicast(unicast);
		clientModel.setCanDoNewAction();
		clientModel.renewValidator(unicast);
		if (!clientModel.isMatchStarted()) {
			clientModel.setMatchStarted();
		}
	}
	
	/**
	 * Gestisce la stampa della parte broadcast del MatchUpdate
	 * @param broadcast
	 */
	
	public void handleBroadcastUpdate(BroadcastUpdate broadcast) {
		if (!broadcast.getBroadcastMessages().isEmpty())
			ui.showBroadcastMessages(broadcast.getBroadcastMessages());
		if (!broadcast.getEventPositions().isEmpty())
			ui.showEventPositions(broadcast.getEventPositions());
		if (!clientModel.arePlayerListsUpdatedRelativeTo(broadcast.getPlayersInGame())) {
			ui.showPlayers(broadcast.getPlayersInGame());
		}
		if (!clientModel.isPlayingPlayerUpdatedRelativeTo(broadcast.getPlayingPlayer())) {
			ui.showPlayerInTurn(broadcast.getPlayingPlayer());
		}
		ui.showLogMessage("Matchupdate applied");
		clientModel.setBroadcast(broadcast);
		if (!clientModel.isMatchStarted()) {
			ui.showInfoMessage("Match is started");
		}
	}
}
