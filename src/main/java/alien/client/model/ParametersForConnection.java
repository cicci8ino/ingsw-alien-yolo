package alien.client.model;

import alien.client.network.ServerParameters;
import alien.client.view.ErrorUI;

public class ParametersForConnection {
	
	private final ServerParameters serverParameters;
	private final ErrorUI errorUI;
	
	/**
	 * Contiene i parametri necessare ad avviare correttamente la connessione
	 * @param serverParameters Parametri di connessione
	 * @param errorUI UI di errori da passare ad Input e Output Handlers
	 */
	
	public ParametersForConnection(ServerParameters serverParameters, ErrorUI errorUI) {
		this.serverParameters=serverParameters;
		this.errorUI=errorUI;
	}

	public ServerParameters getServerParameters() {
		return serverParameters;
	}

	public ErrorUI getErrorUI() {
		return errorUI;
	}
	
}
