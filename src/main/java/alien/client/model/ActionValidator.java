package alien.client.model;

import java.util.Map;

import alien.common.RemoteItemCard;
import alien.common.UnicastUpdate;
import alien.server.action.Attack;
import alien.server.action.EndTurn;
import alien.server.action.ItemCardDiscarded;
import alien.server.action.ItemCardSelected;
import alien.server.action.PositionSelected;
import alien.server.sector.Position;

public class ActionValidator {
	
	private final UnicastUpdate unicastUpdate;
	
	public ActionValidator(UnicastUpdate unicastUpdate) {
		this.unicastUpdate=unicastUpdate;
	}
	
	public boolean isActionValid(Attack action) {
		if (unicastUpdate.getPlayerStatus().canAttack())
			return true;
		return false;
	}

	public boolean isActionValid(EndTurn action) {
		if (unicastUpdate.getPlayerStatus().canEndTurn())
			return true;
		return false;
	}
	
	public boolean isActionValid(ItemCardDiscarded action) {
		if (!unicastUpdate.getPlayerStatus().canDiscardItemCard())
			return false;
		return isCardWithIndexDiscardable(action.getCardIndex());
		
	}
	
	public boolean isActionValid(ItemCardSelected action) {
		if (!unicastUpdate.getPlayerStatus().canSelectItemCard())
			return false;
		return isCardWithIndexUsable(action.getCardIndex());
		
	}
	
	public boolean isActionValid(PositionSelected action) {
		Position selectedPosition = action.getSelectedPosition();
		if (!unicastUpdate.getPlayerStatus().canSelectPosition())
			return false;
		return unicastUpdate.getUsablePositions().contains(selectedPosition);
	}
	
	private boolean isCardWithIndexUsable(int cardIndex) {
		Map<RemoteItemCard, Boolean> cardsInHand = unicastUpdate.getCardsInHand();
		for (RemoteItemCard aCard: cardsInHand.keySet()) {	
			int aCardIndex = aCard.getIndexNumber();
			if (cardIndex==aCardIndex && cardsInHand.get(aCard))
				return true;
		}
		return false;
	}
	
	private boolean isCardWithIndexDiscardable(int cardIndex) {
		Map<RemoteItemCard, Boolean> cardsInHand = unicastUpdate.getCardsInHand();
		for (RemoteItemCard aCard: cardsInHand.keySet()) {	
			int aCardIndex = aCard.getIndexNumber();
			if (cardIndex==aCardIndex)
				return true;
		}
		return false;
	}
	
	
}
