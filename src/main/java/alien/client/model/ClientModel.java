package alien.client.model;

import java.util.List;
import java.util.Map;
import java.util.Observable;

import alien.client.network.OutputHandler;
import alien.client.network.ServerParameters;
import alien.client.view.ErrorUI;
import alien.common.BroadcastUpdate;
import alien.common.PlayerLists;
import alien.common.RemoteItemCard;
import alien.common.UnicastUpdate;
import alien.server.action.Attack;
import alien.server.action.BaseAction;
import alien.server.action.EndTurn;
import alien.server.action.ItemCardDiscarded;
import alien.server.action.ItemCardSelected;
import alien.server.action.PositionSelected;
import alien.server.sector.Position;

public class ClientModel extends Observable implements Runnable {

	private ActionValidator actionValidator;
	private OutputHandler outputHandler;
	private ErrorUI errorUI;
	
	private ServerParameters serverParametersForConnection;
	private BaseAction actionToSend;
	private String nameToSend;
	private String chatToSend;
	private String map;
	private String pawnType;

	private UnicastUpdate currentUnicast;
	private BroadcastUpdate currentBroadcast;
	private Integer turnNumber=new Integer(0);

	private boolean needToConnect = false;
	private boolean needToSendAction = false ;
	private boolean needToSendChat = false;
	private boolean needToSendName = false;
	private boolean actionIsDone = false;
	private boolean matchIsStarted = false;
	private boolean matchIsOver = false;
	
	public ClientModel(ErrorUI errorUI) {
		this.errorUI = errorUI;
	}
	/**
	 * Rinnova l'Action validator dato un determinato Unicast
	 * @param unicast
	 */
	public void renewValidator(UnicastUpdate unicast) {
		this.actionValidator=new ActionValidator(unicast);
	}
	
	/**
	 * Invia azione di attacco solo se questa è consentita da unicast update
	 * @param action
	 */
	
	public void sendCommand(Attack action) {
		if (pawnType.equals("Human") || !actionValidator.isActionValid(action) || actionIsDone)
			errorUI.showErrorMessage("Cannot attack");
		else {
			needToSendAction(action);
		}
	}
	/**
	 * Invia azione di fine turno
	 * @param action
	 */
	public void sendCommand(EndTurn action){
		if (!actionValidator.isActionValid(action) || actionIsDone)
			errorUI.showErrorMessage("Cannot end turn");
		else {
			needToSendAction(action);
		}
	}
	/**
	 * Invia azione "carta scartata"
	 * @param action
	 */
	public void sendCommand(ItemCardDiscarded action){
		if (!actionValidator.isActionValid(action) || actionIsDone)
			errorUI.showErrorMessage("Cannot discard this card");
		else {
			needToSendAction(action);
		}
	}

	/**
	 * Invia azione "carta selezionata"
	 * @param action
	 */
	public void sendCommand(ItemCardSelected action){
		if (!actionValidator.isActionValid(action) || actionIsDone)
			errorUI.showErrorMessage("Cannot use this card");
		else {
			needToSendAction(action);
		}
	}
	/**
	 * Invia azione "posizione selezionata"
	 * @param action
	 */
	public void sendCommand(PositionSelected action){
		if (!actionValidator.isActionValid(action) || actionIsDone)
			errorUI.showErrorMessage("Cannot select this position");
		else {
			needToSendAction(action);
		}
	}
	/**
	 * Avvia il thread che si occupa di inviare le azioni, la chat, il nome e di avviare la connessione.
	 */
	@Override
	public void run() {
		if (needToConnect) {
			needToConnect=false;
			setChanged();
			notifyObservers(new ParametersForConnection(serverParametersForConnection, errorUI));
			return;
		}
		if (needToSendName) {
			needToSendName=false;
			outputHandler.sendUsername(nameToSend);
			return;
		}
		if (!matchIsStarted) {
			errorUI.showErrorMessage("Match is not started yet");
			return;
		}
		if (needToSendAction) {
			needToSendAction=false;
			outputHandler.sendAction(actionToSend);
		}
		if (needToSendChat) {
			needToSendChat=false;
			outputHandler.sendChatMessage(chatToSend, nameToSend);
		}
	}
	
	/**
	 * Preapara l'invio dell'azione e la effettua da thread. Necessario alla funzione run effettuerà la funzionalità richiesta
	 * @param action
	 */
	
	public void needToSendAction(BaseAction action) {
		needToSendAction=true;
		actionToSend = action;
		new Thread(this).start();
	}
	
	/**
	 * Prepara l'avvio della connessione e lo effettua da thread
	 * @param parameters
	 */
	
	public void requestToConnect(ServerParameters parameters) {
		serverParametersForConnection=parameters;
		needToConnect=true;
		new Thread(this).start();
	}
	
	/**
	 * Prepara e invia nome, tramite thread
	 * @param name
	 */
	
	public void setAndSendName(String name) {
		needToSendName=true;
		nameToSend=name;
		setChanged();
		notifyObservers(name);
		new Thread(this).start();
	}
	
	/**
	 * Prepara e invia la chat, tramite thread
	 * @param message
	 */
	
	public void sendChat(String message) {
		needToSendChat = true;
		chatToSend = message;
		new Thread(this).start();
	}
	
	/**
	 * Setta il gestore di output desiderato
	 * @param outputHandler
	 */
	
	public void setOutputHandler(OutputHandler outputHandler) {
		this.outputHandler=outputHandler;
	}
	
	public ErrorUI getErrorUI() {
		return errorUI;
	}
	
	public void setMatchStarted() {
		matchIsStarted=true;
	}
	
	public void setCanDoNewAction() {
		actionIsDone=false;
	}
	
	public boolean isMatchStarted() {
		return matchIsStarted;
	}
	
	public void setMap(String map) {
		this.map=map;
	}
	
	public String getMap() {
		return map;
	}
	
	/**
	 * Check if position saved needs to be updated with parameter
	 * @param position
	 * @return 
	 */
	
	public boolean isPositionUpdatedRelativeTo(Position position) {
		if (!matchIsStarted)
			return false;
		return position.equals(currentUnicast.getActualPosition());
	}
	
	/**
	 * Check if cards saved needs to be updated with parameter
	 * @param cards
	 * @return 
	 */
	
	public boolean areCardsUpdatedRelativeTo(Map<RemoteItemCard, Boolean> cards) {
		if (!matchIsStarted)
			return false;
		return cards.equals(currentUnicast.getCardsInHand());
	}
	
	/**
	 * Salva l'unicast
	 * @param unicast
	 */
	
	public void setUnicast(UnicastUpdate unicast) {
		this.currentUnicast=unicast;
	}
	
	/**
	 * Salva broadcast
	 * @param broadcast
	 */
	
	public void setBroadcast(BroadcastUpdate broadcast) {
		this.currentBroadcast=broadcast;
	}
	
	/**
	 * Check if saved players lists need to be updated with parameter
	 * @param lists
	 * @return 
	 */
	
	public boolean arePlayerListsUpdatedRelativeTo(PlayerLists lists) {
		if (!matchIsStarted)
			return false;
		return lists.equals(currentBroadcast.getPlayersInGame());
	}
	
	/**
	 * Controlla se il nome del giocatore salvato è aggiornato rispetto a quello ricevuto
	 * @param playingPlayer Nome giocatore ricevuto
	 * @return
	 */
	
	public boolean isPlayingPlayerUpdatedRelativeTo(String playingPlayer) {
		if (!matchIsStarted)
			return false;
		return playingPlayer.equals(currentBroadcast.getPlayingPlayer());
	}
	
	public PlayerLists getPlayerLists() {
		return currentBroadcast.getPlayersInGame();
	}
	
	public List<Position> getUsablePositions() {
		return currentUnicast.getUsablePositions();
	}
	
	public Map<RemoteItemCard, Boolean> getCardsInHand() {
		return currentUnicast.getCardsInHand();
	}
	
	/**
	 * Controlla se il numero del turno del parametro è uguale a quello ricevuto
	 * @param turn Turno ricevuto
	 * @return Esito controllo
	 */
	
	public boolean isTurnNumberUpdatedRelativeTo(Integer turn) {
		if (!matchIsStarted)
			return false;
		return turnNumber.equals(turn);
	}
	
	public void setTurnNumber(Integer turn) {
		turnNumber=turn;
	}
	
	public boolean isMatchOver() {
		return matchIsOver;
	}
	
	public void setMatchIsOver() {
		matchIsOver=true;
	}
	public void setPawnType(String pawnType) {
		this.pawnType=pawnType;
	}
	public String getPawnType() {
		return pawnType;
	}
}
