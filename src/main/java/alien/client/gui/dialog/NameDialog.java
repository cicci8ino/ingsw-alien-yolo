package alien.client.gui.dialog;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import alien.client.model.ClientModel;

public class NameDialog extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6888779059771542084L;

	private JPanel contentPane;
	
	private JTextField nameTextField = new JTextField();
	private JLabel nameLabel = new JLabel("Insert name:");
	
	private JFrame thisObject;
	
	private JButton confirmNameButton = new JButton("Confirm");
	
	ClientModel clientModel;
	
	public NameDialog(ClientModel actionPerformer) {
		setResizable(false);
		setTitle("Insert server parameters");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 200, 150);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5)); //crea i bordi
		contentPane.setLayout(new GridLayout(4, 0)); //setta il layout
		setContentPane(contentPane);
		contentPane.add(nameLabel);
		contentPane.add(nameTextField);
		contentPane.add(new JPanel());
		contentPane.add(confirmNameButton);
		confirmNameButton.addActionListener(getConfirmNameAction());
		this.clientModel=actionPerformer;
		thisObject=this;
		setLocationRelativeTo(null);
	}

	private ActionListener getConfirmNameAction() {
        ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	String name = nameTextField.getText();
                clientModel.setAndSendName(name);
                thisObject.setVisible(false);

            }
        };
       
        return action;
    }
	
	
	
}
