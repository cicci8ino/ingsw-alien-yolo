package alien.client.gui.dialog;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class LoadingDialog extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5290937806300483831L;


	public LoadingDialog(String playerName) {
		setResizable(false);
		setTitle("Loading");
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0;
		c.gridy=0;
		add(new JLabel("Welcome "+playerName), c);
		c.gridy=1;
		add(new JLabel("Waiting for match to start"), c);
		
		
		
	}

	public static void main(String[] args) {
		LoadingDialog testDialog = new LoadingDialog("Andrea");
		testDialog.setSize(200, 200);
		testDialog.setVisible(true);
	}
	
	
	public void showDialog() {
		setSize(200, 200);
		setVisible(true);
		setLocationRelativeTo(null);
	}
	
}
