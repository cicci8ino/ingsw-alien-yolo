package alien.client.gui.dialog;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MatchIsOverDialog {
	
	String message;
	JFrame gameWindow;

	public MatchIsOverDialog(String message, JFrame gameWindow) {

		this.message=message;
		this.gameWindow=gameWindow;
			
	}
	
	public void showPopup() {
		
		JOptionPane.showMessageDialog(gameWindow, message, null, JOptionPane.PLAIN_MESSAGE, null);
		
	}
	
}
