package alien.client.gui.dialog;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import alien.client.model.ClientModel;
import alien.client.network.ServerParameters;

public class ConnectionDialog extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1431466331281103775L;

	private JPanel contentPane;
	
	private JTextField serverTextField = new JTextField();
	private JTextField chatPortTextField = new JTextField();
	private JTextField gamePortTextField = new JTextField();
	
	private JLabel serverLabel = new JLabel("Server Address:");
	private JLabel gamePortLabel = new JLabel("Game Port:");
	private JLabel chatPortLabel = new JLabel("Chat Port:");
	
	private JButton connectButton = new JButton("Connect");
	private JButton defaultConnectButton = new JButton("Default");
	

	final private transient ClientModel clientModel;
	
	final private JFrame thisObject;
	
	public ConnectionDialog(ClientModel actionPerformer) {
		this.clientModel=actionPerformer;
		setResizable(false);
		setTitle("Insert server parameters");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 230, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5)); //crea i bordi
		contentPane.setLayout(new GridLayout(9, 0)); //setta il layout
		setContentPane(contentPane); //setta il conenuto del frame
		contentPane.add(serverLabel);
		contentPane.add(serverTextField);
		contentPane.add(gamePortLabel);
		contentPane.add(gamePortTextField);
		contentPane.add(chatPortLabel);
		contentPane.add(chatPortTextField);
		contentPane.add(new JPanel()); //empty space
		contentPane.add(connectButton);
		connectButton.addActionListener(getConnectButtonAction());
		contentPane.add(defaultConnectButton);
		defaultConnectButton.addActionListener(getDefaultConnectButtonAction());
		thisObject=this;
		setLocationRelativeTo(null);
      
	}
        
	
	private ActionListener getConnectButtonAction() {
        ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	String address = serverTextField.getText();
            	String chatPort = chatPortTextField.getText();
            	String gamePort = gamePortTextField.getText();
            	ServerParameters serverParameters = new ServerParameters(address, gamePort, chatPort);
                clientModel.requestToConnect(serverParameters);
                thisObject.setVisible(false);

            }
        };
       
        return action;
    }
	
	private ActionListener getDefaultConnectButtonAction() {
        ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	String address = "127.0.0.1";
            	String chatPort = "2006";
            	String gamePort = "2005";
            	ServerParameters serverParameters = new ServerParameters(address, gamePort, chatPort);
                clientModel.requestToConnect(serverParameters);
                thisObject.setVisible(false);

            }
        };
       
        return action;
    }
	
	
	
}
