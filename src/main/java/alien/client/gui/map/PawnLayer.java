package alien.client.gui.map;

import java.util.List;

import alien.client.gui.hexagon.PawnHexagon;
import alien.server.sector.Position;

public class PawnLayer extends OverlayLayer {


	/**
	 * 
	 */
	private static final long serialVersionUID = 6437577717496248943L;

	@Override
	public synchronized void updatePositions(List<Position> position) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public synchronized void updatePosition(Position position) {
		clear();
		overlayHexagons.add(new PawnHexagon(position));
		repaint();
		
	}
	
}
