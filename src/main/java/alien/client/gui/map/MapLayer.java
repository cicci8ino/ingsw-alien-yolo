package alien.client.gui.map;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;

import javax.swing.JPanel;

import alien.client.gui.hexagon.*;
import alien.client.gui.panels.Configuration;
import alien.client.gui.panels.CustomColor;

public class MapLayer extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8086602697364098475L;
	final List<Hexagon> hexagons;
	final static int BACKGROUND_LINES_FOR_DIAMETER = 3; 
	final static int BACKGROUND_LINES_ANGLE = 45; 
	int radius;
	
	public MapLayer(String textMap) {
		this.radius=(int) Hexagon.RADIUS; 
		this.setSize(MapLayeredPane.WIDTH, MapLayeredPane.HEIGHT);
		this.hexagons= HexagonsBuilder.getGuiSectorFromTextMap(textMap);
	}
	
	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();
        Configuration.setRenderingHints(g2d);
        drawBackground(g2d);
        drawHexagons(g2d);
    }
	
	public void drawBackground(Graphics2D g2d) {
		double radiusInRadians = Math.toRadians(BACKGROUND_LINES_ANGLE);
		double panelSizeX = this.getWidth();
		double panelSizeY = this.getHeight();
		double arcTanRadius = 1/Math.tan(radiusInRadians);
		g2d.setColor(CustomColor.BORDER);
		for (double startX=-panelSizeY; startX<=panelSizeX; startX+=2*radius/BACKGROUND_LINES_FOR_DIAMETER) {
			g2d.drawLine((int)startX, 0, (int)(arcTanRadius*panelSizeX+startX), (int)panelSizeX);
		}	
	}
	
	public void drawHexagons(Graphics2D g2d) {
		for (Hexagon aHexagon:hexagons) {
			aHexagon.draw(g2d);
		}
	}
	
}
