package alien.client.gui.map;

import java.util.List;

import alien.client.gui.hexagon.Hexagon;
import alien.client.gui.hexagon.UsableHexagon;
import alien.server.sector.Position;

public class UsableLayer extends OverlayLayer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 982035678115944147L;

	@Override
	public void updatePositions(List<Position> position) {
		synchronized(this) {
			clear();	
		}
		
		for (Position aPosition:position) {
			overlayHexagons.add(new UsableHexagon(aPosition));
		}
		repaint();
	}

	@Override
	public synchronized void updatePosition(Position position) {
		// TODO Auto-generated method stub
		
	}

	public List<Hexagon> getUsableHexagons() {
		return overlayHexagons;
	}
	
	
}
