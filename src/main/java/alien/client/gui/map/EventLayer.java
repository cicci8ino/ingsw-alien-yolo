package alien.client.gui.map;

import java.util.List;

import alien.client.gui.hexagon.EventHexagon;
import alien.server.sector.Position;

public class EventLayer extends OverlayLayer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5303791773051314017L;

	@Override
	public synchronized void updatePositions(List<Position> position) {
		clear();
		for (Position aPosition:position) {
			overlayHexagons.add(new EventHexagon(aPosition));
		}
		repaint();
	}

	@Override
	public synchronized void updatePosition(Position position) {
		
		
	}
	
}
