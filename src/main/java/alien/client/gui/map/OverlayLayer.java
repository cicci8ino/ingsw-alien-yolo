package alien.client.gui.map;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import alien.client.gui.hexagon.Hexagon;
import alien.client.gui.panels.Configuration;
import alien.server.sector.Position;

public abstract class OverlayLayer extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6705460976314160238L;

	public OverlayLayer() {
		this.setSize(Configuration.MAP_WIDTH,Configuration.MAP_HEIGHT);
		setOpaque(false);
	}
	
	List<Hexagon> overlayHexagons = new ArrayList<Hexagon>();
	
	public abstract void updatePositions(List<Position> position);
	public abstract void updatePosition(Position position);
	public void clear() {;
		while (overlayHexagons.size()>0) {
			overlayHexagons.remove(0);
		}
	}
	
	@Override
    public synchronized void paintComponent(Graphics g) {
		super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();
        Configuration.setRenderingHints(g2d);
        if (!overlayHexagons.isEmpty()) {
        	for (Hexagon aHexagon:overlayHexagons) {
        		aHexagon.draw(g2d);
        	}
        }
    }
	
}
