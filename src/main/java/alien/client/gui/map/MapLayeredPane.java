package alien.client.gui.map;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.JLayeredPane;
import alien.client.gui.hexagon.Hexagon;
import alien.client.gui.panels.Configuration;
import alien.client.model.ClientModel;
import alien.server.action.PositionSelected;
import alien.server.sector.Position;

public class MapLayeredPane extends JLayeredPane {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1140883854519200875L;
	public static int WIDTH=Configuration.MAP_WIDTH;
	public static int HEIGHT=Configuration.MAP_HEIGHT;
	
	private OverlayLayer pawnLayer;
	private OverlayLayer eventLayer;
	private OverlayLayer usableLayer;
	private MapLayer mapLayer;
	private ClientModel actionPerformer;
	
	public MapLayeredPane(String map, ClientModel actionPerformer) {
		pawnLayer =  new PawnLayer();
		eventLayer = new EventLayer();
		usableLayer = new UsableLayer();
		mapLayer = new MapLayer(map);
		this.addMouseListener(new MapMouseListener());
		this.actionPerformer=actionPerformer;
	}
	
	public void dispose() {
		add(eventLayer, new Integer(3));
		add(usableLayer, new Integer(2));
		add(pawnLayer, new Integer(1));
		add(mapLayer, new Integer(0));
	}

	public OverlayLayer getPawnLayer() {
		return pawnLayer;
	}

	public OverlayLayer getEventLayer() {
		return eventLayer;
	}

	public OverlayLayer getUsableLayer() {
		return usableLayer;
	}
	
	public void updateUsablePositions(List<Position> usablePosition) {
		usableLayer.updatePositions(usablePosition);
	}
	
	public void updateEventPositions(List<Position> eventPosition) {
		eventLayer.updatePositions(eventPosition);
	}
	
	public void updatePlayerPosition(Position position) {
		pawnLayer.updatePosition(position);
	}

	class MapMouseListener implements MouseListener {
	@Override
	public void mouseClicked(MouseEvent arg0) {
		int posX = arg0.getX();
		int posY = arg0.getY();
		for (Hexagon aHexagon:((UsableLayer) usableLayer).getUsableHexagons())
			if (aHexagon.contains(new Point(posX,posY))){
				Position positionToSend = aHexagon.getCenter();
				actionPerformer.sendCommand(new PositionSelected(positionToSend));
			}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// NOT USED
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// NOT USED	
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// NOT USED
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// NOT USED
	}
	}
	
	public void setActionPerformer(ClientModel actionPerformer) {
		this.actionPerformer=actionPerformer;
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);
	}
	
}
