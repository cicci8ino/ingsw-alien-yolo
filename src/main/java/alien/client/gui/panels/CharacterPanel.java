package alien.client.gui.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import alien.client.model.ClientModel;
import alien.common.RemoteItemCard;
import alien.server.action.Attack;
import alien.server.action.EndTurn;

public class CharacterPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2699370509922322134L;
	JTextField writeChatTextField = new JTextField();
	JLabel characterNameLabel;
	JButton attackButton = new JButton("Attack");
	JButton endTurnButton = new JButton("End Turn");
	JButton showCardsButton = new JButton("Show cards");
	JRadioButton onTurnButton = new JRadioButton("On turn");
	ClientModel clientModel;
	CardsPanel cardsPanel;
	JFrame cardsWindow = new JFrame();
	JLabel turnLabel = new JLabel("Turn");
	JLabel turnNumber = new JLabel("0/39");
	
	public CharacterPanel(String characterName, ClientModel actionPerformer) {
		onTurnButton.setSelected(false);
		onTurnButton.setEnabled(false);
		this.cardsPanel=new CardsPanel(actionPerformer);
		cardsWindow.setTitle("Cards in hand");
		cardsWindow.setResizable(false);
		this.cardsWindow.add(cardsPanel);
		this.clientModel = actionPerformer;
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridx=0;
		c.gridy=0;
		this.add(new ImagePanel("Logo", "png"), c);
		characterNameLabel = new JLabel(characterName);
		
		c.gridy=1;
		this.add(new ImagePanel(characterName, "png"),c);
		
		c.gridy=2;
		this.add(characterNameLabel, c);
		c.gridy=3;
		
		attackButton.addActionListener(attackActionListener());
		this.add(attackButton, c);
		
		c.gridy=4;
		endTurnButton.addActionListener(endTurnActionListener());
		this.add(endTurnButton, c);
		
		c.gridy=5;
		showCardsButton.addActionListener(showCardsActionListener());
		this.add(showCardsButton, c);
		
		c.gridy=6;
		this.add(onTurnButton, c);
		
		c.gridy=7;
		this.add(turnLabel, c);
		
		c.gridy=8;
		this.add(turnNumber, c);
		
		attackButton.setEnabled(false);
		endTurnButton.setEnabled(false);
		
	}
	
	public void setAttackButton(boolean value) {
		attackButton.setEnabled(value);
	}
	
	public void setEndTurnButton(boolean value) {
		endTurnButton.setEnabled(value);
	}
	
	
	private ActionListener endTurnActionListener() {
        ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clientModel.sendCommand(new EndTurn());
            }
        };
       
        return action;
    }
	
	private ActionListener attackActionListener() {
        ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	clientModel.sendCommand(new Attack());
            }
        };
       
        return action;
    }
	
	private ActionListener showCardsActionListener() {
        ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	if (cardsPanel.containsCard()) 	{
            		cardsWindow.setSize(cardsPanel.getPreferredSize());
            		cardsWindow.setVisible(true);
            	}
            	else {
            		JOptionPane.showMessageDialog(null, "No cards in hand", "Error", JOptionPane.PLAIN_MESSAGE, null);
            	}           	
            }
        }; 
        return action;
	}
	
	public void updateCards(Map<RemoteItemCard, Boolean> cardsInHand) {
		cardsPanel.updateCards(cardsInHand);
		cardsWindow.setSize(cardsPanel.getPreferredSize());
		if (!cardsInHand.isEmpty())
			cardsWindow.setVisible(true);
		else 
			cardsWindow.setVisible(false);
	}
	
	public void setOnTurn() {
		attackButton.setEnabled(true);
		if (clientModel.getPawnType().equals("Human"))
			attackButton.setEnabled(false);
		endTurnButton.setEnabled(true);
		onTurnButton.setSelected(true);
	}
	
	public void setTurnOver() {
		attackButton.setEnabled(false);
		endTurnButton.setEnabled(false);
		onTurnButton.setSelected(false);
	}
	
	public void setCanAttack(boolean value) {
		attackButton.setEnabled(value);
	}
	
	public void setCanEndTurn(boolean value) {
		endTurnButton.setEnabled(value);
	}
	
	public void setTurn(int turnNumber) {
		this.turnNumber.setText(turnNumber+"/39");
	}
}
