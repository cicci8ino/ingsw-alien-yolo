package alien.client.gui.panels;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ImagePanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7248978342195186874L;
	private transient BufferedImage image;
	private String fileSeparator = "/";
	
	public static final int CARD_SIZE_Y = 220;
	public static final int CARD_SIZE_X = 131;

	public ImagePanel(String cardName, String extension)  { 
			try {
				image = ImageIO.read(getClass( ).getResourceAsStream(fileSeparator+"images"+fileSeparator+cardName+"."+extension));
			} catch (IOException e) {
				//NO NEED TO HANDLE THIS EXCEPTION. FILE NAME ARE KNOWN
			}
	    }

	    @Override
	    protected void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        g.drawImage(image, 0, 0, null);            
	    }
	    
	    @Override
	    public Dimension getPreferredSize() {
	    	return new Dimension(image.getWidth(), image.getHeight());
	    }
	    
	    public int getSizeX() {
	    	return image.getWidth();
	    }
	    
	    public int getSizeY() {
	    	return image.getWidth();
	    }
}