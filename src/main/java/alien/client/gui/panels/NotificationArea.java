package alien.client.gui.panels;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.DefaultCaret;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import alien.client.view.CLI;

public class NotificationArea extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4137634812911668996L;
	JScrollPane scrollPane;
	JTextPane textPanel;
	
	public NotificationArea(){
		
		this.textPanel = new JTextPane();
		this.scrollPane = new JScrollPane(textPanel);
		scrollPane.setPreferredSize(new Dimension(Configuration.LEFT_AREA_X_SIZE,380));
		this.add(scrollPane);
		textPanel.setHighlighter(null);
	}
	
	public void append(Color c, String s) { 
		
		StyleContext sc = StyleContext.getDefaultStyleContext();
		AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY,
				StyleConstants.Foreground, c);
		int len = textPanel.getDocument().getLength(); 
		textPanel.setCaretPosition(len); 
		DefaultCaret caret = (DefaultCaret)textPanel.getCaret();
		caret.setUpdatePolicy(DefaultCaret.OUT_BOTTOM);
		textPanel.setCharacterAttributes(aset, false);
		textPanel.setEditable(true);
		textPanel.replaceSelection(s+"\n");;
		textPanel.setEditable(false);
		
	}
	
	public void showBroadcastMessage(String message) {
		append(Color.BLUE, CLI.BROADCAST_MESSAGE_HEADER+message);
	}
	
	public void showChatMessage(String message) {
		append(Color.RED, message);
	}
	
	public void showUnicastMessage(String message) {
		append(Color.GREEN, CLI.UNICAST_MESSAGE_HEADER+message);
	}
	
}
