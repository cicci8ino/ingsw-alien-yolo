package alien.client.gui.panels;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.DefaultCaret;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import alien.client.gui.map.MapLayeredPane;
import alien.common.PlayerLists;

public class PlayersArea extends JTextPane {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8944076570736158784L;
	PlayerLists playerLists;
	String playerInTurn = "";
	
	public PlayersArea() {
		this.setPreferredSize(new Dimension(Configuration.LEFT_AREA_X_SIZE, MapLayeredPane.HEIGHT/3));
		this.setEditable(false); 
		this.setBounds(5, 5, 5, 5);
		
	}
	
	public void setPlayerInTurn(String player) {
		playerInTurn = player;
		updatePlayers(playerLists);
	}
	
	public void updatePlayers(PlayerLists players) {
		this.setEditable(true);
		this.playerLists=players;
		
		this.setText(null); //cancella tutto
		for (String aName:playerLists.getUnknownTypePlayers())
			if (aName.equals(playerInTurn))
				append(Color.GREEN, aName);
			else
				append(Color.RED, aName);
		for (String aName:playerLists.getDeadPlayers())
			append(Color.BLACK, aName);
		this.setEditable(false); 
		
	}
	
	public void append(Color color, String playerName) {
		StyleContext sc = StyleContext.getDefaultStyleContext();
		AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY,
				StyleConstants.Foreground, color);
		int len = getDocument().getLength();
		setCaretPosition(len); 
		DefaultCaret caret = (DefaultCaret)this.getCaret();
		caret.setUpdatePolicy(DefaultCaret.OUT_BOTTOM);
		setCharacterAttributes(aset, false);
		replaceSelection(playerName+"\n");
	}
	
}
