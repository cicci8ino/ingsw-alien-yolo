package alien.client.gui.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import alien.client.gui.map.MapLayeredPane;
import alien.client.model.ClientModel;
import alien.common.PlayerLists;
import alien.common.RemoteItemCard;
import alien.server.sector.Position;

public class CompleteGUI extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3126568920863709701L;
	private CharacterPanel characterPanel;
	private MapLayeredPane map;
	private LeftArea leftArea;
	
	public CompleteGUI(String textMap, String characterName, ClientModel actionPerformer) {
		
		characterPanel = new CharacterPanel(characterName, actionPerformer);
		map = new MapLayeredPane(textMap, actionPerformer);
		map.dispose();
		leftArea = new LeftArea(actionPerformer);
		
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0;
		c.gridy=0;
		this.add(leftArea, c);
		
		c.gridx=1;
		c.gridy=0;
		this.add(map, c);
		
		c.gridx=2;
		c.gridy=0;
		this.add(characterPanel, c);
		
	}
	
	//CONTROLLO CARTE
	
	public void updateCardsInHand(Map<RemoteItemCard, Boolean> cards) {
		characterPanel.updateCards(cards);
	}
	
	//CONTROLLO LISTA GIOCATORI
	
	public void updatePlayerLists(PlayerLists players) {
		leftArea.playerArea.updatePlayers(players);
	}
	
	public void updatePlayerOnTurn(String name) {
		leftArea.playerArea.setPlayerInTurn(name);
	}
	
	// CONTROLLO MAPPA
	
	public void updatePlayerPosition(Position position) {
		map.updatePlayerPosition(position);
	}
	
	public void updateUsablePositions(List<Position> positions) {
		map.updateUsablePositions(positions);
	}
	
	public void updateEventPositions(List<Position> positions) {
		map.updateEventPositions(positions);
	}
	
	//CONTROLLO CHAT
	
	//CONTROLLO NOTIFICHE
	
	public MapLayeredPane getMap() {
		return map;
	}
	
	public void setOnTurn() {
		characterPanel.setOnTurn();
	}
	
	public void setTurnOver() {
		characterPanel.setTurnOver();
	}
	
	public void showBroadcastMessage(String message) {
		leftArea.notificationArea.showBroadcastMessage(message);
	}
	
	public void showChatMessage(String message) {
		leftArea.notificationArea.showChatMessage(message);
	}
	
	public void showUnicastMessage(String message) {
		leftArea.notificationArea.showUnicastMessage(message);
	}
	
	public void setTurn(int turnNumber) {
		this.characterPanel.setTurn(turnNumber);
	}
	
	public void setCanAttack(boolean value) {
		this.characterPanel.setCanAttack(value);
	}
	
	public void setCanEndTurn(boolean value) {
		this.characterPanel.setCanEndTurn(value);
	}
	
}
