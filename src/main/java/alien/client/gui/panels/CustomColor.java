package alien.client.gui.panels;

import java.awt.Color;
import java.awt.color.ColorSpace;

public class CustomColor extends Color {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6442756597660420451L;
	public final static Color BORDER = new Color(120, 122, 118);
	public final static Color DANGEROUS_SECTOR=new Color(204, 204, 204);
	public final static Color USABLE_SECTOR = new Color(51, 255, 51, 60);
	public final static Color PAWN_SECTOR = new Color(255, 0, 0);
	public final static Color EVENT_SECTOR = new Color(255, 255, 51, 60);
	
	public CustomColor(ColorSpace arg0, float[] arg1, float arg2) {
		super(arg0, arg1, arg2);
	}

	public CustomColor(float arg0, float arg1, float arg2, float arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public CustomColor(float arg0, float arg1, float arg2) {
		super(arg0, arg1, arg2);
	}

	public CustomColor(int arg0, boolean arg1) {
		super(arg0, arg1);
	}

	public CustomColor(int arg0, int arg1, int arg2, int arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public CustomColor(int arg0, int arg1, int arg2) {
		super(arg0, arg1, arg2);
	}

	public CustomColor(int arg0) {
		super(arg0);
	}

}
