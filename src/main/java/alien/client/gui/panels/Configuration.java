package alien.client.gui.panels;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;

public class Configuration {
	
	public static final int LEFT_AREA_X_SIZE=230;
	public static final int HEXAGON_RADIUS = 25;
	
	public static final int MAP_BORDER_X = 10;
	public static final int MAP_BORDER_Y = 10;
	
	public static final int MAP_WIDTH = (int) Math.round(HEXAGON_RADIUS*(24*3*0.5))+2*MAP_BORDER_X;
	public static final int MAP_HEIGHT = (int) Math.round(HEXAGON_RADIUS*Math.sqrt(3)*29*0.5)+2*MAP_BORDER_Y;
	
	public static final int RESOLUTION_X = 1366;
	public static final int RESOLUTION_Y = 768;
	
	public static final int CENTER_SCREEN_X = Toolkit.getDefaultToolkit().getScreenSize().width;
	public static final int CENTER_SCREEN_Y = Toolkit.getDefaultToolkit().getScreenSize().height;
	
	public static void setRenderingHints(Graphics2D g2d) {
		
		g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
	    g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
	    g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
	    g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
	    g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
	    
	}

}
