package alien.client.gui.panels;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import alien.client.model.ClientModel;
import alien.common.RemoteItemCard;
import alien.server.action.ItemCardDiscarded;
import alien.server.action.ItemCardSelected;

public class CardsPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7231349353332177481L;
	private transient ClientModel clientModel;
	private transient Map<ImagePanel, RemoteItemCard> imageToCard = new HashMap<ImagePanel, RemoteItemCard>();
	public static final int CARDS_GAP = 20;
	
	public CardsPanel(ClientModel actionPerformer) {
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.addMouseListener(new CardsMouseListener());
		this.clientModel = actionPerformer;
	}
	
	public void updateCards(Map<RemoteItemCard, Boolean> cardsInHand) {
		
		clear();
		if (!cardsInHand.isEmpty())
			this.setLayout(new GridLayout(0, cardsInHand.size()));
		for (RemoteItemCard aCard:cardsInHand.keySet()) {
			ImagePanel cardPanel = new ImagePanel(aCard.getName(), "png");
			imageToCard.put(cardPanel, aCard);
			this.add(cardPanel);
			cardPanel.repaint();
		}
		repaint();
	}
	
	public void clear() {
		this.removeAll();
		imageToCard.clear();
	}
	
	
	
	class CardsMouseListener implements MouseListener {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			int posX = arg0.getX();;
			if (!imageToCard.isEmpty()) {
				for (ImagePanel aPanel:imageToCard.keySet()){
					RemoteItemCard clickedCard = imageToCard.get(aPanel);
					if(aPanel.getX()+aPanel.getSizeX()>=posX && aPanel.getX()<=posX) {
						int choice;
						if ("Alien".equals(clientModel.getPawnType()))
							choice = getChoiceFromPopupAlien(clickedCard.getName());	
						else
							choice = getChoiceFromPopup(clickedCard.getName());	
						if (choice==1) {
							clientModel.sendCommand(new ItemCardDiscarded(clickedCard.getIndexNumber()));
							
						}
						if (choice==2) {
							clientModel.sendCommand(new ItemCardSelected(clickedCard.getIndexNumber()));					
						}
					}
				}
			}
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// NOT USED
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			//NOT USED
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			//NOT USED
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			//NOT USED
		}	
	}

	public int getChoiceFromPopup(String cardName) {
		String[] options = new String[] {"Cancel", "Discard", "Use"};
		int response = JOptionPane.showOptionDialog(null, cardName, "Item Card",
		     JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
		     null, options, options[0]);
		return response;
		
	}
	
	public int getChoiceFromPopupAlien(String cardName) {
		String[] options = new String[] {"Cancel", "Discard"};
		int response = JOptionPane.showOptionDialog(null, cardName, "Item Card",
		     JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
		     null, options, options[0]);
		return response;
		
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(ImagePanel.CARD_SIZE_X*imageToCard.size()+CARDS_GAP, ImagePanel.CARD_SIZE_Y+CARDS_GAP);
	}

	public boolean containsCard() {
		return !imageToCard.isEmpty();
	}
	
	
}
	


