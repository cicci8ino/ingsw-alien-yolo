package alien.client.gui.panels;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import alien.client.gui.map.MapLayeredPane;
import alien.client.model.ClientModel;

public class LeftArea extends JPanel{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8960153537528770657L;
	PlayersArea playerArea = new PlayersArea();
	NotificationArea notificationArea = new NotificationArea();
	JTextField chatField = new JTextField();
	JLabel playersLabel = new JLabel("Players");
	JLabel notificationLabel = new JLabel("Notifications");
	JSeparator separator = new JSeparator();
	JPanel thisObject;
	ClientModel clientModel;
	
	public LeftArea(ClientModel actionPerformer) {
		chatField.setPreferredSize(new Dimension(Configuration.LEFT_AREA_X_SIZE,30));
		separator.setPreferredSize(new Dimension(Configuration.LEFT_AREA_X_SIZE, 2));
		this.clientModel=actionPerformer;
		thisObject=this;
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridx=0;
		c.gridy=0;
		this.add(separator, c);
		
		c.gridy=1;
		this.add(playersLabel, c);
		
		c.gridy=2;
		this.add(playerArea, c);
		
		c.gridy=3;
		this.add(notificationLabel, c);
		
		c.gridy=4;
		this.add(separator, c);
		
		c.gridy=5;
		this.add(notificationArea, c);
		
		c.gridy=6;
		this.add(separator, c);

		c.gridy=7;
		this.add(chatField, c);
		chatField.addActionListener(sendChatActionListener());
		
	}
	
	public static void main(String[] args) {
		JFrame window = new JFrame();
		window.setSize(230, MapLayeredPane.HEIGHT+70);
		window.add(new LeftArea(null));
		window.setVisible(true);
	}
	
	private ActionListener sendChatActionListener() {
        ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	clientModel.sendChat(chatField.getText());
            	chatField.setText(null);
            }
        };    
        return action;
    }	
	
}
