package alien.client.gui.hexagon;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Polygon;

import alien.client.gui.panels.Configuration;
import alien.client.gui.panels.CustomColor;
import alien.server.sector.Position;

public abstract class Hexagon extends Polygon{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 799300854567920957L;
	private double centerX;
	private double centerY;
	private final Position center;
	public final static double RADIUS=Configuration.HEXAGON_RADIUS;
	public final static double BORDER_RADIUS_RATIO = 0.08; //consigliato 0.08;0.1
	public final static String FONT = "TitilliumTitle20";
	public final static double FONTSIZE_RADIUS_RATIO= 0.55;
	public final static double BORDER_GAP = 10;
	
	/**
	 * Crea Esagono (Polygon) data una posizione
	 * Calcola la posizione del centro (pixel) a partire dalle coordinate e aggiunge tutti i punti al vettore del Polygon
	 * @param position Posizione del settore
	 */
	
	public Hexagon(Position position) {
		this.center=position;
		double positionX = (double)position.getColumn();
		double positionY = (double)position.getRow();
		centerX=Math.round((positionX*3/2-0.5)*RADIUS)+Configuration.MAP_BORDER_X;
		centerY=Math.round((positionY-positionX%2/2)*RADIUS*Math.sqrt(3))+Configuration.MAP_BORDER_Y;	
		double degree;
		for(degree=0; degree<360; degree+=60) {
			double degreeInRadians=Math.toRadians(degree);
			
			double pointX = centerX+RADIUS*Math.cos(degreeInRadians);
			double pointY = centerY+RADIUS*Math.sin(degreeInRadians);
			addPoint((int)Math.round(pointX), (int)Math.round(pointY));
		}
	}
	
	/**
	 * Costruisce esagono data la posizione sullo schermo e il raggio
	 * @param x
	 * @param y
	 * @param radius
	 */
	public Hexagon(int x, int y, double radius) {
		center=null;
		int degree;
		for(degree=0; degree<360; degree+=60) {
			double degreeInRadians=Math.toRadians(degree);
			double pointX = (double)x+radius*Math.cos(degreeInRadians);
			double pointY = (double)y+radius*Math.sin(degreeInRadians);
			addPoint((int)pointX, (int)pointY);
		}
	}
	public static int getBorderSize() {
		return (int) (RADIUS*BORDER_RADIUS_RATIO);
	}
	
	public int getCenterX() {
		return (int) centerX;
	}
	public int getCenterY(){
		return (int) centerY;
	}
	
	public String getStringPosition(){
		return center.toString();
	}
	public Position getCenter() {
		return center;
	}

	public int[] getVectorX() {
		return xpoints;
	}
	public int[] getVectorY() {
		return ypoints;
	}
	public abstract void draw(Graphics2D g2d);
	/**
	 * Disegna l'esagono con sfondo e bordo di colore dato dai parametri
	 * @param g2d
	 * @param borderColor Colore del bordo
	 * @param fillColor Colore di riempimento
	 */
	public void simplyDraw(Graphics2D g2d, Color borderColor, Color fillColor) {
		g2d.setColor(fillColor);
		g2d.fill(this);
		g2d.setStroke(new BasicStroke(getBorderSize()));
		g2d.setColor(borderColor);
		g2d.draw(this);
		
		
	}
	/**
	 * Stampa il nome dei settori sugli esagoni
	 * @param g2d
	 */
	public void drawString(Graphics2D g2d) {
		String stringPosition = getStringPosition();
		Font font = new Font(FONT, Font.PLAIN, getFontSize());
		FontMetrics fm = g2d.getFontMetrics(font);
		g2d.setFont(font);
		g2d.setColor(CustomColor.BLACK);
		int centerXForString = (int) (getCenterX()-fm.stringWidth(stringPosition)*0.47);
		int centerYForString = (int) (getCenterY()+fm.getHeight()*0.38);
		g2d.drawString(stringPosition, centerXForString, centerYForString);
	}
	public int getFontSize() {
		return (int) (RADIUS*FONTSIZE_RADIUS_RATIO);
	} 
	
}
