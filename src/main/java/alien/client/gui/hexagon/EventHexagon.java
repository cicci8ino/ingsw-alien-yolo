package alien.client.gui.hexagon;

import java.awt.Graphics2D;

import alien.client.gui.panels.CustomColor;
import alien.server.sector.Position;

public class EventHexagon extends Hexagon {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1087542529179348713L;

	public EventHexagon(Position position) {
		super(position);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void draw(Graphics2D g2d) {
		simplyDraw(g2d, CustomColor.BORDER, CustomColor.EVENT_SECTOR);
	}

	
}
