package alien.client.gui.hexagon;

import alien.server.sector.Position;

public class AlienHexagon extends SpecialHexagon{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3663211203878507986L;

	public AlienHexagon(Position center) {
		super(center, ALIEN_CHARACTER);
	}
	
}
