package alien.client.gui.hexagon;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

import alien.client.gui.panels.CustomColor;
import alien.server.sector.Position;

public abstract class SpecialHexagon extends Hexagon{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5332619542567951190L;
	final static double SPECIAL_HEXAGON_LINE_RATIO = 0.15;
	final static double SPECIAL_HEXAGON_RADIUS_RATIO =0.5;
	final static double SPECIAL_FONT_RADIUS_RATIO = 0.9;
	final static char HUMAN_CHARACTER = 'H';
	final static char ALIEN_CHARACTER = 'A';
	final static String SPECIAL_HEXAGON_FONT = "TitilliumMaps29L";
	
	char characterToPrint;
	
	public SpecialHexagon(Position position, char character) {
		super(position);
		this.characterToPrint=character;
	}

	@Override
	public void draw(Graphics2D g2d) {
		super.simplyDraw(g2d, CustomColor.BORDER, Color.BLACK);
		g2d.setPaint(Color.WHITE);
		drawCharacter(g2d, characterToPrint);
	}
	
	public void drawCharacter(Graphics2D g2d, char character) {
		Font font = new Font(SPECIAL_HEXAGON_FONT, Font.BOLD, (int) (RADIUS*SPECIAL_FONT_RADIUS_RATIO));
		g2d.setFont(font);
		String number = String.valueOf(character);
		FontMetrics fm = g2d.getFontMetrics(font);
		int centerXForString = (int) (getCenterX()-fm.stringWidth(number)*0.45);
		int centerYForString = (int) (getCenterY()+fm.getHeight()*0.38);
		g2d.drawString(number, centerXForString, centerYForString);
	}
}
