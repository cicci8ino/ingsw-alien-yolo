package alien.client.gui.hexagon;

import alien.server.sector.Position;

public class HumanHexagon extends SpecialHexagon {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7018409372573630862L;

	public HumanHexagon(Position center) {
		super(center, HUMAN_CHARACTER);
	}

}
