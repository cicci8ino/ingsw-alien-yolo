package alien.client.gui.hexagon;

import java.awt.Graphics2D;

import alien.client.gui.panels.CustomColor;
import alien.server.sector.Position;

public class SafeHexagon extends Hexagon{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7966511960814595413L;

	public SafeHexagon(Position center) {
		super(center);
	}

	@Override
	public void draw(Graphics2D g2d) {
		super.simplyDraw(g2d, CustomColor.BORDER, CustomColor.WHITE);	
		drawString(g2d);
	}

}
