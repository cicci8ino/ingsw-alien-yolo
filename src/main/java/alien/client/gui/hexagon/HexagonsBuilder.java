package alien.client.gui.hexagon;

import java.util.ArrayList;
import java.util.List;

import alien.server.sector.Position;


public abstract class HexagonsBuilder {

	/**
	 * Ritorna una lista di esagoni a partire da una mappa testuale
	 * @param textMap Mappa in formato testuale
	 * @return Lista di esagoni
	 */
	
	public static List<Hexagon> getGuiSectorFromTextMap(String textMap) {
		
		List<Hexagon> hexagons = new ArrayList<Hexagon>();
		int row=1, col=1;
		for (Character c:textMap.toCharArray()) {
			Hexagon hexagonToSave;
			Position positionToSave = new Position(row, col);
			switch (c) {
			case 'D':
				hexagonToSave=new DangerousHexagon(positionToSave);
				hexagons.add(hexagonToSave);
				break;
			case 'S':
				hexagonToSave=new SafeHexagon(positionToSave);
				hexagons.add(hexagonToSave);
				break;
			case '\n':
				row++;
				col=0;
				break;
			case 'A':
				hexagonToSave=new AlienHexagon(positionToSave);
				hexagons.add(hexagonToSave);
				break;
			case 'H':
				hexagonToSave=new HumanHexagon(positionToSave);
				hexagons.add(hexagonToSave);
				break;
			default:
				break;
			}
			col++;
			if (Character.isDigit(c)) {
				hexagonToSave=new EscapeHexagon(positionToSave, c);
				hexagons.add(hexagonToSave);
			}
	}
		return hexagons;
	
}
}