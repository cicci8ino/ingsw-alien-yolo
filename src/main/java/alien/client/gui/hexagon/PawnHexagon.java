package alien.client.gui.hexagon;

import java.awt.Graphics2D;

import alien.client.gui.panels.CustomColor;
import alien.server.sector.Position;

public class PawnHexagon extends Hexagon {


	/**
	 * 
	 */
	private static final long serialVersionUID = 413192558392939937L;

	public PawnHexagon(Position position) {
		super(position);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void draw(Graphics2D g2d) {
		super.simplyDraw(g2d, CustomColor.BORDER, CustomColor.PAWN_SECTOR);
		
	}

}
