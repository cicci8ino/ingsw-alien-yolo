package alien.client.gui.hexagon;

import java.awt.Graphics2D;
import java.awt.geom.Area;
import alien.client.gui.panels.CustomColor;
import alien.server.sector.Position;

public class DangerousHexagon extends Hexagon {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1817601167118474530L;

	public DangerousHexagon(Position center) {
		super(center);
	}
	public DangerousHexagon(int i, int j, double d) {
		super(i, j, d);
	}
	final static double LITTLE_HEXAGON_RADIUS_RATIO=0.3;
	
	@Override
	public void draw(Graphics2D g2d) {
		super.simplyDraw(g2d, CustomColor.BORDER, CustomColor.DANGEROUS_SECTOR);
		drawMiniHexagons(g2d);
		drawString(g2d);
	}
	public void drawMiniHexagons(Graphics2D g2d) {
		Area hexagonAndMiniHexagonsArea = new Area();
		int pointsX[] = this.getVectorX();
		int pointsY[] = this.getVectorY();
		for (int numberOfMiniHexagons=0; numberOfMiniHexagons<6; numberOfMiniHexagons++) {
			Hexagon miniHexagon = new DangerousHexagon(pointsX[numberOfMiniHexagons],pointsY[numberOfMiniHexagons], RADIUS*LITTLE_HEXAGON_RADIUS_RATIO);
			Area hexagonArea = new Area(this);
			Area miniHexagonArea = new Area(miniHexagon);
			hexagonArea.intersect(miniHexagonArea);
			hexagonAndMiniHexagonsArea.add(hexagonArea);
		}
		g2d.setColor(CustomColor.BORDER);
		g2d.fill(hexagonAndMiniHexagonsArea);
	}

}
