package alien.client.gui.hexagon;

import java.awt.Graphics2D;

import alien.client.gui.panels.CustomColor;
import alien.server.sector.Position;

public class UsableHexagon extends Hexagon {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8825139120847059034L;

	public UsableHexagon(Position position) {
		super(position);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void draw(Graphics2D g2d) {
		simplyDraw(g2d, CustomColor.BORDER, CustomColor.USABLE_SECTOR);
	}

	
	
}
