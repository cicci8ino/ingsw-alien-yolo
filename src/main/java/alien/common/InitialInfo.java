package alien.common;

import java.io.Serializable;

public class InitialInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1000563148204812264L;
	private final String textMap;
	private final String characterName;
	private final String welcomeMessage; 
	private final String pawnType;
	private final RemoteStatus initialStatus;
	/**
	 * Crea notifica da inviare da inviare come prima notifica della partita
	 * @param textMap Mappa in formato testuale
	 * @param pawnType Il nome del tipo di pedina
	 * @param characterName Il nome del personaggio
	 * @param welcomeMessage Un messaggio di benvenuto
	 * @param initialStatus Lo stato iniziale
	 */
	public InitialInfo(String textMap,String pawnType, String characterName, String welcomeMessage, RemoteStatus initialStatus) {
		this.textMap=textMap;
		this.characterName=characterName;
		this.welcomeMessage=welcomeMessage;
		this.pawnType=pawnType;
		this.initialStatus=initialStatus;
	}

	public String getTextMap() {
		return textMap;
	}

	public String getCharacterName() {
		return characterName;
	}
	
	public String getWelcomeMessage() {
		return welcomeMessage;
	}
	
	public String getPawnType()
	{
		return pawnType;
	}
	public RemoteStatus getInitialStatus()
	{
		return initialStatus;
	}
}
