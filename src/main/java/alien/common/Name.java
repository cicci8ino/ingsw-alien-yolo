package alien.common;

import java.io.Serializable;

public class Name implements Serializable{

	private static final long serialVersionUID = 4789443396083933894L;
	private String name;
	
	public Name(String name) {
		this.name=name;
	}
	public String getName() {
		return name;
	}
}
