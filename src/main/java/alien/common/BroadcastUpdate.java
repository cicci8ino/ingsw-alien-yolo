package alien.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import alien.server.sector.Position;

public class BroadcastUpdate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5123242795649902877L;
	private final PlayerLists playersInGame; 
	private Boolean matchOver=false;
	private final ArrayList<Position> eventPositions;
	private final String playingPlayer;
	private final ArrayList<String> broadcastMessages; 
	
	/**
	 * Costruisci BroadcastUpdates
	 * Tutti i parametri vengono inizializzati tramite il costruttore
	 * Solo per i messaggi c'è bisogno di usare un metodo a parte:
	 * addBroadcastMessage(message)
	 * @param playersInGame
	 * @param eventPositions
	 * @param playingPlayer
	 */
	
	public BroadcastUpdate(PlayerLists playersInGame,
			ArrayList<Position> eventPositions, String playingPlayer) {
		this.playersInGame = playersInGame;
		this.eventPositions = eventPositions;
		this.playingPlayer = playingPlayer;
		this.broadcastMessages = new ArrayList<String>();

	}

	
	
	private BroadcastUpdate(PlayerLists playersInGame,
			ArrayList<Position> eventPositions, String playingPlayer,
			ArrayList<String> broadcastMessages) {
		this.playersInGame = playersInGame;
		this.eventPositions = eventPositions;
		this.playingPlayer = playingPlayer;
		this.broadcastMessages = broadcastMessages;

	}


	public PlayerLists getPlayersInGame() {
		return playersInGame;
	}

	public List<Position> getEventPositions() {
		return eventPositions;
	}

	public String getPlayingPlayer() {
		return playingPlayer;
	}

	public void addBroadcastMessage(String message) {
		broadcastMessages.add(message);
	}
	
	public List<String> getBroadcastMessages() {
		return broadcastMessages;
	}
	public void setMatchOver(Boolean matchOver)
	{
		this.matchOver=matchOver;
	}
	public Boolean getMatchOver()
	{
		return matchOver;
	}
	@SuppressWarnings("unchecked")
	@Override
	public BroadcastUpdate clone() {
		PlayerLists clonedPlayerLists = playersInGame.clone();
		ArrayList<Position> clonedEventPositions = (ArrayList<Position>) eventPositions.clone();
		ArrayList<String> clonedBroadcastMessages = (ArrayList<String>) broadcastMessages.clone();
		BroadcastUpdate update =  new BroadcastUpdate(clonedPlayerLists, clonedEventPositions, playingPlayer, clonedBroadcastMessages);
		update.matchOver=this.matchOver;
		return	update;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((broadcastMessages == null) ? 0 : broadcastMessages
						.hashCode());
		result = prime * result
				+ ((eventPositions == null) ? 0 : eventPositions.hashCode());
		result = prime * result
				+ ((matchOver == null) ? 0 : matchOver.hashCode());
		result = prime * result
				+ ((playersInGame == null) ? 0 : playersInGame.hashCode());
		result = prime * result
				+ ((playingPlayer == null) ? 0 : playingPlayer.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BroadcastUpdate other = (BroadcastUpdate) obj;
		if (broadcastMessages == null) {
			if (other.broadcastMessages != null)
				return false;
		} else if (!broadcastMessages.equals(other.broadcastMessages))
			return false;
		if (eventPositions == null) {
			if (other.eventPositions != null)
				return false;
		} else if (!eventPositions.equals(other.eventPositions))
			return false;
		if (matchOver == null) {
			if (other.matchOver != null)
				return false;
		} else if (!matchOver.equals(other.matchOver))
			return false;
		if (playersInGame == null) {
			if (other.playersInGame != null)
				return false;
		} else if (!playersInGame.equals(other.playersInGame))
			return false;
		if (playingPlayer == null) {
			if (other.playingPlayer != null)
				return false;
		} else if (!playingPlayer.equals(other.playingPlayer))
			return false;
		return true;
	}
	
	
}
