package alien.common;

import java.io.Serializable;
import java.util.HashMap;

public class MatchUpdate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3185065022100724047L;
	private final BroadcastUpdate broadcastUpdate;
	private final HashMap<String,UnicastUpdate> unicastUpdates;
	private UnicastUpdate privateUnicastUpdate;
	private boolean containsUnicastUpdates;
	
	/**
	 * Costruttore per generare un MatchUpdate con solo Broadcast
	 * Se si ha bisogno anche di Unicast aggiungerlo con il metodo addUnicastUpdateForPlayer
	 * @param broadcastUpdate
	 */
	
	public MatchUpdate(BroadcastUpdate broadcastUpdate) {
		this.unicastUpdates = new HashMap<String, UnicastUpdate>();
		this.broadcastUpdate = broadcastUpdate;
		this.containsUnicastUpdates = false;
	}
	
	/**Aggiungi un unicast update alla notifica
	 * @param name Nome giocatore
	 * @param playerUpdate UnicastUpdate associato al nome
	 */
	
	public void addUnicastUpdateForPlayer(String name, UnicastUpdate playerUpdate) {
		unicastUpdates.put(name, playerUpdate);
		containsUnicastUpdates=true;
	}
	
	public BroadcastUpdate getBroadcastUpdate() {
		return broadcastUpdate;
	}

	/**
	 * MatchUpdate contiene UnicastUpdate o è solo formato
	 * da una parte di Broadcast?
	 * @return
	 */
	
	public boolean containsUnicastUpdates() {
		return containsUnicastUpdates;
	}
	
	/**
	 * Costrusci un MatchStatus personalizzato per un determinato player
	 * Rimuove tutti i playerinfo che non sono di sua competenza
	 * @param name
	 * @return
	 * @throws CloneNotSupportedException
	 */
	
	public MatchUpdate prepareUpdateForPlayer(String name) {
		MatchUpdate matchUpdateCustom = this.clone();
		if (!matchUpdateCustom.containsUnicastUpdates()) { //IN caso non contenga niente stiamo apposto
			return matchUpdateCustom;
		}
		matchUpdateCustom.containsUnicastUpdates=false;
		if (matchUpdateCustom.unicastUpdates.containsKey(name)) { //se lo contiene lo salvo nella variabile
			matchUpdateCustom.containsUnicastUpdates=true;
			matchUpdateCustom.privateUnicastUpdate=matchUpdateCustom.unicastUpdates.get(name).clone();
		}
		for (String aName: matchUpdateCustom.unicastUpdates.keySet()) { //rimuovo tutte le cose
			UnicastUpdate playerInfoToRemove = matchUpdateCustom.unicastUpdates.get(aName);
			matchUpdateCustom.unicastUpdates.remove(playerInfoToRemove);
		}
		return matchUpdateCustom;
	}
	
	/**
	 * Ritorna UnicastUpdate, in seguito a prepareUpdateForPlayer
	 * @return
	 */
	
	public UnicastUpdate getUnicastUpdate() {
		return privateUnicastUpdate;
	}

	private MatchUpdate(BroadcastUpdate broadcastUpdate,
			HashMap<String, UnicastUpdate> unicastUpdates,
			boolean containsUnicastUpdates, UnicastUpdate privateUnicastUpdate) {
		this.broadcastUpdate = broadcastUpdate;
		this.unicastUpdates = unicastUpdates;
		this.containsUnicastUpdates = containsUnicastUpdates;
		this.privateUnicastUpdate=privateUnicastUpdate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public MatchUpdate clone() {
		return new MatchUpdate(broadcastUpdate.clone(), (HashMap<String, UnicastUpdate>) unicastUpdates.clone(), containsUnicastUpdates, privateUnicastUpdate);
	}
	
}
