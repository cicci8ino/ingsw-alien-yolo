package alien.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alien.server.sector.Position;

public class UnicastUpdate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5939959565084219046L;
	private final ArrayList<Position> usablePositions;
	private final HashMap<RemoteItemCard, Boolean> cardsInHand;
	private final RemoteStatus playerStatus;
	private final ArrayList<String> unicastMessages;
	private final Position actualPosition;
	private Integer turn=new Integer(0);
	/**
	 * Update unicast, personalizzati per i singoli giocatori
	 * Contengono informazioni private
	 * Da creare ad inizio turno e poi da modificare durante il turno.
	 * @param cardsInHand Carte in mano al giocatore.
	 * @param playerStatus Stato del giocatore
	 * @param unicastMessage Messaggi privati rivolti al giocatore in questione
	 * @param actualPosition Posizione corrente
	 */

	public UnicastUpdate(RemoteStatus playerStatus,
			Position actualPosition) {
		this.usablePositions = new ArrayList<Position>();
		this.cardsInHand = new HashMap<RemoteItemCard, Boolean>();
		this.playerStatus = playerStatus;
		this.unicastMessages = new ArrayList<String>();
		this.actualPosition = actualPosition;
	}

	private UnicastUpdate(ArrayList<Position> usablePositions,
			HashMap<RemoteItemCard, Boolean> cardsInHand,
			RemoteStatus playerStatus, ArrayList<String> unicastMessages,
			Position actualPosition) {
		super();
		this.usablePositions = usablePositions;
		this.cardsInHand = cardsInHand;
		this.playerStatus = playerStatus;
		this.unicastMessages = unicastMessages;
		this.actualPosition = actualPosition;
	}

	public void addUnicastMessage(String message) {
		unicastMessages.add(message);
	}

	public List<Position> getUsablePositions() {
		return usablePositions;
	}

	public Map<RemoteItemCard, Boolean> getCardsInHand() {
		return cardsInHand;
	}

	public RemoteStatus getPlayerStatus() {
		return playerStatus;
	}

	public ArrayList<String> getUnicastMessages() {
		return unicastMessages;
	}

	public Position getActualPosition() {
		return actualPosition;
	}
	public Integer getTurn()
	{
		return turn;
	}
	
	/**
	 * Imposta la determinata carta utilizzabile
	 * @param card Carta da settare utilizzabile
	 */
	public void setCardUsable(RemoteItemCard card) {
		addCard(card, true);
	}
	public void setTurn(Integer turn)
	{
		this.turn=turn;
	}
	public void setCardUnusable(RemoteItemCard card) {
		addCard(card, false);
	}
	public void addCard(RemoteItemCard card, Boolean usable) {
		cardsInHand.put(card, usable);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public UnicastUpdate clone() {
		ArrayList<Position> clonedUsablePositions = (ArrayList<Position>) usablePositions.clone();
		HashMap<RemoteItemCard, Boolean> clonedCardsInHand = (HashMap<RemoteItemCard, Boolean>) cardsInHand.clone();
		RemoteStatus clonedPlayerStatus = playerStatus.clone();
		ArrayList<String> clonedUnicastMessages = (ArrayList<String>) unicastMessages.clone();
		Position clonedActualPosition = actualPosition.clone();
		UnicastUpdate update= new UnicastUpdate(clonedUsablePositions, clonedCardsInHand, clonedPlayerStatus, clonedUnicastMessages, clonedActualPosition);
		update.turn=turn;
		return update;
	
	}
	/**
	 * Aggiunge posizioni alla lista di posizioni utilizzabili.
	 * Il client utilizzerà poi le posizioni per creare correttamente l'azione consentita
	 * @param positions Posizioni che il client può selezionare
	 */
	
	public void addUsablePositions(List<Position> positions) {
		usablePositions.addAll(positions);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((actualPosition == null) ? 0 : actualPosition.hashCode());
		result = prime * result
				+ ((cardsInHand == null) ? 0 : cardsInHand.hashCode());
		result = prime * result
				+ ((playerStatus == null) ? 0 : playerStatus.hashCode());
		result = prime * result + ((turn == null) ? 0 : turn.hashCode());
		result = prime * result
				+ ((unicastMessages == null) ? 0 : unicastMessages.hashCode());
		result = prime * result
				+ ((usablePositions == null) ? 0 : usablePositions.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnicastUpdate other = (UnicastUpdate) obj;
		if (actualPosition == null) {
			if (other.actualPosition != null)
				return false;
		} else if (!actualPosition.equals(other.actualPosition))
			return false;
		if (cardsInHand == null) {
			if (other.cardsInHand != null)
				return false;
		} else if (!cardsInHand.equals(other.cardsInHand))
			return false;
		if (playerStatus == null) {
			if (other.playerStatus != null)
				return false;
		} else if (!playerStatus.equals(other.playerStatus))
			return false;
		if (turn == null) {
			if (other.turn != null)
				return false;
		} else if (!turn.equals(other.turn))
			return false;
		if (unicastMessages == null) {
			if (other.unicastMessages != null)
				return false;
		} else if (!unicastMessages.equals(other.unicastMessages))
			return false;
		if (usablePositions == null) {
			if (other.usablePositions != null)
				return false;
		} else if (!usablePositions.equals(other.usablePositions))
			return false;
		return true;
	}
	
	
	
}
