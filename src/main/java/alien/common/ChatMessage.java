package alien.common;

import java.io.Serializable;
import java.util.Calendar;

public class ChatMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8211200829894711417L;
	private final String message;
	private String hour;
	private String minutes;
	private final String sender;
	
	/**
	 * Create a chat message using current date
	 * @param message message
	 * @param sender sender name
	 */
	
	public ChatMessage(String message, String sender) {
		Calendar now = Calendar.getInstance();
		this.message=message;
		this.sender=sender;
		int hourInt=now.get(Calendar.HOUR_OF_DAY);
		int minutesInt=now.get(Calendar.MINUTE);
		if (hourInt<10)
			this.hour="0"+String.valueOf(hourInt);
		else
			this.hour=String.valueOf(hourInt);
		if (minutesInt<10)
			this.minutes="0"+String.valueOf(minutesInt);
		else
			this.minutes=String.valueOf(minutesInt);
		
	}
	
	public String toString() {
		String chatMessage = "["+hour+":"+minutes+"-"+sender+"] "+message;
		return chatMessage;
	}
	
	public String getMessage() {
		return message;
	}
	
}
