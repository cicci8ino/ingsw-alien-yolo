package alien.common;

import java.io.Serializable;

public class RemoteItemCard implements Serializable{

	private static final long serialVersionUID = -6864129736497969657L;
	private final String name;
	private final int indexNumber;
	/**
	 * Costruisce carta remota da inviare al client.
	 * @param name Nome della carta
	 * @param indexNumber Parametro utilizzato dal server per interpretare la carta scelta dal client
	 */
	public RemoteItemCard(String name,int indexNumber) {
		this.name=name;
		this.indexNumber=indexNumber;
	}
	public String getName() {
		return name;
	}
	public int getIndexNumber() {
		return indexNumber;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + indexNumber;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RemoteItemCard other = (RemoteItemCard) obj;
		if (indexNumber != other.indexNumber)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
	
}
