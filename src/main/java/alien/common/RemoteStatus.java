package alien.common;

import java.io.Serializable;

public class RemoteStatus implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7957039304229731491L;
	private boolean isDead = false;
	private boolean hasWon = false;
	private boolean onTurn = false;

	private boolean canAttack = false;
	private boolean canEndTurn = false;
	private boolean canSelectPosition = false;
	private boolean canSelectItemCard = false;
	private boolean canDiscardItemCard = false;

	/**
	 * Costruisce stato remoto. Settare i parametri con i setter
	 */
	
	public RemoteStatus() {
	}



	public boolean isDead() {
		return isDead;
	}

	public boolean hasWon() {
		return hasWon;
	}

	public boolean isOnTurn() {
		return onTurn;
	}
	public boolean canAttack() {
		return canAttack;
	}

	public boolean canEndTurn() {
		return canEndTurn;
	}
	public boolean canSelectPosition()
	{
		return canSelectPosition;
	}
	public boolean canSelectItemCard()
	{
		return canSelectItemCard;
	}
	public boolean canDiscardItemCard()
	{
		return canDiscardItemCard;
	}
	public void setCanAttack(boolean canAttack) {
		this.canAttack = canAttack;
	}

	public void setCanEndTurn(boolean canEndTurn) {
		this.canEndTurn = canEndTurn;
	}

	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}

	public void setHasWon(boolean hasWon) {
		this.hasWon = hasWon;
	}

	public void setOnTurn(boolean onTurn) {
		this.onTurn = onTurn;
	}
	public void setCanSelectPosition(boolean canSelectPosition) {
		this.canSelectPosition = canSelectPosition;
	}

	public void setCanSelectItemCard(boolean canSelectItemCard) {
		this.canSelectItemCard = canSelectItemCard;
	}

	public void setCanDiscardItemCard(boolean canDiscardItemCard) {
		this.canDiscardItemCard = canDiscardItemCard;
	}
	@Override
	public RemoteStatus clone() {
		RemoteStatus remoteStatus = new RemoteStatus();
		remoteStatus.isDead=isDead;
		remoteStatus.hasWon=hasWon;
		remoteStatus.onTurn=onTurn;
		
		remoteStatus.canAttack=canAttack;
		remoteStatus.canEndTurn=canEndTurn;
		remoteStatus.canSelectPosition=canSelectPosition;
		remoteStatus.canSelectItemCard=canSelectItemCard;
		remoteStatus.canDiscardItemCard=canDiscardItemCard;
		return remoteStatus;
	}



	@Override
	public String toString() {
		return "RemoteStatus [isDead=" + isDead + ", hasWon=" + hasWon
				+ ", onTurn=" + onTurn + ", canAttack=" + canAttack
				+ ", canEndTurn=" + canEndTurn + ", canSelectPosition="
				+ canSelectPosition + ", canSelectItemCard="
				+ canSelectItemCard + ", canDiscardItemCard="
				+ canDiscardItemCard + "]";
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (canAttack ? 1231 : 1237);
		result = prime * result + (canDiscardItemCard ? 1231 : 1237);
		result = prime * result + (canEndTurn ? 1231 : 1237);
		result = prime * result + (canSelectItemCard ? 1231 : 1237);
		result = prime * result + (canSelectPosition ? 1231 : 1237);
		result = prime * result + (hasWon ? 1231 : 1237);
		result = prime * result + (isDead ? 1231 : 1237);
		result = prime * result + (onTurn ? 1231 : 1237);
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RemoteStatus other = (RemoteStatus) obj;
		if (canAttack != other.canAttack)
			return false;
		if (canDiscardItemCard != other.canDiscardItemCard)
			return false;
		if (canEndTurn != other.canEndTurn)
			return false;
		if (canSelectItemCard != other.canSelectItemCard)
			return false;
		if (canSelectPosition != other.canSelectPosition)
			return false;
		if (hasWon != other.hasWon)
			return false;
		if (isDead != other.isDead)
			return false;
		if (onTurn != other.onTurn)
			return false;
		return true;
	}
	
	
	
}
