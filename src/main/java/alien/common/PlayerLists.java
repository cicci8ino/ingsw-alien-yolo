package alien.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PlayerLists implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -916956848140046854L;
	private final ArrayList<String> alivePlayers;
	private final ArrayList<String> deadPlayers;

	
	/**
	 * Crea diverse liste di giocatori da inserire nel broadcast update.
	 * Ha bisogno di un solo paramentro che è la lista dei giocatori sconosciuti.
	 * Tale PlayerLists verrà generata solo all'inizio della partita e modificata di volta in volta.
	 * @param unknownTypePlayers
	 */
	
	public PlayerLists(ArrayList<String> unknownTypePlayers) {
		this.alivePlayers = unknownTypePlayers;
		this.deadPlayers = new ArrayList<String>();
	}
	
	

	private PlayerLists(ArrayList<String> unknownTypePlayers,
			ArrayList<String> deadPlayers) {
		this.alivePlayers = unknownTypePlayers;
		this.deadPlayers = deadPlayers;
	
	}



	public List<String> getUnknownTypePlayers() {
		return alivePlayers;
	}

	public List<String> getDeadPlayers() {
		return deadPlayers;
	}
	
	/**
	 * Aggiunge il nome alla lista dei morti.
	 * @param deadName
	 */
	
	public void addToDeadPlayers(String deadName) {
		deadPlayers.add(deadName);
		alivePlayers.remove(deadName);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public PlayerLists clone() {
		ArrayList<String> clonedUnknownTypePlayers = (ArrayList<String>) this.alivePlayers.clone();
		ArrayList<String> clonedDeadPlayers = (ArrayList<String>) this.deadPlayers.clone(); 
		return new PlayerLists(clonedUnknownTypePlayers, clonedDeadPlayers);
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((alivePlayers == null) ? 0 : alivePlayers.hashCode());
		result = prime * result
				+ ((deadPlayers == null) ? 0 : deadPlayers.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerLists other = (PlayerLists) obj;
		if (alivePlayers == null) {
			if (other.alivePlayers != null)
				return false;
		} else if (!alivePlayers.equals(other.alivePlayers))
			return false;
		if (deadPlayers == null) {
			if (other.deadPlayers != null)
				return false;
		} else if (!deadPlayers.equals(other.deadPlayers))
			return false;
		return true;
	}
	
	
	
}
