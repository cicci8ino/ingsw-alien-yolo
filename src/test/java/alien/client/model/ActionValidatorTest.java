package alien.client.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import alien.common.RemoteItemCard;
import alien.common.RemoteStatus;
import alien.common.UnicastUpdate;
import alien.server.action.*;
import alien.server.sector.Position;

public class ActionValidatorTest {

	List<Position> positions = new ArrayList<Position>();
	RemoteStatus status = new RemoteStatus();
	UnicastUpdate unicastUpdate = new UnicastUpdate(status, new Position(3,3));
	ActionValidator test = new ActionValidator(unicastUpdate);

	@Test
	public void testIsActionValidAttack() {
		status.setCanAttack(true);
		assertTrue(test.isActionValid(new Attack()));
		status.setCanAttack(false);
		assertFalse(test.isActionValid(new Attack()));
	}

	@Test
	public void testIsActionValidEndTurn() {
		status.setCanEndTurn(true);
		assertTrue(test.isActionValid(new EndTurn()));
		status.setCanEndTurn(false);
		assertFalse(test.isActionValid(new EndTurn()));
	}

	@Test
	public void testIsActionValidItemCardDiscarded() {
		RemoteItemCard cardOne = new RemoteItemCard("Carta", 2);
		RemoteItemCard cardTwo = new RemoteItemCard("Carta", 5);
		unicastUpdate.addCard(cardOne, true);
		unicastUpdate.addCard(cardTwo, false);
		status.setCanDiscardItemCard(true);
		assertTrue("Seleziona carta esistente", test.isActionValid(new ItemCardDiscarded(2)));
		assertTrue("Seleziona carta non selezionabile. Corretto, per il discarding non si fa questo controllo", test.isActionValid(new ItemCardDiscarded(5)));
		assertFalse("Seleziona carta non esistente", test.isActionValid(new ItemCardDiscarded(7)));
	}

	@Test
	public void testIsActionValidItemCardSelected() {
		RemoteItemCard cardOne = new RemoteItemCard("Carta", 2);
		RemoteItemCard cardTwo = new RemoteItemCard("Carta", 5);
		unicastUpdate.addCard(cardOne, true);
		unicastUpdate.addCard(cardTwo, false);
		status.setCanSelectItemCard(true);
		assertTrue("Seleziona carta esistente", test.isActionValid(new ItemCardSelected(2)));
		assertFalse("Seleziona carta non utilizzabile", test.isActionValid(new ItemCardSelected(5)));
		assertFalse("Seleziona carta non esistente", test.isActionValid(new ItemCardSelected(7)));
	}

	@Test
	public void testIsActionValidPositionSelected() {
		
		positions.add(new Position(1,1));
		positions.add(new Position(1,2));
		positions.add(new Position(1,3));
		positions.add(new Position(1,4));
		unicastUpdate.addUsablePositions(positions);
		status.setCanSelectPosition(true);
		for (Position aPosition:positions)
			assertTrue("Seleziona posizione nella lista", test.isActionValid(new PositionSelected(aPosition)));
		assertFalse("Seleziona posizione non nella lista", test.isActionValid(new PositionSelected(new Position(2,8))));
		status.setCanSelectPosition(false);
		assertFalse("Seleziona posizione nella lista, ma select position false",test.isActionValid(new PositionSelected(new Position(2,10))));
		
	}

}
