package alien.common;

import static org.junit.Assert.*;

import org.junit.Test;

public class NameTest {
	
	String name = "Calimero";
	Name testName = new Name(name);


	@Test
	public void testGetName() {
		assertEquals(testName.getName(), name);
	}

}
