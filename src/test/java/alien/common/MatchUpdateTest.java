package alien.common;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import alien.server.sector.Position;

public class MatchUpdateTest {

	ArrayList<String> players;
	ArrayList<Position> usablePositions;
	ArrayList<Position> eventPositions;
	PlayerLists playerLists;
	BroadcastUpdate broadcast;
	MatchUpdate match;
	RemoteStatus statusForGiorgio;
	RemoteStatus statusForAndrea;
	RemoteStatus statusForMario;
	UnicastUpdate forGiorgio;
	UnicastUpdate forAndrea;
	UnicastUpdate forMario;
	List<String> message;
	Position currentPosition;
	int roundNum =5;

	@Before
	public void init() {
		prepareUnicast();
		prepareBroadcast();
		prepareMatch();
		
	}
	
	public void prepareMatch() {
		match = new MatchUpdate(broadcast);
		match.addUnicastUpdateForPlayer("Giorgio", forGiorgio);
		match.addUnicastUpdateForPlayer("Andrea", forAndrea);
		match.addUnicastUpdateForPlayer("Mario", forMario);
	}
	
	public void prepareUnicast() {
		currentPosition = new Position(3,5);
		usablePositions = new ArrayList<Position>();
		usablePositions.add(new Position(3,10));
		statusForGiorgio = new RemoteStatus();
		statusForAndrea = new RemoteStatus();
		statusForMario = new RemoteStatus();
		forMario = new UnicastUpdate(statusForMario,new Position(6,3));
		forAndrea = new UnicastUpdate(statusForAndrea,new Position(3,5));
		forGiorgio = new UnicastUpdate(statusForGiorgio, currentPosition);
		forGiorgio.addUsablePositions(usablePositions);
		forGiorgio.setTurn(roundNum);
		List<String> messages = new ArrayList<String>();
		messages.add("Ciao");
		messages.add("Ciaone");
		messages.add("Noise");
		for (String message:messages)
			forGiorgio.addUnicastMessage(message);
		assertEquals(messages.size(), forGiorgio.getUnicastMessages().size());
		for (String message:forGiorgio.getUnicastMessages())
			messages.remove(message);
		assertTrue(messages.isEmpty());
		
	}
	
	public void prepareBroadcast() {
		eventPositions=new ArrayList<Position>();
		eventPositions.add(new Position(2,3));
		eventPositions.add(new Position(2,5));
		eventPositions.add(new Position(2,6));
		players = new ArrayList<String>();
		playerLists = new PlayerLists(players);
		broadcast = new BroadcastUpdate(playerLists, eventPositions, "Giorgio");
	}

	@Test
	public void testAddUnicastUpdateForPlayer() {
		
		
		MatchUpdate matchForGiorgio = match.prepareUpdateForPlayer("Giorgio");
		MatchUpdate matchForAndrea = match.prepareUpdateForPlayer("Andrea");
		assertTrue(matchForGiorgio.containsUnicastUpdates());
		assertEquals(matchForGiorgio.getUnicastUpdate(), forGiorgio);
		assertEquals(matchForAndrea.getUnicastUpdate(), forAndrea);
		assertFalse(matchForAndrea.getUnicastUpdate().equals(forMario));
		assertTrue(matchForGiorgio.getBroadcastUpdate().equals(broadcast));
	}

	@Test
	public void testGetBroadcastUpdate() {
		assertTrue(match.getBroadcastUpdate().equals(broadcast));
	}

	@Test
	public void testContainsUnicastUpdates() {
		assertTrue(match.containsUnicastUpdates());
		MatchUpdate match2 = new MatchUpdate(broadcast);
		assertFalse(match2.containsUnicastUpdates());
		
	}
	
	@Test
	public void equalsTest() {
		assertTrue(broadcast.equals(broadcast.clone()));
		assertTrue(forGiorgio.equals(forGiorgio.clone()));
	}
	

	@Test
	public void testUnicast() {
		RemoteItemCard card1 = new RemoteItemCard("Card 1", 2);
		RemoteItemCard card2 = new RemoteItemCard("Card 2", 3);
		forGiorgio.addCard(card1, true);
		forGiorgio.addCard(card2, false);
		assertTrue(forGiorgio.getCardsInHand().get(card1));
		assertFalse(forGiorgio.getCardsInHand().get(card2));
		forGiorgio.setCardUsable(card2);
		assertTrue(forGiorgio.getCardsInHand().get(card2));
		
		assertEquals(forGiorgio.getUsablePositions(), usablePositions);
		
		assertEquals(forGiorgio.getActualPosition(), currentPosition);
		
		
		assertEquals(forGiorgio.getTurn(), new Integer(roundNum));
	}
	
	@Test
	public void testBroadcast() {
		assertEquals(match.getBroadcastUpdate().getPlayersInGame(), playerLists);
		assertEquals(broadcast.getEventPositions(), eventPositions);
		assertEquals(broadcast.getPlayingPlayer(), "Giorgio");
		assertFalse(broadcast.getMatchOver());
		broadcast.setMatchOver(true);
		assertTrue(broadcast.getMatchOver());
	}

}
