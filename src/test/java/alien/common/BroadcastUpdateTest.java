package alien.common;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import alien.server.sector.Position;

public class BroadcastUpdateTest {

	ArrayList<Position> eventPositions;
	PlayerLists playerLists;
	BroadcastUpdate broadcast;
	ArrayList<String> players;
	
	@Before
	public void init() {
		eventPositions=new ArrayList<Position>();
		eventPositions.add(new Position(2,3));
		eventPositions.add(new Position(2,5));
		eventPositions.add(new Position(2,6));
		players = new ArrayList<String>();
		playerLists = new PlayerLists(players);
		broadcast = new BroadcastUpdate(playerLists, eventPositions, "Giorgio");
	}

	@Test
	public void testGetPlayersInGame() {
		assertEquals(broadcast.getPlayersInGame(), playerLists);
	}

	@Test
	public void testGetEventPositions() {
		assertEquals(broadcast.getEventPositions(), eventPositions);
	}

	@Test
	public void testGetPlayingPlayer() {
		assertEquals(broadcast.getPlayingPlayer(), "Giorgio");
	}

	@Test
	public void testGetBroadcastMessages() {
		List<String> messages = new ArrayList<String>();
		messages.add("Ciao");
		messages.add("Ciaone");
		messages.add("Noise");
		for (String message:messages)
			broadcast.addBroadcastMessage(message);
		assertEquals(messages.size(), broadcast.getBroadcastMessages().size());
		for (String message:broadcast.getBroadcastMessages())
			messages.remove(message);
		assertTrue(messages.isEmpty());
	}

	@Test
	public void testSetMatchOver() {
		broadcast.setMatchOver(true);
		assertTrue(broadcast.getMatchOver());
		broadcast.setMatchOver(false);
		assertFalse(broadcast.getMatchOver());
	}


	@Test
	public void testClone() {
		assertEquals(broadcast, broadcast.clone());
	}


}
