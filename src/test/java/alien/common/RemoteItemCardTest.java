package alien.common;

import static org.junit.Assert.*;

import org.junit.Test;

public class RemoteItemCardTest {

	String cardName = "Carta Canta";
	int cardIndex = 2;
	RemoteItemCard testItemCard = new RemoteItemCard(cardName,cardIndex);

	@Test
	public void testRemoteItemCard() {
		testGetName();
		testGetIndexNumber();
	}

	@Test
	public void testGetName() {
		assertEquals(testItemCard.getName(), cardName);
	}

	@Test
	public void testGetIndexNumber() {
		assertEquals(testItemCard.getIndexNumber(), cardIndex);
	}

	@Test
	public void testEqualsObject() {
		assertEquals(testItemCard, new RemoteItemCard(cardName, cardIndex));
	}

}
