package alien.common;

import static org.junit.Assert.*;

import org.junit.Test;

public class RemoteStatusTest {

	RemoteStatus testStatus = new RemoteStatus();
	
	@Test
	public void testRemoteStatus() {
		assertFalse(testStatus.canAttack());
		assertFalse(testStatus.canDiscardItemCard());
		assertFalse(testStatus.canEndTurn());
		assertFalse(testStatus.canSelectItemCard());
		assertFalse(testStatus.canSelectPosition());
		assertFalse(testStatus.hasWon());
		assertFalse(testStatus.isDead());
		assertFalse(testStatus.isOnTurn());
	}

	@Test
	public void testCanAttack() {
		RemoteStatus cloneStatus = testStatus.clone();
		boolean value = true;
		cloneStatus.setCanAttack(value);
		cloneStatus.setCanAttack(value);
		assertEquals(cloneStatus.canAttack(), value);
		value = false;
		cloneStatus.setCanAttack(false);
		cloneStatus.setCanAttack(value);
		assertEquals(cloneStatus.canAttack(), value);
		
	}

	
	@Test
	public void testSetCanAttack() {
		RemoteStatus cloneStatus = testStatus.clone();
		boolean value = true;
		cloneStatus.setCanAttack(value);
		assertEquals(cloneStatus.canAttack(), value);
		assertEquals(testStatus.canDiscardItemCard(), cloneStatus.canDiscardItemCard());
		assertEquals(testStatus.canEndTurn(), cloneStatus.canEndTurn());
		assertEquals(testStatus.canSelectItemCard(), cloneStatus.canSelectItemCard());
		assertEquals(testStatus.canSelectPosition(), cloneStatus.canSelectPosition());
		assertEquals(testStatus.hasWon(), cloneStatus.hasWon());
		assertEquals(testStatus.isDead(), cloneStatus.isDead());
		assertEquals(testStatus.isOnTurn(), cloneStatus.isOnTurn());
		
		value = false;
		cloneStatus.setCanAttack(value);
		
		assertEquals(cloneStatus.canAttack(), value);
		assertEquals(testStatus.canDiscardItemCard(), cloneStatus.canDiscardItemCard());
		assertEquals(testStatus.canEndTurn(), cloneStatus.canEndTurn());
		assertEquals(testStatus.canSelectItemCard(), cloneStatus.canSelectItemCard());
		assertEquals(testStatus.canSelectPosition(), cloneStatus.canSelectPosition());
		assertEquals(testStatus.hasWon(), cloneStatus.hasWon());
		assertEquals(testStatus.isDead(), cloneStatus.isDead());
		assertEquals(testStatus.isOnTurn(), cloneStatus.isOnTurn());
	}

	@Test
	public void testClone() {
		RemoteStatus statusToClone = new RemoteStatus();
		statusToClone.setDead(false);
		statusToClone.setOnTurn(true);
		statusToClone.setCanSelectPosition(true);
		assertEquals(statusToClone, statusToClone.clone());
	}

	public RemoteStatus getClonedStatus() {
		return this.testStatus.clone();
	}
	
}
