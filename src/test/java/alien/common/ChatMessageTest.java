package alien.common;

import static org.junit.Assert.*;

import java.util.Calendar;

import org.junit.Test;

public class ChatMessageTest {
	
	String message = "hello";
	String sender = "Andrea";
	ChatMessage testMessage = new ChatMessage(message, sender);
	Calendar now = Calendar.getInstance();
	int hourInt=now.get(Calendar.HOUR_OF_DAY);
	int minutesInt=now.get(Calendar.MINUTE);

	@Test
	public void testToString() {
		String hourString;
		String minutesString;
		if (hourInt<10)
			hourString = "0"+String.valueOf(hourInt);
		else
			hourString = String.valueOf(hourInt);
		if (minutesInt<10)
			minutesString = "0"+String.valueOf(minutesInt);
		else
			minutesString = String.valueOf(minutesInt);
		assertEquals("["+hourString+":"+minutesString+"-"+sender+"] "+message, testMessage.toString());
	}

	@Test
	public void testGetMessage() {
		assertEquals(message,testMessage.getMessage());
	}

}
