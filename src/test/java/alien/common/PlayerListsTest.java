package alien.common;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class PlayerListsTest {
	List<String> players = new ArrayList<String>();
	PlayerLists playerLists = new PlayerLists((ArrayList)players);

	@Before
	public void init() {
		players.add("Luigi");
		players.add("Fabio");
		players.add("Andrea");
		players.add("Calimero");
	}

	@Test
	public void testGetUnknownTypePlayers() {
		assertTrue(players.equals(playerLists.getUnknownTypePlayers()));
	}

	@Test
	public void testGetDeadPlayers() {
		playerLists.addToDeadPlayers("Luigi");
		List<String> deadPlayers = new ArrayList<String>();
		deadPlayers.add("Luigi");
		assertTrue(playerLists.getDeadPlayers().equals(deadPlayers));
	}

	@Test
	public void testAddToDeadPlayers() {
		List<String> oldList = new ArrayList<String>();
		List<String> deadList = new ArrayList<String>();
		for (String aPlayer:playerLists.getDeadPlayers())
			deadList.add(aPlayer);
		for (String aPlayer:playerLists.getUnknownTypePlayers())
			oldList.add(aPlayer);
		playerLists.addToDeadPlayers("Calimero");
		oldList.remove("Calimero");
		deadList.add("Calimero");
		assertEquals(oldList, playerLists.getUnknownTypePlayers());
		assertEquals(deadList, playerLists.getDeadPlayers());
		
	}

	@Test
	public void testClone() {
		assertEquals(playerLists.clone().getDeadPlayers(), playerLists.getDeadPlayers());
		assertEquals(playerLists.clone().getUnknownTypePlayers(), playerLists.getUnknownTypePlayers());
		
	}

	@Test
	public void testEqualsObject() {
		assertTrue(playerLists.equals(playerLists.clone()));
	}

}
