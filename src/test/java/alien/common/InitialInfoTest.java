package alien.common;

import static org.junit.Assert.*;

import org.junit.Test;

public class InitialInfoTest {

	String map = "NANSNANDANS";
	String characterName = "Captain Pug";
	String pawnType = "Alien";
	String welcomeMessage = "Welcome pug";
	RemoteStatus remoteStatus = new RemoteStatus();
	InitialInfo test = new InitialInfo(map, pawnType, characterName, welcomeMessage, remoteStatus);
	
	@Test
	public void testGetTextMap() {
		assertTrue(map.equals(test.getTextMap()));
	}

	@Test
	public void testGetCharacterName() {
		assertTrue(characterName.equals(test.getCharacterName()));
	}

	@Test
	public void testGetWelcomeMessage() {
		assertTrue(welcomeMessage.equals(test.getWelcomeMessage()));
	}

	@Test
	public void testGetPawnType() {
		assertTrue(pawnType.equals(test.getPawnType()));
	}

	@Test
	public void testGetInitialStatus() {
		assertTrue(remoteStatus.equals(test.getInitialStatus()));
	}

}
