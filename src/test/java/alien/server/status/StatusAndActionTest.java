package alien.server.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;

import static org.junit.Assert.*;




import org.junit.Test;

import alien.server.action.Attack;
import alien.server.action.BaseAction;
import alien.server.action.Disconnected;
import alien.server.action.EndTurn;
import alien.server.action.ItemCardDiscarded;
import alien.server.action.ItemCardSelected;
import alien.server.action.PositionSelected;
import alien.server.card.DeckCreator;
import alien.server.game.GameBoard;
import alien.server.game.Player;
import alien.server.sector.MapClass;
import alien.server.sector.MapGenerator;
import alien.server.sector.Position;

public class StatusAndActionTest {
	MapClass map;
	GameBoard gameBoard;
	Status[] statuses;
	@Before
	public void testFSM() {
		 map = new MapGenerator().getRandomMap();
		 gameBoard = new GameBoard(map);
		List<String> players = new ArrayList<String> ();
		players.add("pippo");
		players.add("franco");
		gameBoard.createPlayers(players);
		gameBoard.assignRoles();
		gameBoard.chooseRandomlyFirstPlayer();
		
	}
	@Test
	public void attackTest()
	{
		BaseAction attack = new Attack();
		Player player = gameBoard.getPlayerOnTurn();
		Status[] statuses = {new TurnStartedStatus(),
				new WaitingPosition(new NoiseEvent(player)), 
				new WaitingPosition(new SpotlightEvent(player, map)),
				new WaitingItemCard(player.getStatus()),
						new HasMovedStatus(),
						new HasAttackedStatus(),
						new TurnEndedStatus(),
						new DeadStatus(),
						new WonStatus(),
						new DisconnectedStatus()};
		attack.setPlayerName(gameBoard.getPlayerOnTurn().getName());
		for(Status status : statuses)
		{
			player.setStatus(status);
	     	attack.applyTo(gameBoard);
		}
 	
	}
	@Test
	public void positionSelectedTest()
	{	
		Player player = gameBoard.getPlayerOnTurn();
		Position pos =map.getAdjacentValidPositions(player.getPawn().getPosition(), player.getPawn()).get(0);
		BaseAction positionSelected = new PositionSelected(pos);
		Status[] statuses = {new TurnStartedStatus(),
				new WaitingPosition(new HasMovedStatus(),new NoiseEvent(player)), 
				new WaitingPosition(new HasMovedStatus(),new SpotlightEvent(player, map)),
				new WaitingItemCard(player.getStatus()),
						new HasMovedStatus(),
						new HasAttackedStatus(),
						new TurnEndedStatus(),
						new DeadStatus(),
						new WonStatus(),
						new DisconnectedStatus()};
		positionSelected.setPlayerName(gameBoard.getPlayerOnTurn().getName());
		for(Status status : statuses)
		{
			
			player.setStatus(status);
			positionSelected.applyTo(gameBoard);
		}
		
	}
	@Test
	public void itemCardSelectedTest()
	{
		
		Player player = gameBoard.getPlayerOnTurn();
		BaseAction itemCardSelected = new ItemCardSelected(0);
		Status[] statuses = {new TurnStartedStatus(),
				new WaitingPosition(new HasMovedStatus(),new NoiseEvent(player)), 
				new WaitingPosition(new HasMovedStatus(),new SpotlightEvent(player, map)),
				new WaitingItemCard(player.getStatus()),
						new HasMovedStatus(),
						new HasAttackedStatus(),
						new TurnEndedStatus(),
						new DeadStatus(),
						new WonStatus(),
						new DisconnectedStatus()};
		itemCardSelected.setPlayerName(gameBoard.getPlayerOnTurn().getName());
		for(Status status : statuses)
		{
				player.drawItemCard();
				player.setStatus(status);
				itemCardSelected.applyTo(gameBoard);
			
			
		}
		
	}
	@Test
	public void itemCardDiscardedTest()
	{
		
		Player player = gameBoard.getPlayerOnTurn();
		BaseAction itemCardDiscarded = new ItemCardDiscarded(0);
		Status[] statuses = {new TurnStartedStatus(),
				new WaitingPosition(new HasMovedStatus(),new NoiseEvent(player)), 
				new WaitingPosition(new HasMovedStatus(),new SpotlightEvent(player, map)),
				new WaitingItemCard(player.getStatus()),
						new HasMovedStatus(),
						new HasAttackedStatus(),
						new TurnEndedStatus(),
						new DeadStatus(),
						new WonStatus(),
						new DisconnectedStatus()};
		itemCardDiscarded.setPlayerName(gameBoard.getPlayerOnTurn().getName());
		for(Status status : statuses)
		{
				player.drawItemCard();
				player.setStatus(status);
				itemCardDiscarded.applyTo(gameBoard);
			
		}

	}
	@Test
	public void turnEndedTest()
	{
		Player player = gameBoard.getPlayerOnTurn();
		BaseAction endTurn = new EndTurn();
		Status[] statuses = {new TurnStartedStatus(),
				new WaitingPosition(new HasMovedStatus(),new NoiseEvent(player)), 
				new WaitingPosition(new HasMovedStatus(),new SpotlightEvent(player, map)),
				new WaitingItemCard(player.getStatus()),
						new HasMovedStatus(),
						new HasAttackedStatus(),
						new TurnEndedStatus(),
						new DeadStatus(),
						new WonStatus(),
						new DisconnectedStatus()};
		endTurn.setPlayerName(gameBoard.getPlayerOnTurn().getName());
		for(Status status : statuses)
		{
			player.setStatus(status);
			endTurn.applyTo(gameBoard);
		}
	}
	@Test
	public void disconnectedTest()
	{
		Player player = gameBoard.getPlayerOnTurn();
		BaseAction disconnect = new Disconnected();
		Status[] statuses = {new TurnStartedStatus(),
				new WaitingPosition(new HasMovedStatus(),new NoiseEvent(player)), 
				new WaitingPosition(new HasMovedStatus(),new SpotlightEvent(player, map)),
				new WaitingItemCard(player.getStatus()),
						new HasMovedStatus(),
						new HasAttackedStatus(),
						new TurnEndedStatus(),
						new DeadStatus(),
						new WonStatus(),
						new DisconnectedStatus()};
		disconnect.setPlayerName(gameBoard.getPlayerOnTurn().getName());
		for(Status status : statuses)
		{
			player.setStatus(status);
			disconnect.applyTo(gameBoard);
			assertTrue(player.getStatus()instanceof DisconnectedStatus);
		}
	}
}