package alien.server.game;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import alien.server.sector.AlienSector;
import alien.server.sector.DangerousSector;
import alien.server.sector.EscapeSector;
import alien.server.sector.HumanSector;
import alien.server.sector.MapClass;
import alien.server.sector.MapGenerator;
import alien.server.sector.Position;
import alien.server.sector.Sector;
import alien.server.sector.SecureSector;

public class HumanTest {

	@Test
	public void testCanUseItem() {

		Human human = new Human(null, "Human");
		assertTrue(human.canUseItem());
	}

	@Test
	public void testCanAttack() {	
	Human human = new Human(null, "Human");
	assertFalse(human.canAttack());
	}

	@Test
	public void testCanEnter() {
		Human human = new Human(null, "Human");
		Position pos = new Position(1, 1);
		Sector [] failSectors = {new HumanSector(pos), new AlienSector(pos)};
		Sector [] successSectors = {new DangerousSector(pos),new EscapeSector(pos, 1),new SecureSector(pos)};
		for(Sector sector: failSectors) assertFalse(human.canEnter(sector));
		for(Sector sector: successSectors) assertTrue(human.canEnter(sector));

	}

	@Test
	public void testRegister() {
		MapGenerator mg = new MapGenerator();
		MapClass map=mg.getRandomMap();
		GameBoard gameBoard = new GameBoard(map);

		Human human = new Human(null, "Human");
		human.register(gameBoard);
		assertEquals(human.getStartingPosition(),human.getPosition());
		assertEquals(human.getStartingPosition(), gameBoard.getMap().getHumanPosition());
	}

	

	@Test
	public void testEscape() {

		Human human = new Human(null, "Human");
		human.escape();
		assertTrue(human.isEscaped());
	}

	@Test
	public void testIsEscaped() {
		Human human = new Human(null, "Human");
		assertFalse(human.isEscaped());
		human.escape();
		assertTrue(human.isEscaped());
	}


	@Test
	public void testMove() {
		Human human = new Human(null, "Human");
		Position pos = new Position(3, 2);
		human.move(pos);
		assertEquals(human.getPosition(), pos);
		
	}

	@Test
	public void testDie() {

		Human human = new Human(null, "Human");
		human.die();
		assertFalse(human.isAlive());
	}

	@Test
	public void testAttack() {

		Human human = new Human(null, "Human");
		Human human2 = new Human(null, "Human");
		List<Pawn> attacked = new ArrayList<Pawn>();
		human.attack(attacked);
		
		Alien alien = new Alien(null, "Alien");
		attacked.add(alien);
		attacked.add(human2);
		human.attack(attacked);
		assertFalse(alien.isAlive());
		assertFalse(human2.isAlive());
	}


	@Test
	public void testSetSilent() {

		Human human = new Human(null, "Human");
		assertFalse(human.isSilent());
		human.setSilent(true);
		assertTrue(human.isSilent());
	}
	@Test
	public void testSetStartingPosition() {
		Human human = new Human(null, "Human");
		Position pos = new Position(3,2);
		human.setStartingPosition(pos);
		assertEquals(human.getStartingPosition(),pos);
		assertEquals(human.getStartingPosition(),human.getPosition());
	}

	@Test
	public void testGetName() {

		Human human = new Human(null, "Human");
		assertEquals(human.getName(),"Human");
	}

}
