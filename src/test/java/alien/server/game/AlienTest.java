package alien.server.game;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import alien.server.sector.AlienSector;
import alien.server.sector.DangerousSector;
import alien.server.sector.EscapeSector;
import alien.server.sector.HumanSector;
import alien.server.sector.MapClass;
import alien.server.sector.MapGenerator;
import alien.server.sector.Position;
import alien.server.sector.Sector;
import alien.server.sector.SecureSector;

public class AlienTest {


	@Test
	public void testCanUseItem() {
		Alien alien = new Alien(null, "Alien");
		assertFalse(alien.canUseItem());
	}

	@Test
	public void testCanAttack() {
		Alien alien = new Alien(null, "Alien");
		assertTrue(alien.canAttack());
	}

	@Test
	public void testCanEnter() {
		Alien alien = new Alien(null, "Alien");
		Position pos = new Position(1, 1);
		Sector [] failSectors = {new HumanSector(pos),new EscapeSector(pos, 1), new AlienSector(pos)};
		Sector [] successSectors = {new DangerousSector(pos),new SecureSector(pos)};
		for(Sector sector: failSectors) assertFalse(alien.canEnter(sector));
		for(Sector sector: successSectors) assertTrue(alien.canEnter(sector));
	}

	@Test
	public void testRegister() {
		MapGenerator mg = new MapGenerator();
		MapClass map=mg.getRandomMap();
		GameBoard gameBoard = new GameBoard(map);
		Alien alien = new Alien(null, "Alien");
		alien.register(gameBoard);
		assertEquals(alien.getStartingPosition(),alien.getPosition());
		assertEquals(alien.getStartingPosition(), gameBoard.getMap().getAlienPosition());
	}

	@Test
	public void testAttack() {
		Alien alien = new Alien(null, "Alien");

		List<Pawn> attacked = new ArrayList<Pawn>();
		alien.attack(attacked);
		assertEquals(alien.getNumberOfSteps(), alien.numberOfSteps);

		Alien alien2 = new Alien(null, "Alien2");
		attacked.add(alien2);
		alien.attack(attacked);
		assertEquals(alien.getNumberOfSteps(), alien.numberOfSteps);
		assertFalse(alien2.isAlive());
		attacked.clear();
		
		Human human = new Human(null, "Human");
		attacked.add(human);
		int steps = alien.getNumberOfSteps();
		alien.attack(attacked);
		assertEquals(alien.getNumberOfSteps(),steps+1);
		assertFalse(human.isAlive());
	}

	@Test
	public void testMove() {

		Alien alien = new Alien(null, "Alien");
		Position pos = new Position(3, 2);
		alien.move(pos);
		assertEquals(alien.getPosition(), pos);
		
	}

	@Test
	public void testSetStartingPosition() {
		Alien alien = new Alien(null, "Alien");
		Position pos = new Position(3,2);
		alien.setStartingPosition(pos);
		assertEquals(alien.getStartingPosition(),pos);
		assertEquals(alien.getStartingPosition(),alien.getPosition());
	}

	@Test
	public void testGetName() {

		Alien alien = new Alien(null, "Alien");
		assertEquals(alien.getName(), "Alien");
	}
	@Test 
	public void testDie()
	{
		Alien alien = new Alien(null, "Alien");
		alien.die();
		assertFalse(alien.isAlive());
	}

}
