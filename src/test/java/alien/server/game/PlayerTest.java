package alien.server.game;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;
import alien.server.card.DefenseCard;
import alien.server.card.GameDecks;
import alien.server.exception.CannotDefendException;
import alien.server.sector.MapClass;
import alien.server.sector.MapGenerator;
import alien.server.sector.Position;
import alien.server.sector.Sector;
import alien.server.sector.SecureSector;
import alien.server.status.DeadStatus;
import alien.server.status.HasMovedStatus;
import alien.server.status.TurnStartedStatus;
import alien.server.status.WaitingItemCard;
import alien.server.status.WonStatus;

public class PlayerTest {
	Player player;
	GameDecks decks;
	MapClass map;
	@Before
	public void setPlayer()
	{

		decks=new GameDecks(3);
    	  MapGenerator mg = new MapGenerator();
     	  map=mg.getRandomMap();
		player=new Player(map, decks, "Pippo");
		player.drawCharacterCard();
		player.getPawn().move(player.getMap().getAlienPosition());
		
	}
	@Test
	public void testPlayer() {
		Player p = new Player(null, null, "Sandro");
	}

	@Test
	public void testUseItem() {	
		for(int i=0;i<Player.MAX_NUMBER_OF_ITEMS;i++)
		{
			player.drawItemCard();
		}

		int deckSize=decks.getItemCardDeckSize();
		for(int i=Player.MAX_NUMBER_OF_ITEMS-1;i>=0;i--)
		{
			player.useItem(i);
		}
		assertTrue(decks.getItemCardDeckSize()>=deckSize&&decks.getItemCardDeckSize()<=deckSize+Player.MAX_NUMBER_OF_ITEMS);
	
	}

	@Test
	public void testDiscardItem() {
		for(int i=0;i<Player.MAX_NUMBER_OF_ITEMS;i++)
		{
			player.drawItemCard();
		}

		int deckSize=decks.getItemCardDeckSize();
		for(int i=Player.MAX_NUMBER_OF_ITEMS-1;i>=0;i--)
		{
			player.discardItem(i);
		}
		assertTrue(decks.getItemCardDeckSize()==deckSize+Player.MAX_NUMBER_OF_ITEMS);
		try{
			player.discardItem(0);
			fail();
		}
		catch(IndexOutOfBoundsException ex)
		{
			
		}
	}

	@Test
	public void testDrawSectorCard() {
		player.drawSectorCard();
	}

	@Test
	public void testDrawEscapeCard() {
		player.drawEscapeCard();
	}

	@Test
	public void testDrawCharacterCard() {
		decks=new GameDecks(3);
		player=new Player(map, decks, "Pippo");
		assertTrue(player.getPawn()==null);
		player.drawCharacterCard();
		assertTrue(player.getPawn()!=null);
	}
	
	@Test
	public void testDrawItemCard() {

		int deckSize=decks.getItemCardDeckSize();
		player.drawItemCard();
		assertTrue(decks.getItemCardDeckSize()==deckSize-1);
		for(int i=0;i<Player.MAX_NUMBER_OF_ITEMS;i++)
		{
			player.drawItemCard();
		}
		assertTrue(player.getStatus() instanceof WaitingItemCard);
	}

	@Test
	public void testPawnKilled() {
		Position p = map.getHumanPosition();
			Pawn pawn = player.getPawn();
			pawn.setStartingPosition(p);
			Sector s = map.getSector(p);
			s.pawnIsArrived(pawn);
			player.pawnKilled();
			assertTrue(s.getPawns().isEmpty());
			assertTrue(player.getStatus() instanceof DeadStatus);
	}

	@Test
	public void testIsYourTurn() 
	{
		int turn = player.getTurn();
		player.isYourTurn();
		assertTrue(player.getTurn()==turn+1);
	}

	@Test
	public void testCanPlay() {
		player.setStatus(new TurnStartedStatus());
		assertTrue(player.canPlay());
		player.setStatus(new DeadStatus());
		assertFalse(player.canPlay());
	}

	@Test
	public void testRemoveEffects() {
		player.drawItemCard();
		player.useItem(0);
		player.removeEffects();
	}

	@Test
	public void testMove() {
		Position pos1= new Position(1, 1);
		Position pos2= new Position(1, 2);
		Sector sector1,sector2;
		Pawn pawn = player.getPawn();
		sector1 = new SecureSector(pos1);
		sector2 = new SecureSector(pos2);
		map.putSector(sector1);
		map.putSector(sector2);
		sector1.pawnIsArrived(pawn);
		pawn.move(pos1);
		player.move(pos2);
		assertEquals(player.getPawn().getPosition(),pos2);
		assertTrue(sector1.getPawns().isEmpty());
		assertTrue(sector2.getPawns().get(0).equals(pawn));
		assertTrue(player.getStatus() instanceof HasMovedStatus);
	}

	@Test
	public void testCanDefend() {
		assertFalse(player.canDefend());
		player.addDefenseItem(new DefenseCard());
		assertTrue(player.canDefend());
	}

	@Test
	public void testDefend() {
		player.addDefenseItem(new DefenseCard());
		player.defend();
		try{
			player.defend();
			fail();
		}catch(CannotDefendException ex){}
	}

	@Test
	public void testSetStatus() {
		player.setStatus(new WonStatus());
		assertTrue(player.getStatus() instanceof WonStatus);
	}

	@Test
	public void testAttack() {
		Human human = new Human(null, "Human");
		player.getMap().getSector(player.getPawn().getPosition()).pawnIsArrived(human);
		player.attack();
		assertFalse(human.isAlive());
	}

	@Test
	public void testIsAlive() {
		assertTrue(player.isAlive());
		player.getPawn().die();
		assertFalse(player.isAlive());
	}

	@Test
	public void testPopAndPushIncompleteStatus() {
		player.setStatus(new HasMovedStatus());
		player.pushIncompleteStatus(new WaitingItemCard(player.getStatus()));
		assertTrue(player.getStatus()instanceof WaitingItemCard);
		assertTrue(player.popIncompleteStatus() instanceof WaitingItemCard);
		assertTrue(player.getStatus() instanceof HasMovedStatus);
	}

}
