package alien.server.game;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import alien.server.sector.MapClass;
import alien.server.sector.MapGenerator;
import alien.server.status.DisconnectedStatus;
import alien.server.status.TurnEndedStatus;
import alien.server.status.TurnStartedStatus;
import alien.server.status.WonStatus;

public class GameBoardTest {
	GameBoard gameBoard;
@Before
public void setGameBoard()
{
	MapClass map = new MapGenerator().getRandomMap();
	gameBoard= new GameBoard(map);
}

	@Test
	public void testCreatePlayers() {
		List<String> players = new ArrayList<String> ();
		players.add("pippo");
		players.add("franco");
		gameBoard.createPlayers(players);
		assertEquals(gameBoard.getPlayersNames(),players);
		assertTrue(gameBoard.getPlayers().size()==players.size());
	}

	@Test
	public void testHumansVictory() {
		List<String> players = new ArrayList<String> ();
		players.add("pippo");
		gameBoard.createPlayers(players);
		gameBoard.assignRoles();
		gameBoard.humansVictory();
		assertTrue(gameBoard.getPlayerByName("pippo").getStatus() instanceof TurnEndedStatus);
	}

	@Test
	public void testAliensVictory() {
		List<String> players = new ArrayList<String> ();
		players.add("pippo");
		gameBoard.createPlayers(players);
		gameBoard.assignRoles();
		gameBoard.aliensVictory();
		assertTrue(gameBoard.getPlayerByName("pippo").getStatus() instanceof WonStatus);
	}

	@Test
	public void testDisconnectPlayer() {
		List<String> players = new ArrayList<String>();
		players.add("pippo");
		players.add("franco");
		gameBoard.createPlayers(players);
		gameBoard.assignRoles();
		gameBoard.chooseRandomlyFirstPlayer();
		gameBoard.disconnectPlayer("pippo");
		assertTrue(gameBoard.getPlayerByName("pippo").getStatus() instanceof DisconnectedStatus);
	}

	@Test
	public void testGetPlayerOnTurn() {
		List<String> players = new ArrayList<String> ();
		players.add("pippo");
		players.add("franco");
		gameBoard.createPlayers(players);
		gameBoard.assignRoles();
		gameBoard.chooseRandomlyFirstPlayer();
		assertTrue(players.contains(gameBoard.getPlayerOnTurn().getName()));
	}

	@Test
	public void testNextTurn() {
		List<String> players = new ArrayList<String> ();
		players.add("pippo");
		players.add("franco");
		gameBoard.createPlayers(players);
		gameBoard.assignRoles();
		gameBoard.chooseRandomlyFirstPlayer();
		String playerOnTurn= gameBoard.getPlayerOnTurn().getName();
		gameBoard.nextTurn();
		assertFalse(playerOnTurn.equals(gameBoard.getPlayerOnTurn().getName()));
	}


	@Test
	public void testRegisterHuman() {
		Human human = new Human(null, "Umano");
		gameBoard.registerHuman(human);
		assertTrue(gameBoard.getMap().getHumanPosition().equals(human.getStartingPosition()));
	}

	@Test
	public void testRegisterAlien() {

		Alien alien = new Alien(null, "Alieno");
		gameBoard.registerAlien(alien);
		assertTrue(gameBoard.getMap().getAlienPosition().equals(alien.getStartingPosition()));
	}

	@Test
	public void testGetDeadPlayersNames() {
		List<String> players = new ArrayList<String> ();
		players.add("pippo");
		players.add("franco");
		gameBoard.createPlayers(players);
		gameBoard.assignRoles();
		gameBoard.chooseRandomlyFirstPlayer();
		Player player = gameBoard.getPlayerOnTurn();
		player.getPawn().die();
		assertTrue(gameBoard.getDeadPlayersNames().contains(player.getName()));
		players.remove(player.getName());
		assertFalse(gameBoard.getDeadPlayersNames().contains(players.get(0)));
	}

	@Test(expected=NullPointerException.class)
	public void testRegister() {
		gameBoard.register(null);
	}

	@Test
	public void testUnregisterHuman() {

		Human human = new Human(null, "Umano");
		gameBoard.registerHuman(human);
		gameBoard.unregister(human);
	}

	@Test
	public void testUnregisterAlien() {
		Alien alien = new Alien(null, "Alieno");
		gameBoard.registerAlien(alien);
		gameBoard.unregister(alien);
	}


	@Test
	public void testAssignRoles() {
		List<String> players = new ArrayList<String> ();
		players.add("pippo");
		players.add("franco");
		gameBoard.createPlayers(players);
		gameBoard.assignRoles();
		for(Player player : gameBoard.getPlayers())
		{
			assertTrue(player.getPawn()!=null);
			assertTrue(player.getPawn().getStartingPosition()!=null);
		}
	}

	@Test
	public void testChooseRandomlyFirstPlayer() {
		List<String> players = new ArrayList<String> ();
		players.add("pippo");
		players.add("franco");
		gameBoard.createPlayers(players);
		gameBoard.assignRoles();
		gameBoard.chooseRandomlyFirstPlayer();
		List<Player> playersList = gameBoard.getPlayers();
		assertTrue(gameBoard.getPlayerOnTurn().getStatus() instanceof TurnStartedStatus);
		playersList.remove(gameBoard.getPlayerOnTurn());
		assertFalse(playersList.get(0).getStatus() instanceof TurnStartedStatus);
	}

}
