package alien.server.sector;

import static org.junit.Assert.*;

import org.junit.Test;

import alien.server.exception.MalformedMapException;
import alien.server.file.MapHandler;

public class MapGeneratorTest {

	@Test
	public void testGetRandomMap() {
		MapGenerator mg = new MapGenerator();
		MapClass map= mg.getRandomMap();
	    try {
	    	MapHandler.isTextMapLegit(map.toString());
	    }
		catch (MalformedMapException e) {
			fail();
	    }
	}
}

