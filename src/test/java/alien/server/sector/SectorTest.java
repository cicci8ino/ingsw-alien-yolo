package alien.server.sector;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import alien.server.game.Alien;
import alien.server.game.Human;
import alien.server.game.Pawn;

public class SectorTest {

	Position position = new Position(10,10);
	Sector sector = new SecureSector(position);
	
	
	
	@Test
	public void testSector() {
		assertTrue(sector.getPosition().equals(position));
	}

	@Test
	public void testPawnIsArrived() {
		Sector sector = new SecureSector(position);
		Pawn testPawn1 = new Alien(null, "Luigi");
		Pawn testPawn2 = new Alien(null, "Giuseppe");
		List<Pawn> enteredPawn = new ArrayList<Pawn>();
		enteredPawn.add(testPawn1);
		enteredPawn.add(testPawn2);
		sector.pawnIsArrived(testPawn1);
		sector.pawnIsArrived(testPawn2);
		assertEquals("Ho inserito due pawn, quindi nel settore dovrebbero essercene due", sector.getPawns().size(), 2);
		for (Pawn aPawn:sector.getPawns())
			enteredPawn.remove(aPawn);
		assertTrue("Rimuovo tutti i pawn entrati nel settore dalla mia lista", enteredPawn.isEmpty());
			
	}

	@Test
	public void testPawnExit() {
		Sector sector = new SecureSector(position);
		testPawnIsArrived();
		Pawn testPawn1 = new Alien(null, "Luigi");
		Pawn testPawn2 = new Alien(null, "Giuseppe");
		Pawn testPawn3 = new Human(null, "Mario");
		Pawn testPawn4 = new Human(null, "Francesco");
		List<Pawn> enteredPawn = new ArrayList<Pawn>();
		List<Pawn> exitPawn = new ArrayList<Pawn>();
		enteredPawn.add(testPawn1);
		enteredPawn.add(testPawn2);
		enteredPawn.add(testPawn3);
		enteredPawn.add(testPawn4);
		exitPawn.add(testPawn3);
		exitPawn.add(testPawn4);
		for (Pawn aPawn:enteredPawn)
			sector.pawnIsArrived(aPawn);
		for (Pawn aPawn:exitPawn)
			sector.pawnExit(aPawn);
		assertEquals(sector.getPawns().size(), enteredPawn.size()-exitPawn.size());
		for (Pawn aPawn:exitPawn)
			enteredPawn.remove(aPawn);
		assertEquals(enteredPawn.size(), sector.getPawns().size());
		for (Pawn aPawn:sector.getPawns())
			enteredPawn.remove(aPawn);
		assertTrue(enteredPawn.isEmpty());
	}
	

}
