package alien.server.sector;

import static org.junit.Assert.*;

import org.junit.Test;

import alien.server.game.Alien;
import alien.server.game.Human;

public class AlienSectorTest {

	Sector alienSector = new AlienSector(null);
	
	@Test
	public void testCanEnterHuman() {
		assertFalse(alienSector.canEnter(new Human(null, null)));
	}

	@Test
	public void testCanEnterAlien() {
		assertFalse(alienSector.canEnter(new Alien(null, null)));
	}

}
