package alien.server.sector;

import static org.junit.Assert.*;

import org.junit.Test;

import alien.server.game.Alien;
import alien.server.game.Human;

public class DangerousSectorTest {

	Sector dangerousSector = new DangerousSector(null);
	
	@Test
	public void testCanEnterHuman() {
		assertTrue(dangerousSector.canEnter(new Human(null, null)));
	}

	@Test
	public void testCanEnterAlien() {
		assertTrue(dangerousSector.canEnter(new Alien(null, null)));
	}

}
