package alien.server.sector;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import alien.server.file.MapHandler;
import alien.server.game.Alien;
import alien.server.game.Human;
import alien.server.game.Pawn;

public class MapClassTest {
	
	static MapClass map;
	static MapClass mapTwo;
	static Position startingPosition= new Position(12, 15);
	static Pawn humanPawn = new Human(null, null);
	static Pawn alienPawn = new Alien(null, null);
	Position humanPosition = new Position(10,12);
	Position alienPosition = new Position(9,12);
	static List<Position> humanPositions = new ArrayList<Position>();
	static List<Position> alienPositions = new ArrayList<Position>();
	
	@BeforeClass
	public static void initMap() {
		/*
		 * Carica mappa conosciuta
		 */
		MapHandler mapHandler = new MapHandler();
		map = mapHandler.getMap("fermi");
		mapTwo = mapHandler.getMap("fermi");
		humanPawn.setStartingPosition(startingPosition);
		alienPawn.setStartingPosition(startingPosition);
		
	}

	@Test
	public void testReachablePositions() {
		/*
		 * Movimento umano
		 */
		humanPositions.add(new Position(11,16));
		humanPositions.add(new Position(11,14));
		humanPositions.add(new Position(12,14));
		List<Position> tempHumanList = new ArrayList<Position>();
		tempHumanList.add(new Position(11,16));
		tempHumanList.add(new Position(11,14));
		tempHumanList.add(new Position(12,14));
		assertEquals(map.reachablePositions(humanPawn).size(), 3);
		for (Position aPosition:map.reachablePositions(humanPawn)) {
			tempHumanList.remove(aPosition);
		}
		assertTrue(tempHumanList.isEmpty());
		/*
		 * Movimento alieno
		 */
		alienPositions.add(new Position(11,16));
		alienPositions.add(new Position(11,14));
		alienPositions.add(new Position(12,14));
		alienPositions.add(new Position(10,14));
		alienPositions.add(new Position(11,13));
		alienPositions.add(new Position(13,13));
		alienPositions.add(new Position(10,16));
		assertEquals(map.reachablePositions(alienPawn).size(), 7);
		for (Position aPosition:map.reachablePositions(alienPawn)) {
			alienPositions.remove(aPosition);
		}
		assertTrue(alienPositions.isEmpty());
		
	}

	@Test
	public void testIsReachable() {
		/*
		 * Provo solo per umano
		 */
		for (Position aPosition:humanPositions) {
			assertTrue(map.isReachable(aPosition, humanPawn));
		}
		assertFalse(map.isReachable(new Position(13,15), humanPawn));
		assertFalse(map.isReachable(humanPawn.getStartingPosition(), humanPawn));
	}

	@Test
	public void testIsValidPositionToMove() {
		alienPawn.setStartingPosition(startingPosition);
		assertFalse("Alieno non può entrare nel settore alieno",map.isValidPosition(alienPosition, alienPawn));
		assertFalse("Alieno non può entrare nel settore umano", map.isValidPosition(humanPosition, alienPawn));
	}

	@Test
	public void testIsValidPositionPosition() {
		assertFalse(MapClass.isValidPosition(new Position(100,100)));
		assertTrue(MapClass.isValidPosition(new Position(14,23)));
	}

	@Test
	public void testPutSector() {
		Position position = new Position(3,3);
		Sector sector = map.getSector(position);
		assertTrue(sector==null);
		sector = new SecureSector(position);
		map.putSector(sector);
		assertTrue(map.getSector(position) instanceof SecureSector);
		assertEquals(map.getSector(position), sector);
	}

	@Test
	public void testRemoveSector() {
		Position positionRemoved = new Position(2, 13);
		Sector sector = map.getSector(positionRemoved);
		map.removeSector(positionRemoved);
		Sector sectorRemoved = map.getSector(positionRemoved);
		assertFalse(sector.equals(sectorRemoved));
		assertTrue(sectorRemoved==null);
	}

	@Test
	public void testGetSector() {
		
		Position dangerous = new Position(3,14);
		
		Position secure = new Position(6,15);
		Position escape = new Position(5,14);
		Sector sector = map.getSector(secure);
		assertTrue(sector instanceof SecureSector);
		sector = map.getSector(escape);
		assertTrue(sector instanceof EscapeSector);
		sector = map.getSector(dangerous);
		assertTrue(sector instanceof DangerousSector);
		assertEquals(map.getHumanPosition(), humanPosition);
		sector = map.getSector(humanPosition);
		assertTrue(sector instanceof HumanSector);
		assertEquals(map.getAlienPosition(), alienPosition);
		sector = map.getSector(alienPosition);
		assertTrue(sector instanceof AlienSector);
	}
	
	@Test
	public void testSetHumanPosition() {
		Position position = new Position(3,5);
		mapTwo.setHumanPosition(position);
		assertTrue(mapTwo.getHumanPosition().equals(position));
	}

	@Test
	public void testSetAlienPosition() {
		Position position = new Position(3,5);
		mapTwo.setAlienPosition(position);
		assertTrue(mapTwo.getAlienPosition().equals(position));
	}


}
