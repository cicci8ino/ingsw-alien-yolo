package alien.server.sector;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.Test;

public class PositionTest {
	
	int xOne=3;
	int yOne=7;
	int xTwo=12;
	int yTwo=8;
	Position position = new Position(xOne,yOne);
	Position positionTwo = new Position(xTwo, yTwo);
	Position clonedPosition = new Position(xOne, yOne);
	Position evenPosition = new Position(3,8);

	@Test
	public void testPosition() {
		assertEquals(position.getRow(), xOne);
		assertEquals(position.getColumn(), yOne);
	}

	@Test
	public void testGetColumn() {
		assertEquals(position.getColumn(), yOne);
	}

	@Test
	public void testGetRow() {
		assertEquals(position.getRow(), xOne);
	}

	@Test
	public void testGetColumnString() {
		Character columnCharacter = Character.valueOf((char) (yOne+64));
		assertEquals(""+columnCharacter, position.getColumnString());
	}

	@Test
	public void testGetRowString() {
		if (xOne<10) {
			assertEquals(position.getRowString(), "0"+xOne);
		}
		else
			assertEquals(position.getRowString(), ""+xOne);
		assertEquals(positionTwo.getRowString(), ""+xTwo);
	}

	@Test
	public void testGetAdjacentPositions() {
		List<Position> adjacentPositionOdd = new ArrayList<Position>();
		adjacentPositionOdd.add(new Position(2,7));
		adjacentPositionOdd.add(new Position(4,7));
		adjacentPositionOdd.add(new Position(2,6));
		adjacentPositionOdd.add(new Position(3,6));
		adjacentPositionOdd.add(new Position(2,8));
		adjacentPositionOdd.add(new Position(3,8));
		for (Position aPosition:position.getAdjacentPositions())
			adjacentPositionOdd.remove(aPosition);
		assertTrue(adjacentPositionOdd.isEmpty());
		assertTrue(position.getAdjacentPositions().size()==6);
		List<Position> adjacentPositionEven = new ArrayList<Position>();
		adjacentPositionEven.add(new Position(2,8));
		adjacentPositionEven.add(new Position(4,8));
		adjacentPositionEven.add(new Position(3,7));
		adjacentPositionEven.add(new Position(4,7));
		adjacentPositionEven.add(new Position(3,9));
		adjacentPositionEven.add(new Position(4,9));
		for (Position aPosition:evenPosition.getAdjacentPositions())
			adjacentPositionEven.remove(aPosition);
		assertTrue(adjacentPositionEven.isEmpty());
		assertTrue(positionTwo.getAdjacentPositions().size()==6);
		
		
	}

	@Test
	public void testClone() {
		assertTrue(position.equals(position.clone()));
	}

	@Test
	public void testEqualsObject() {
		assertTrue(position.equals(clonedPosition));
		assertFalse(position.equals(positionTwo));
	}

	@Test
	public void testToString() {
		testGetColumnString();
		testGetRowString();
		assertEquals(position.toString(), "G03");
	}

}
