package alien.server.sector;

import static org.junit.Assert.*;

import org.junit.Test;

import alien.server.game.Alien;
import alien.server.game.Human;

public class HumanSectorTest {

	Sector humanSector = new HumanSector(null);
	
	@Test
	public void testCanEnterHuman() {
		assertFalse(humanSector.canEnter(new Human(null, null)));
	}

	@Test
	public void testCanEnterAlien() {
		assertFalse(humanSector.canEnter(new Alien(null, null)));
	}

}
