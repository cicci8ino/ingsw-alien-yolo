package alien.server.sector;

import static org.junit.Assert.*;

import org.junit.Test;

import alien.server.game.Alien;
import alien.server.game.Human;
import alien.server.game.Player;

public class EscapeSectorTest {

	EscapeSector escapeSector = new EscapeSector(null, 2);
	
	@Test
	public void testCanEnterHuman() {
		assertTrue(escapeSector.canEnter(new Human(null, null)));
	}

	@Test
	public void testCanEnterAlien() {
		assertFalse(escapeSector.canEnter(new Alien(null, null)));
	}

	@Test
	public void testIsUsable() {
		assertTrue(escapeSector.isUsable());
		try {
		escapeSector.playerStopsHere(new Player(null, null, null));
		} catch (NullPointerException e) {
			
		} finally {
			assertFalse(escapeSector.canEnter(new Human(null, null)));
			assertFalse(escapeSector.isUsable());
		}
		
	}

}
