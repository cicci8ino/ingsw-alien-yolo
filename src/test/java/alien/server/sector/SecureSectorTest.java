package alien.server.sector;

import static org.junit.Assert.*;

import org.junit.Test;

import alien.server.game.Alien;
import alien.server.game.Human;

public class SecureSectorTest {

	Sector secureSector = new SecureSector(null);
	
	@Test
	public void testCanEnterHuman() {
		assertTrue(secureSector.canEnter(new Human(null, null)));
	}

	@Test
	public void testCanEnterAlien() {
		assertTrue(secureSector.canEnter(new Alien(null, null)));
	}


}
